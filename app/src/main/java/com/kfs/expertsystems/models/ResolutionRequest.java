package com.kfs.expertsystems.models;

/**
 * Created by Tomesh on 27-09-2017.
 */

public class ResolutionRequest {

    private String uid;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
