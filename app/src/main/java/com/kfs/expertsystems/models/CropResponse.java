package com.kfs.expertsystems.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Tomesh on 21-09-2017.
 */

public class CropResponse implements Serializable {


    /**
     * message : success
     * code : 200
     * responseData : {"crops":[{"id":1,"createdDateTime":1505973073000,"updatedDateTime":1505973073000,"cropName":"Tomato","cropType":"1","cropDesc":"Vegetable","cropImageUrl":"/crop_Images/96d130a4-aa9b-48e3-8380-5f7f0eba06e31505973073337","stageModel":[{"id":1,"createdDateTime":1505973073000,"updatedDateTime":1505973073000,"stageName":"Stage 01","stageDesc":"First stage","stageImageUrl":"/stage_Images/686d3372-c084-4e34-8717-0f11aaf8c94d1505973072674","stageLevel":1},{"id":2,"createdDateTime":1505973073000,"updatedDateTime":1505973073000,"stageName":"Stage 02","stageDesc":"Second stage","stageImageUrl":"/stage_Images/1ab5af45-9633-4835-a722-0a4d3a4ad2bc1505973073054","stageLevel":2},{"id":3,"createdDateTime":1505973073000,"updatedDateTime":1505973073000,"stageName":"Stage 03","stageDesc":"Third stage","stageImageUrl":"/stage_Images/f76f8626-5a80-4737-96f8-829feb99f0201505973073160","stageLevel":3},{"id":4,"createdDateTime":1505973073000,"updatedDateTime":1505973073000,"stageName":"Stage 04","stageDesc":"First stage","stageImageUrl":"/stage_Images/a3f3deff-bfdc-4735-901e-d90c930e5fae1505973073289","stageLevel":4}]},{"id":2,"createdDateTime":1505973483000,"updatedDateTime":1505973483000,"cropName":"Ragi","cropType":"2","cropDesc":"cereal crop","cropImageUrl":"/crop_Images/b08a0ebe-52ff-47a8-9058-f01a343c18891505973483155","stageModel":[{"id":5,"createdDateTime":1505973483000,"updatedDateTime":1505973483000,"stageName":"Stage 01","stageDesc":"First stage","stageImageUrl":"/stage_Images/13a2265f-8050-4436-8216-6572e764bf561505973482781","stageLevel":1},{"id":6,"createdDateTime":1505973483000,"updatedDateTime":1505973483000,"stageName":"Stage 02","stageDesc":"Second stage","stageImageUrl":"/stage_Images/b09cda7c-50b2-47aa-a194-f4b25528577e1505973482890","stageLevel":2},{"id":7,"createdDateTime":1505973483000,"updatedDateTime":1505973483000,"stageName":"Stage 03","stageDesc":"Third stage","stageImageUrl":"/stage_Images/fa37d028-a084-4398-b90b-b9128ce45bc61505973482990","stageLevel":3},{"id":8,"createdDateTime":1505973483000,"updatedDateTime":1505973483000,"stageName":"Stage 04","stageDesc":"Fourth stage","stageImageUrl":"/stage_Images/929cf9ef-5f4e-49d2-adb5-94543dcc07961505973483067","stageLevel":4}]}],"imageBaseUrl":"http://192.168.0.116:8080/images"}
     */

    public CropResponse(){

    }

    private String message;
    private int code;
    private ResponseDataBean responseData;



    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public ResponseDataBean getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseDataBean responseData) {
        this.responseData = responseData;
    }

    public static class ResponseDataBean implements Serializable {
        /**
         * crops : [{"id":1,"createdDateTime":1505973073000,"updatedDateTime":1505973073000,"cropName":"Tomato","cropType":"1","cropDesc":"Vegetable","cropImageUrl":"/crop_Images/96d130a4-aa9b-48e3-8380-5f7f0eba06e31505973073337","stageModel":[{"id":1,"createdDateTime":1505973073000,"updatedDateTime":1505973073000,"stageName":"Stage 01","stageDesc":"First stage","stageImageUrl":"/stage_Images/686d3372-c084-4e34-8717-0f11aaf8c94d1505973072674","stageLevel":1},{"id":2,"createdDateTime":1505973073000,"updatedDateTime":1505973073000,"stageName":"Stage 02","stageDesc":"Second stage","stageImageUrl":"/stage_Images/1ab5af45-9633-4835-a722-0a4d3a4ad2bc1505973073054","stageLevel":2},{"id":3,"createdDateTime":1505973073000,"updatedDateTime":1505973073000,"stageName":"Stage 03","stageDesc":"Third stage","stageImageUrl":"/stage_Images/f76f8626-5a80-4737-96f8-829feb99f0201505973073160","stageLevel":3},{"id":4,"createdDateTime":1505973073000,"updatedDateTime":1505973073000,"stageName":"Stage 04","stageDesc":"First stage","stageImageUrl":"/stage_Images/a3f3deff-bfdc-4735-901e-d90c930e5fae1505973073289","stageLevel":4}]},{"id":2,"createdDateTime":1505973483000,"updatedDateTime":1505973483000,"cropName":"Ragi","cropType":"2","cropDesc":"cereal crop","cropImageUrl":"/crop_Images/b08a0ebe-52ff-47a8-9058-f01a343c18891505973483155","stageModel":[{"id":5,"createdDateTime":1505973483000,"updatedDateTime":1505973483000,"stageName":"Stage 01","stageDesc":"First stage","stageImageUrl":"/stage_Images/13a2265f-8050-4436-8216-6572e764bf561505973482781","stageLevel":1},{"id":6,"createdDateTime":1505973483000,"updatedDateTime":1505973483000,"stageName":"Stage 02","stageDesc":"Second stage","stageImageUrl":"/stage_Images/b09cda7c-50b2-47aa-a194-f4b25528577e1505973482890","stageLevel":2},{"id":7,"createdDateTime":1505973483000,"updatedDateTime":1505973483000,"stageName":"Stage 03","stageDesc":"Third stage","stageImageUrl":"/stage_Images/fa37d028-a084-4398-b90b-b9128ce45bc61505973482990","stageLevel":3},{"id":8,"createdDateTime":1505973483000,"updatedDateTime":1505973483000,"stageName":"Stage 04","stageDesc":"Fourth stage","stageImageUrl":"/stage_Images/929cf9ef-5f4e-49d2-adb5-94543dcc07961505973483067","stageLevel":4}]}]
         * imageBaseUrl : http://192.168.0.116:8080/images
         */

        private String imageBaseUrl;
        private List<CropsBean> crops;

        public String getImageBaseUrl() {
            return imageBaseUrl;
        }

        public void setImageBaseUrl(String imageBaseUrl) {
            this.imageBaseUrl = imageBaseUrl;
        }

        public List<CropsBean> getCrops() {
            return crops;
        }

        public void setCrops(List<CropsBean> crops) {
            this.crops = crops;
        }

        public static class CropsBean implements Serializable {
            /**
             * id : 1
             * createdDateTime : 1505973073000
             * updatedDateTime : 1505973073000
             * cropName : Tomato
             * cropType : 1
             * cropDesc : Vegetable
             * cropImageUrl : /crop_Images/96d130a4-aa9b-48e3-8380-5f7f0eba06e31505973073337
             * stageModel : [{"id":1,"createdDateTime":1505973073000,"updatedDateTime":1505973073000,"stageName":"Stage 01","stageDesc":"First stage","stageImageUrl":"/stage_Images/686d3372-c084-4e34-8717-0f11aaf8c94d1505973072674","stageLevel":1},{"id":2,"createdDateTime":1505973073000,"updatedDateTime":1505973073000,"stageName":"Stage 02","stageDesc":"Second stage","stageImageUrl":"/stage_Images/1ab5af45-9633-4835-a722-0a4d3a4ad2bc1505973073054","stageLevel":2},{"id":3,"createdDateTime":1505973073000,"updatedDateTime":1505973073000,"stageName":"Stage 03","stageDesc":"Third stage","stageImageUrl":"/stage_Images/f76f8626-5a80-4737-96f8-829feb99f0201505973073160","stageLevel":3},{"id":4,"createdDateTime":1505973073000,"updatedDateTime":1505973073000,"stageName":"Stage 04","stageDesc":"First stage","stageImageUrl":"/stage_Images/a3f3deff-bfdc-4735-901e-d90c930e5fae1505973073289","stageLevel":4}]
             */

            private int id;
            private long createdDateTime;
            private long updatedDateTime;
            private String cropName;
            private String cropType;
            private String cropDesc;
            private String cropImageUrl;
            private String cropPDFUrl;
            private String cropPlaystoreLink;
            private List<StageModelBean> stageModel;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public long getCreatedDateTime() {
                return createdDateTime;
            }

            public void setCreatedDateTime(long createdDateTime) {
                this.createdDateTime = createdDateTime;
            }

            public long getUpdatedDateTime() {
                return updatedDateTime;
            }

            public void setUpdatedDateTime(long updatedDateTime) {
                this.updatedDateTime = updatedDateTime;
            }

            public String getCropName() {
                return cropName;
            }

            public void setCropName(String cropName) {
                this.cropName = cropName;
            }

            public String getCropType() {
                return cropType;
            }

            public void setCropType(String cropType) {
                this.cropType = cropType;
            }

            public String getCropDesc() {
                return cropDesc;
            }

            public void setCropDesc(String cropDesc) {
                this.cropDesc = cropDesc;
            }

            public String getCropImageUrl() {
                return cropImageUrl;
            }

            public void setCropImageUrl(String cropImageUrl) {
                this.cropImageUrl = cropImageUrl;
            }

            public String getCropPDFUrl() {
                return cropPDFUrl;
            }

            public void setCropPDFUrl(String cropPDFUrl) {
                this.cropPDFUrl = cropPDFUrl;
            }

            public String getCropPlaystoreLink() {
                return cropPlaystoreLink;
            }

            public void setCropPlaystoreLink(String cropPlaystoreLink) {
                this.cropPlaystoreLink = cropPlaystoreLink;
            }

            public List<StageModelBean> getStageModel() {
                return stageModel;
            }

            public void setStageModel(List<StageModelBean> stageModel) {
                this.stageModel = stageModel;
            }

            public static class StageModelBean implements Serializable {
                /**
                 * id : 1
                 * createdDateTime : 1505973073000
                 * updatedDateTime : 1505973073000
                 * stageName : Stage 01
                 * stageDesc : First stage
                 * stageImageUrl : /stage_Images/686d3372-c084-4e34-8717-0f11aaf8c94d1505973072674
                 * stageLevel : 1
                 */

                private int id;
                private long createdDateTime;
                private long updatedDateTime;
                private String stageName;
                private String stageDesc;
                private String stageImageUrl;
                private int stageLevel;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public long getCreatedDateTime() {
                    return createdDateTime;
                }

                public void setCreatedDateTime(long createdDateTime) {
                    this.createdDateTime = createdDateTime;
                }

                public long getUpdatedDateTime() {
                    return updatedDateTime;
                }

                public void setUpdatedDateTime(long updatedDateTime) {
                    this.updatedDateTime = updatedDateTime;
                }

                public String getStageName() {
                    return stageName;
                }

                public void setStageName(String stageName) {
                    this.stageName = stageName;
                }

                public String getStageDesc() {
                    return stageDesc;
                }

                public void setStageDesc(String stageDesc) {
                    this.stageDesc = stageDesc;
                }

                public String getStageImageUrl() {
                    return stageImageUrl;
                }

                public void setStageImageUrl(String stageImageUrl) {
                    this.stageImageUrl = stageImageUrl;
                }

                public int getStageLevel() {
                    return stageLevel;
                }

                public void setStageLevel(int stageLevel) {
                    this.stageLevel = stageLevel;
                }
            }
        }
    }
}
