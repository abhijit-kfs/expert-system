package com.kfs.expertsystems.models;

/**
 * Created by Tomesh on 06-10-2017.
 */

public class AddPasswordRequest {

    private String password;
    private String uid;

    public AddPasswordRequest(String password, String uid) {
        this.password = password;
        this.uid = uid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
