package com.kfs.expertsystems.models;

/**
 * Created by Tomesh on 19-09-2017.
 */
public class UserResponse extends  BaseResponse{


    /**
     * responseData : {"errorMsg":"Registration Success","userId":"b06072e5-145a-4958-ac8b-bd4df60d07b8"}
     * code : 200
     */


    private ResponseDataBean responseData;

    public ResponseDataBean getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseDataBean responseData) {
        this.responseData = responseData;
    }

    public static class ResponseDataBean {
        /**
         * errorMsg : Registration Success
         * userId : b06072e5-145a-4958-ac8b-bd4df60d07b8
         */

        private String errorMsg;
        private String userId;
        private int passwordExists;
        private String userType;

        public String getErrorMsg() {
            return errorMsg;
        }

        public int getPasswordExists() {
            return passwordExists;
        }

        public String getUserType() {
            return userType;
        }

        public void setErrorMsg(String errorMsg) {
            this.errorMsg = errorMsg;
        }

        public String getUserId() {
            return userId;
        }

        public void setPasswordExists(int passwordExists) {
            this.passwordExists = passwordExists;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }
    }
}