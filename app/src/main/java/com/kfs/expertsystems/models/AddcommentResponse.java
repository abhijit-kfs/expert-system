package com.kfs.expertsystems.models;

import java.util.List;

/**
 * Created by Tomesh on 28-09-2017.
 */

public class AddcommentResponse {

    /**
     * message : success
     * code : 200
     * responseData : {"comments":[{"id":4,"createdDateTime":1506582004000,"updatedDateTime":1506582004000,"comDesc":"This is a tomato problem. Please explain the situation and causes. I may help you with the solution","gallery":[{"id":25,"imageUrl":"/enquries_image/e751b272-1fc9-41c4-94ad-64f0dc88a4581506582003730"}],"userType":1,"enquiriesId":5},{"id":11,"createdDateTime":1506597290000,"updatedDateTime":1506597290000,"comDesc":"Last last ","gallery":[{"id":38,"imageUrl":"/enquries_image/d97e8e02-f7fe-4881-a0b7-ef85ca95f3bf1506597289859"}],"userType":0,"enquiriesId":5},{"id":9,"createdDateTime":1506591492000,"updatedDateTime":1506591492000,"comDesc":"The problem ","gallery":[{"id":36,"imageUrl":"/enquries_image/5f88b3cd-1377-49ce-8d89-eeb09771efda1506591492478"}],"userType":0,"enquiriesId":5},{"id":5,"createdDateTime":1506582384000,"updatedDateTime":1506582384000,"comDesc":"This is a tomato problem. Please explain the situation and causes. I may help you with the solution","gallery":[{"id":26,"imageUrl":"/enquries_image/8b455fa1-0be9-416d-9fee-d9737a241c4c1506582383639"}],"userType":1,"enquiriesId":5},{"id":3,"createdDateTime":1506580689000,"updatedDateTime":1506580689000,"comDesc":"string","gallery":[{"id":24,"imageUrl":"/enquries_image/742c09b8-b96b-482d-a991-2eab659902b51506580688806"}],"userType":0,"enquiriesId":5},{"id":6,"createdDateTime":1506582709000,"updatedDateTime":1506582709000,"comDesc":"This is a tomato problem. Please explain","gallery":[{"id":31,"imageUrl":"/enquries_image/a37d5b86-a455-4fd3-896d-d902539caad91506582708937"}],"userType":0,"enquiriesId":5},{"id":7,"createdDateTime":1506585431000,"updatedDateTime":1506585431000,"comDesc":"The problem was I have been told I need a new phone for this time to help my dad out here with a new one please and help us please follow back and please help me ","gallery":[{"id":33,"imageUrl":"/enquries_image/8c282139-b0ad-451f-b45c-b09dfdc0bf211506585431126"},{"id":32,"imageUrl":"/enquries_image/6ccd8a84-0fd3-47bf-9884-26fb401f6ba01506585430950"}],"userType":0,"enquiriesId":5},{"id":10,"createdDateTime":1506597078000,"updatedDateTime":1506597078000,"comDesc":"Last","gallery":[{"id":37,"imageUrl":"/enquries_image/58b00d43-a559-4c13-bf55-a9213c71e0eb1506597078164"}],"userType":0,"enquiriesId":5},{"id":8,"createdDateTime":1506591080000,"updatedDateTime":1506591080000,"comDesc":"Admin ","gallery":[{"id":35,"imageUrl":"/enquries_image/54dd9dfe-d11d-42c3-b580-d9338c251b381506591079575"}],"userType":0,"enquiriesId":5},{"id":12,"createdDateTime":1506599825000,"updatedDateTime":1506599825000,"comDesc":"afjl;dakl;sdfj;lsjfuhfilsfhilsudgysdgyfgliggsigilssfdvfdfdvfdv","gallery":[{"id":43,"imageUrl":"/enquries_image/56e0a3ac-5d54-4891-9f97-af74bf0e53d41506599825478"}],"userType":0,"enquiriesId":5}],"msg":"Comments Added SuccessFully.","baseUrl":"http://192.168.0.109:8080/images"}
     */

    private String message;
    private int code;
    private ResponseDataBean responseData;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public ResponseDataBean getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseDataBean responseData) {
        this.responseData = responseData;
    }

    public static class ResponseDataBean {
        /**
         * comments : [{"id":4,"createdDateTime":1506582004000,"updatedDateTime":1506582004000,"comDesc":"This is a tomato problem. Please explain the situation and causes. I may help you with the solution","gallery":[{"id":25,"imageUrl":"/enquries_image/e751b272-1fc9-41c4-94ad-64f0dc88a4581506582003730"}],"userType":1,"enquiriesId":5},{"id":11,"createdDateTime":1506597290000,"updatedDateTime":1506597290000,"comDesc":"Last last ","gallery":[{"id":38,"imageUrl":"/enquries_image/d97e8e02-f7fe-4881-a0b7-ef85ca95f3bf1506597289859"}],"userType":0,"enquiriesId":5},{"id":9,"createdDateTime":1506591492000,"updatedDateTime":1506591492000,"comDesc":"The problem ","gallery":[{"id":36,"imageUrl":"/enquries_image/5f88b3cd-1377-49ce-8d89-eeb09771efda1506591492478"}],"userType":0,"enquiriesId":5},{"id":5,"createdDateTime":1506582384000,"updatedDateTime":1506582384000,"comDesc":"This is a tomato problem. Please explain the situation and causes. I may help you with the solution","gallery":[{"id":26,"imageUrl":"/enquries_image/8b455fa1-0be9-416d-9fee-d9737a241c4c1506582383639"}],"userType":1,"enquiriesId":5},{"id":3,"createdDateTime":1506580689000,"updatedDateTime":1506580689000,"comDesc":"string","gallery":[{"id":24,"imageUrl":"/enquries_image/742c09b8-b96b-482d-a991-2eab659902b51506580688806"}],"userType":0,"enquiriesId":5},{"id":6,"createdDateTime":1506582709000,"updatedDateTime":1506582709000,"comDesc":"This is a tomato problem. Please explain","gallery":[{"id":31,"imageUrl":"/enquries_image/a37d5b86-a455-4fd3-896d-d902539caad91506582708937"}],"userType":0,"enquiriesId":5},{"id":7,"createdDateTime":1506585431000,"updatedDateTime":1506585431000,"comDesc":"The problem was I have been told I need a new phone for this time to help my dad out here with a new one please and help us please follow back and please help me ","gallery":[{"id":33,"imageUrl":"/enquries_image/8c282139-b0ad-451f-b45c-b09dfdc0bf211506585431126"},{"id":32,"imageUrl":"/enquries_image/6ccd8a84-0fd3-47bf-9884-26fb401f6ba01506585430950"}],"userType":0,"enquiriesId":5},{"id":10,"createdDateTime":1506597078000,"updatedDateTime":1506597078000,"comDesc":"Last","gallery":[{"id":37,"imageUrl":"/enquries_image/58b00d43-a559-4c13-bf55-a9213c71e0eb1506597078164"}],"userType":0,"enquiriesId":5},{"id":8,"createdDateTime":1506591080000,"updatedDateTime":1506591080000,"comDesc":"Admin ","gallery":[{"id":35,"imageUrl":"/enquries_image/54dd9dfe-d11d-42c3-b580-d9338c251b381506591079575"}],"userType":0,"enquiriesId":5},{"id":12,"createdDateTime":1506599825000,"updatedDateTime":1506599825000,"comDesc":"afjl;dakl;sdfj;lsjfuhfilsfhilsudgysdgyfgliggsigilssfdvfdfdvfdv","gallery":[{"id":43,"imageUrl":"/enquries_image/56e0a3ac-5d54-4891-9f97-af74bf0e53d41506599825478"}],"userType":0,"enquiriesId":5}]
         * msg : Comments Added SuccessFully.
         * baseUrl : http://192.168.0.109:8080/images
         */

        private String msg;
        private String baseUrl;
        private List<CommentsBean> comments;

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getBaseUrl() {
            return baseUrl;
        }

        public void setBaseUrl(String baseUrl) {
            this.baseUrl = baseUrl;
        }

        public List<CommentsBean> getComments() {
            return comments;
        }

        public void setComments(List<CommentsBean> comments) {
            this.comments = comments;
        }

        public static class CommentsBean {
            /**
             * id : 4
             * createdDateTime : 1506582004000
             * updatedDateTime : 1506582004000
             * comDesc : This is a tomato problem. Please explain the situation and causes. I may help you with the solution
             * gallery : [{"id":25,"imageUrl":"/enquries_image/e751b272-1fc9-41c4-94ad-64f0dc88a4581506582003730"}]
             * userType : 1
             * enquiriesId : 5
             */

            private int id;
            private long createdDateTime;
            private long updatedDateTime;
            private String comDesc;
            private int userType;
            private int enquiriesId;
            private List<GalleryBean> gallery;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public long getCreatedDateTime() {
                return createdDateTime;
            }

            public void setCreatedDateTime(long createdDateTime) {
                this.createdDateTime = createdDateTime;
            }

            public long getUpdatedDateTime() {
                return updatedDateTime;
            }

            public void setUpdatedDateTime(long updatedDateTime) {
                this.updatedDateTime = updatedDateTime;
            }

            public String getComDesc() {
                return comDesc;
            }

            public void setComDesc(String comDesc) {
                this.comDesc = comDesc;
            }

            public int getUserType() {
                return userType;
            }

            public void setUserType(int userType) {
                this.userType = userType;
            }

            public int getEnquiriesId() {
                return enquiriesId;
            }

            public void setEnquiriesId(int enquiriesId) {
                this.enquiriesId = enquiriesId;
            }

            public List<GalleryBean> getGallery() {
                return gallery;
            }

            public void setGallery(List<GalleryBean> gallery) {
                this.gallery = gallery;
            }

            public static class GalleryBean {
                /**
                 * id : 25
                 * imageUrl : /enquries_image/e751b272-1fc9-41c4-94ad-64f0dc88a4581506582003730
                 */

                private int id;
                private String imageUrl;
                private String videoUrl;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getImageUrl() {
                    return imageUrl;
                }

                public void setImageUrl(String imageUrl) {
                    this.imageUrl = imageUrl;
                }

                public String getVideoUrl() {
                    return videoUrl;
                }

                public void setVideoUrl(String videoUrl) {
                    this.videoUrl = videoUrl;
                }
            }
        }
    }
}
