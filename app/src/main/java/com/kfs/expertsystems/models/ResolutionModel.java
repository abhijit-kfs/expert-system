package com.kfs.expertsystems.models;

import java.io.Serializable;

public class ResolutionModel implements Serializable{

    private String status;

    private int problemId;

    private String desc;

    private long createdDate;

    private String cropName;

    private String stageName;

    private int cropId;

    private int stageId;

    private String cropImage;

    private String baseURL;

    private ResolutionResponse.ResponseDataBean.ResolutionListBean responseDataBean;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getProblemId() {
        return problemId;
    }

    public void setProblemId(int problemId) {
        this.problemId = problemId;
    }

    public long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(long createdDate) {
        this.createdDate = createdDate;
    }

    public String getCropName() {
        return cropName;
    }

    public void setCropName(String cropName) {
        this.cropName = cropName;
    }

    public String getStageName() {
        return stageName;
    }

    public void setStageName(String stageName) {
        this.stageName = stageName;
    }

    public int getCropId() {
        return cropId;
    }

    public void setCropId(int cropId) {
        this.cropId = cropId;
    }

    public int getStageId() {
        return stageId;
    }

    public void setStageId(int stageId) {
        this.stageId = stageId;
    }

    public ResolutionModel() {
    }

    public ResolutionModel(String status) {
        this.status = status;
    }

    public ResolutionResponse.ResponseDataBean.ResolutionListBean getResponseDataBean() {
        return responseDataBean;
    }

    public void setResponseDataBean(ResolutionResponse.ResponseDataBean.ResolutionListBean responseDataBean) {
        this.responseDataBean = responseDataBean;
    }

    public String getCropImage() {
        return cropImage;
    }

    public void setCropImage(String cropImage) {
        this.cropImage = cropImage;
    }

    public String getBaseURL() {
        return baseURL;
    }

    public void setBaseURL(String baseURL) {
        this.baseURL = baseURL;
    }
}
