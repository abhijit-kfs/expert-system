package com.kfs.expertsystems.models;

/**
 * Created by Tomesh on 06-10-2017.
 */

public class ChangePasswordRequest {


    /**
     * newPasswd : qwerty
     * oldPasswd : 1111
     * uid : 408f583f-b82a-42eb-a22a-5172f80c0c67
     */

    private String newPasswd;
    private String oldPasswd;
    private String confirmNewPassword;
    private String uid;

    public String getNewPasswd() {
        return newPasswd;
    }

    public void setNewPasswd(String newPasswd) {
        this.newPasswd = newPasswd;
    }

    public String getOldPasswd() {
        return oldPasswd;
    }

    public void setOldPasswd(String oldPasswd) {
        this.oldPasswd = oldPasswd;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getConfirmNewPassword() {
        return confirmNewPassword;
    }

    public void setConfirmNewPassword(String confirmNewPassword) {
        this.confirmNewPassword = confirmNewPassword;
    }
}
