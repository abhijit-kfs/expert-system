package com.kfs.expertsystems.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Tomesh on 11-10-2017.
 */

public class NewsResponse implements Serializable {


    /**
     * message : Success
     * code : 200
     * responseData : {"news":[{"id":1,"createdDateTime":1507534545000,"updatedDateTime":1507534545000,"newsTitle":"Taj tak","news_Discription":"asdasdaslkjafoidjoidshjfsdjkhfliudshlvlufhvlufidhlvfduihvlfduihlsiuhlus","image":"/news_image/e80e2f20-d83b-448a-a087-54ba680f83991507534545036"}],"baseUrl":"http://54.169.217.51:8080/images"}
     */

    private String message;
    private int code;
    private ResponseDataBean responseData;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public ResponseDataBean getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseDataBean responseData) {
        this.responseData = responseData;
    }

    public static class ResponseDataBean implements Serializable{
        /**
         * news : [{"id":1,"createdDateTime":1507534545000,"updatedDateTime":1507534545000,"newsTitle":"Taj tak","news_Discription":"asdasdaslkjafoidjoidshjfsdjkhfliudshlvlufhvlufidhlvfduihvlfduihlsiuhlus","image":"/news_image/e80e2f20-d83b-448a-a087-54ba680f83991507534545036"}]
         * baseUrl : http://54.169.217.51:8080/images
         */

        private String baseUrl;
        private List<NewsBean> news;

        public String getBaseUrl() {
            return baseUrl;
        }

        public void setBaseUrl(String baseUrl) {
            this.baseUrl = baseUrl;
        }

        public List<NewsBean> getNews() {
            return news;
        }

        public void setNews(List<NewsBean> news) {
            this.news = news;
        }

        public static class NewsBean implements Serializable{
            /**
             * id : 1
             * createdDateTime : 1507534545000
             * updatedDateTime : 1507534545000
             * newsTitle : Taj tak
             * news_Discription : asdasdaslkjafoidjoidshjfsdjkhfliudshlvlufhvlufidhlvfduihvlfduihlsiuhlus
             * image : /news_image/e80e2f20-d83b-448a-a087-54ba680f83991507534545036
             */

            private int id;
            private long createdDateTime;
            private long updatedDateTime;
            private String newsTitle;
            private String news_Discription;
            private String image;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public long getCreatedDateTime() {
                return createdDateTime;
            }

            public void setCreatedDateTime(long createdDateTime) {
                this.createdDateTime = createdDateTime;
            }

            public long getUpdatedDateTime() {
                return updatedDateTime;
            }

            public void setUpdatedDateTime(long updatedDateTime) {
                this.updatedDateTime = updatedDateTime;
            }

            public String getNewsTitle() {
                return newsTitle;
            }

            public void setNewsTitle(String newsTitle) {
                this.newsTitle = newsTitle;
            }

            public String getNews_Discription() {
                return news_Discription;
            }

            public void setNews_Discription(String news_Discription) {
                this.news_Discription = news_Discription;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }
        }
    }
}
