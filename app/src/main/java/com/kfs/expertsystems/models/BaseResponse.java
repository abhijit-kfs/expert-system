package com.kfs.expertsystems.models;

/**
 * Created by Tomesh on 19-09-2017.
 */

public class BaseResponse {

/*
     "message": "SUCCESS",
             "code": 200
*/

    private String message;

    private String code;



    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
