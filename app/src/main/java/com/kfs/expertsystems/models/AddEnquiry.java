package com.kfs.expertsystems.models;

import java.util.List;

/**
 * Created by Tomesh on 26-09-2017.
 */

public class AddEnquiry {


    /**
     * cropId : string
     * enquiriesDesc : string
     * enquiriesImages : [{"image":"string"}]
     * enquiriesVideos : [{"video":"string"}]
     * stageId : string
     * status : string
     * userId : string
     */

    private String cropId;
    private String enquiriesDesc;
    private String stageId;
    private String status;
    private String userId;
    private String latitude;
    private String longitude;
    private List<EnquiriesImagesBean> enquiriesImages;
    private List<EnquiriesVideosBean> enquiriesVideos;

    public String getCropId() {
        return cropId;
    }

    public void setCropId(String cropId) {
        this.cropId = cropId;
    }

    public String getEnquiriesDesc() {
        return enquiriesDesc;
    }

    public void setEnquiriesDesc(String enquiriesDesc) {
        this.enquiriesDesc = enquiriesDesc;
    }

    public String getStageId() {
        return stageId;
    }

    public void setStageId(String stageId) {
        this.stageId = stageId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public List<EnquiriesImagesBean> getEnquiriesImages() {
        return enquiriesImages;
    }

    public void setEnquiriesImages(List<EnquiriesImagesBean> enquiriesImages) {
        this.enquiriesImages = enquiriesImages;
    }

    public List<EnquiriesVideosBean> getEnquiriesVideos() {
        return enquiriesVideos;
    }

    public void setEnquiriesVideos(List<EnquiriesVideosBean> enquiriesVideos) {
        this.enquiriesVideos = enquiriesVideos;
    }

    public static class EnquiriesImagesBean {
        /**
         * image : string
         */

        private String image;

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }

    public static class EnquiriesVideosBean {
        /**
         * video : string
         */

        private String video;

        public String getVideo() {
            return video;
        }

        public void setVideo(String video) {
            this.video = video;
        }
    }
}
