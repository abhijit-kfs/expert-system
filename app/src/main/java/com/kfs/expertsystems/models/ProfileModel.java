package com.kfs.expertsystems.models;

/**
 * Created by Tomesh on 05-10-2017.
 */

public class ProfileModel {


    /**
     * message : SUCCESS
     * code : 200
     * responseData : {"password":"1111","emailId":"s@g.com","firstName":"Swaroop G S","phoneNumber":9423535324,"address":"vijayanagar, ancenvneov envi,v.venviev,above e the only thing I would like to do is to get a screen ","uid":"37d3568a-b162-480a-b8e1-4906c1e9652d","profileImage":"/profile_image/a643e0db-f45f-4023-ad0d-63124f1149081507123938575","baseUrl":"http://192.168.2.10:8080/images"}
     */

    private String message;
    private int code;
    private ResponseDataBean responseData;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public ResponseDataBean getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseDataBean responseData) {
        this.responseData = responseData;
    }

    public static class ResponseDataBean {
        /**
         * password : 1111
         * emailId : s@g.com
         * firstName : Swaroop G S
         * phoneNumber : 9423535324
         * address : vijayanagar, ancenvneov envi,v.venviev,above e the only thing I would like to do is to get a screen
         * uid : 37d3568a-b162-480a-b8e1-4906c1e9652d
         * profileImage : /profile_image/a643e0db-f45f-4023-ad0d-63124f1149081507123938575
         * baseUrl : http://192.168.2.10:8080/images
         */

        private String password;
        private String emailId;
        private String firstName;
        private long phoneNumber;
        private String address;
        private String uid;
        private String profileImage;
        private boolean isImageChanged = false;
        private String baseUrl;

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getEmailId() {
            return emailId;
        }

        public void setEmailId(String emailId) {
            this.emailId = emailId;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public long getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(long phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getProfileImage() {
            return profileImage;
        }

        public void setProfileImage(String profileImage) {
            this.profileImage = profileImage;
        }

        public String getBaseUrl() {
            return baseUrl;
        }

        public void setBaseUrl(String baseUrl) {
            this.baseUrl = baseUrl;
        }

        public boolean isImageChanged() {
            return isImageChanged;
        }

        public void setImageChanged(boolean imageChanged) {
            isImageChanged = imageChanged;
        }
    }
}
