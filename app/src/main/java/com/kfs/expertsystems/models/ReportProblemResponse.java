package com.kfs.expertsystems.models;

/**
 * Created by Tomesh on 27-09-2017.
 */

public class ReportProblemResponse {


    /**
     * message : success
     * code : 200
     * responseData : {"problemId":"CRPID7","msg":"Data Saved Successfully."}
     */

    private String message;
    private int code;
    private ResponseDataBean responseData;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public ResponseDataBean getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseDataBean responseData) {
        this.responseData = responseData;
    }

    public static class ResponseDataBean {
        /**
         * problemId : CRPID7
         * msg : Data Saved Successfully.
         */

        private String problemId;
        private String msg;

        public String getProblemId() {
            return problemId;
        }

        public void setProblemId(String problemId) {
            this.problemId = problemId;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }
    }
}
