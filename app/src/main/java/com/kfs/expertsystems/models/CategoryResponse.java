package com.kfs.expertsystems.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by shylesh on 20/04/18.
 */

public class CategoryResponse {

    /**
     * message : Success
     * code : 200
     * responseData : [{"id":1,"productCategoryName":"Insecticides"},{"id":2,"productCategoryName":"Fungicides"},{"id":3,"productCategoryName":"Herbicides"},{"id":4,"productCategoryName":"Seed Treatment Products"},{"id":5,"productCategoryName":"Plant Nutrient Products"},{"id":6,"productCategoryName":"Animal Health Products"},{"id":7,"productCategoryName":"Public Health Insecticides"},{"id":8,"productCategoryName":"Others"}]
     */

    private String message;
    private int code;
    private List<ResponseDataBean> responseData;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<ResponseDataBean> getResponseData() {
        return responseData;
    }

    public void setResponseData(List<ResponseDataBean> responseData) {
        this.responseData = responseData;
    }

    public static class ResponseDataBean implements Parcelable {
        /**
         * id : 1
         * productCategoryName : Insecticides
         */

        private int id;
        private String productCategoryName;
        private List<GalleryCategoryBean> gallery;

        protected ResponseDataBean(Parcel in) {
            id = in.readInt();
            productCategoryName = in.readString();
            gallery = in.createTypedArrayList(GalleryCategoryBean.CREATOR);
        }

        public static final Creator<ResponseDataBean> CREATOR = new Creator<ResponseDataBean>() {
            @Override
            public ResponseDataBean createFromParcel(Parcel in) {
                return new ResponseDataBean(in);
            }

            @Override
            public ResponseDataBean[] newArray(int size) {
                return new ResponseDataBean[size];
            }
        };

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getProductCategoryName() {
            return productCategoryName;
        }

        public void setProductCategoryName(String productCategoryName) {
            this.productCategoryName = productCategoryName;
        }

        public List<GalleryCategoryBean> getGallery() {
            return gallery;
        }

        public void setGallery(List<GalleryCategoryBean> gallery) {
            this.gallery = gallery;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(id);
            parcel.writeString(productCategoryName);
            parcel.writeTypedList(gallery);
        }

        public static class GalleryCategoryBean implements Parcelable {

            private int id;
            private String pdfUrl;
            private String pdfName;

            public static final Creator<GalleryCategoryBean> CREATOR = new Creator<GalleryCategoryBean>() {
                @Override
                public GalleryCategoryBean createFromParcel(Parcel in) {
                    return new GalleryCategoryBean(in);
                }

                @Override
                public GalleryCategoryBean[] newArray(int size) {
                    return new GalleryCategoryBean[size];
                }
            };

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getPdfUrl() {
                return pdfUrl;
            }

            public void setPdfUrl(String pdfUrl) {
                this.pdfUrl = pdfUrl;
            }

            public String getPdfName() {
                return pdfName;
            }

            public void setPdfName(String pdfName) {
                this.pdfName = pdfName;
            }

            protected GalleryCategoryBean(Parcel in) {
                id = in.readInt();
                pdfUrl = in.readString();
                pdfName = in.readString();
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel parcel, int flags) {
                parcel.writeInt(id);
                parcel.writeString(pdfUrl);
                parcel.writeString(pdfName);
            }
        }
    }
}
