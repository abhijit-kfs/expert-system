package com.kfs.expertsystems.models;

/**
 * Created by Tomesh on 19-09-2017.
 */

public class UserRequest {


    /**
     * address : string
     * email : string
     * firstName : string
     * loginType : string
     * password : string
     * phoneNumber : 0
     * city : string
     * facebookLogin : 0
     * phoneNumber : 0
     */

    private String address;
    private String email;
    private String firstName;
    private String loginType;
    private String password;
    private long phoneNumber;
    private String confirmPasswd;
    private String userName;
    private String userType;
    private String city;
    private int facebookLogin;

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public int getFacebookLogin() {
        return facebookLogin;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFacebookLogin(int facebookLogin) {
        this.facebookLogin = facebookLogin;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getConfirmPasswd() {
        return confirmPasswd;
    }

    public void setConfirmPasswd(String confirmPasswd) {
        this.confirmPasswd = confirmPasswd;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }


}
