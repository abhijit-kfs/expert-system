package com.kfs.expertsystems.models;

import java.util.List;

/**
 * Created by Tomesh on 28-09-2017.
 */

public class AddComments {


    /**
     * commentsImages : [{"image":"string"}]
     * commentsVideos : [{"video":"string"}]
     * description : string
     * enquiryId : 0
     * userType : 0
     */

    private String description;
    private int enquiryId;
    private int userType;
    private List<CommentsImagesBean> commentsImages;
    private List<CommentsVideosBean> commentsVideos;
    private String userId;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getEnquiryId() {
        return enquiryId;
    }

    public void setEnquiryId(int enquiryId) {
        this.enquiryId = enquiryId;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public List<CommentsImagesBean> getCommentsImages() {
        return commentsImages;
    }

    public void setCommentsImages(List<CommentsImagesBean> commentsImages) {
        this.commentsImages = commentsImages;
    }

    public List<CommentsVideosBean> getCommentsVideos() {
        return commentsVideos;
    }

    public void setCommentsVideos(List<CommentsVideosBean> commentsVideos) {
        this.commentsVideos = commentsVideos;
    }

    public static class CommentsImagesBean {
        /**
         * image : string
         */

        private String image;

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }

    public static class CommentsVideosBean {
        /**
         * video : string
         */

        private String video;

        public String getVideo() {
            return video;
        }

        public void setVideo(String video) {
            this.video = video;
        }
    }
}
