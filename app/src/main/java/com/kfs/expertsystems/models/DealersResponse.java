package com.kfs.expertsystems.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Tomesh on 06-10-2017.
 */

public class DealersResponse implements Serializable {


    /**
     * message : Success
     * code : 200
     * responseData : {"dealer":[{"id":1,"createdDateTime":1507296914000,"updatedDateTime":1507296914000,"dealerName":"delear tur","dealerNumber":"asdasd","dealerAddress":"bangalore","dealerType":"striasdsadng","dealerLat":0,"dealerLong":0,"pestisides":true,"fungicides":true,"herbicides":false,"others":"asdasda"}]}
     */

    private String message;
    private int code;
    private ResponseDataBean responseData;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public ResponseDataBean getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseDataBean responseData) {
        this.responseData = responseData;
    }

    public static class ResponseDataBean implements Serializable {
        private List<DealerBean> dealer;

        public List<DealerBean> getDealer() {
            return dealer;
        }

        public void setDealer(List<DealerBean> dealer) {
            this.dealer = dealer;
        }

        public static class DealerBean implements Serializable {
            /**
             * id : 1
             * createdDateTime : 1507296914000
             * updatedDateTime : 1507296914000
             * dealerName : delear tur
             * dealerNumber : asdasd
             * dealerAddress : bangalore
             * dealerType : striasdsadng
             * dealerLat : 0
             * dealerLong : 0
             * pestisides : true
             * fungicides : true
             * herbicides : false
             * others : asdasda
             */

            private int id;
            private long createdDateTime;
            private long updatedDateTime;
            private String dealerName;
            private String dealerNumber;
            private String dealerAddress;
            private String dealerType;
            private String dealerCompanyName;
            private double dealerLat;
            private double dealerLong;
            private boolean pestisides;
            private boolean fungicides;
            private boolean herbicides;
            private String others;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public long getCreatedDateTime() {
                return createdDateTime;
            }

            public void setCreatedDateTime(long createdDateTime) {
                this.createdDateTime = createdDateTime;
            }

            public long getUpdatedDateTime() {
                return updatedDateTime;
            }

            public void setUpdatedDateTime(long updatedDateTime) {
                this.updatedDateTime = updatedDateTime;
            }

            public String getDealerName() {
                return dealerName;
            }

            public String getDealerCompanyName() {
                return dealerCompanyName;
            }

            public void setDealerCompanyName(String dealerCompanyName) {
                this.dealerCompanyName = dealerCompanyName;
            }

            public void setDealerName(String dealerName) {
                this.dealerName = dealerName;
            }

            public String getDealerNumber() {
                return dealerNumber;
            }

            public void setDealerNumber(String dealerNumber) {
                this.dealerNumber = dealerNumber;
            }

            public String getDealerAddress() {
                return dealerAddress;
            }

            public void setDealerAddress(String dealerAddress) {
                this.dealerAddress = dealerAddress;
            }

            public String getDealerType() {
                return dealerType;
            }

            public void setDealerType(String dealerType) {
                this.dealerType = dealerType;
            }

            public double getDealerLat() {
                return dealerLat;
            }

            public void setDealerLat(double dealerLat) {
                this.dealerLat = dealerLat;
            }

            public double getDealerLong() {
                return dealerLong;
            }

            public void setDealerLong(double dealerLong) {
                this.dealerLong = dealerLong;
            }

            public boolean isPestisides() {
                return pestisides;
            }

            public void setPestisides(boolean pestisides) {
                this.pestisides = pestisides;
            }

            public boolean isFungicides() {
                return fungicides;
            }

            public void setFungicides(boolean fungicides) {
                this.fungicides = fungicides;
            }

            public boolean isHerbicides() {
                return herbicides;
            }

            public void setHerbicides(boolean herbicides) {
                this.herbicides = herbicides;
            }

            public String getOthers() {
                return others;
            }

            public void setOthers(String others) {
                this.others = others;
            }
        }
    }
}
