package com.kfs.expertsystems.models;

/**
 * Created by Tomesh on 28-09-2017.
 */

public class ResolvedModel {

    /**
     * enquiryId : 0
     * status : string
     */

    private int enquiryId;
    private String status;

    public int getEnquiryId() {
        return enquiryId;
    }

    public void setEnquiryId(int enquiryId) {
        this.enquiryId = enquiryId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
