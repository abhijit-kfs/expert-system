package com.kfs.expertsystems.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by shylesh on 17/04/18.
 */

public class ProductsResponse implements Parcelable {
    /**
     * message : Success
     * code : 200
     * responseData : {"products":[{"id":1,"productCategory":{"id":1,"productCategoryName":"Insecticides"},"productName":"Safer Caterpillar Killer","productUnit":"300ml","productQuantity":"1","productImage":"/crop_Images/96251f33-35cc-44d2-a6b9-7029ca5b169b1523448809330","productPrice":"500"},{"id":2,"productCategory":{"id":2,"productCategoryName":"Fungicides"},"productName":"Product2","productUnit":"300ml","productQuantity":"1","productImage":"7029ca5b169b1523448809330","productPrice":"500"},{"id":3,"productCategory":{"id":3,"productCategoryName":"Herbicides"},"productName":"product3","productUnit":"300ml","productQuantity":"1","productImage":"7029ca5b169b1523448809330","productPrice":"500"},{"id":4,"productCategory":{"id":4,"productCategoryName":"Seed Treatment Products"},"productName":"product4","productQuantity":"1","productImage":"7029ca5b169b1523448809330","productPrice":"500"},{"id":7,"createdDateTime":1523628700000,"productCategory":{"id":1,"productCategoryName":"Insecticides"},"productName":"string","productDescription":"string","productUnit":"string","productQuantity":"string","productImage":"/crop_Images/99746f0f-88ee-461e-bae6-f792110efcc01523628699550","productPrice":"string"},{"id":8,"createdDateTime":1523628786000,"productCategory":{"id":1,"productCategoryName":"Insecticides"},"productName":"SAFER","productDescription":"Kills","productUnit":"1","productQuantity":"500ML","productImage":"/crop_Images/aa65fffa-58b5-4934-bd95-0412a2c591051523628786438","productPrice":"500"},{"id":9,"createdDateTime":1523942185000,"productCategory":{"id":1,"productCategoryName":"Insecticides"},"productName":"TestProduct","productDescription":"test description","productUnit":"200ml","productQuantity":"20","productImage":"/crop_Images/c4df0363-42bb-42a3-8916-77ce2e1e2e381523942185424","productPrice":"500"}],"imageUrl":"http://54.169.217.51:8080/images/"}
     */

    private String message;
    private int code;
    private ResponseDataBean responseData;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public ResponseDataBean getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseDataBean responseData) {
        this.responseData = responseData;
    }

    public static class ResponseDataBean implements Parcelable {
        /**
         * products : [{"id":1,"productCategory":{"id":1,"productCategoryName":"Insecticides"},"productName":"Safer Caterpillar Killer","productUnit":"300ml","productQuantity":"1","productImage":"/crop_Images/96251f33-35cc-44d2-a6b9-7029ca5b169b1523448809330","productPrice":"500"},{"id":2,"productCategory":{"id":2,"productCategoryName":"Fungicides"},"productName":"Product2","productUnit":"300ml","productQuantity":"1","productImage":"7029ca5b169b1523448809330","productPrice":"500"},{"id":3,"productCategory":{"id":3,"productCategoryName":"Herbicides"},"productName":"product3","productUnit":"300ml","productQuantity":"1","productImage":"7029ca5b169b1523448809330","productPrice":"500"},{"id":4,"productCategory":{"id":4,"productCategoryName":"Seed Treatment Products"},"productName":"product4","productQuantity":"1","productImage":"7029ca5b169b1523448809330","productPrice":"500"},{"id":7,"createdDateTime":1523628700000,"productCategory":{"id":1,"productCategoryName":"Insecticides"},"productName":"string","productDescription":"string","productUnit":"string","productQuantity":"string","productImage":"/crop_Images/99746f0f-88ee-461e-bae6-f792110efcc01523628699550","productPrice":"string"},{"id":8,"createdDateTime":1523628786000,"productCategory":{"id":1,"productCategoryName":"Insecticides"},"productName":"SAFER","productDescription":"Kills","productUnit":"1","productQuantity":"500ML","productImage":"/crop_Images/aa65fffa-58b5-4934-bd95-0412a2c591051523628786438","productPrice":"500"},{"id":9,"createdDateTime":1523942185000,"productCategory":{"id":1,"productCategoryName":"Insecticides"},"productName":"TestProduct","productDescription":"test description","productUnit":"200ml","productQuantity":"20","productImage":"/crop_Images/c4df0363-42bb-42a3-8916-77ce2e1e2e381523942185424","productPrice":"500"}]
         * imageUrl : http://54.169.217.51:8080/images/
         */

        private String imageUrl;
        private List<ProductsBean> products;

        protected ResponseDataBean(Parcel in) {
            imageUrl = in.readString();
            products = in.createTypedArrayList(ProductsBean.CREATOR);
        }

        public static final Creator<ResponseDataBean> CREATOR = new Creator<ResponseDataBean>() {
            @Override
            public ResponseDataBean createFromParcel(Parcel in) {
                return new ResponseDataBean(in);
            }

            @Override
            public ResponseDataBean[] newArray(int size) {
                return new ResponseDataBean[size];
            }
        };

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public List<ProductsBean> getProducts() {
            return products;
        }

        public void setProducts(List<ProductsBean> products) {
            this.products = products;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(imageUrl);
            parcel.writeTypedList(products);
        }

        public static class ProductsBean implements Parcelable {
            /**
             * id : 1
             * productCategory : {"id":1,"productCategoryName":"Insecticides"}
             * productName : Safer Caterpillar Killer
             * productUnit : 300ml
             * productQuantity : 1
             * productImage : /crop_Images/96251f33-35cc-44d2-a6b9-7029ca5b169b1523448809330
             * productPrice : 500
             * createdDateTime : 1523628700000
             * productDescription : string
             */

            private int id;
            private ProductCategoryBean productCategory;
            private String productName;
            private String productUnit;
            private String productQuantity;
            private String productImage;
            private String productPrice;
            private long createdDateTime;
            private String productDescription;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public ProductCategoryBean getProductCategory() {
                return productCategory;
            }

            public void setProductCategory(ProductCategoryBean productCategory) {
                this.productCategory = productCategory;
            }

            public String getProductName() {
                return productName;
            }

            public void setProductName(String productName) {
                this.productName = productName;
            }

            public String getProductUnit() {
                return productUnit;
            }

            public void setProductUnit(String productUnit) {
                this.productUnit = productUnit;
            }

            public String getProductQuantity() {
                return productQuantity;
            }

            public void setProductQuantity(String productQuantity) {
                this.productQuantity = productQuantity;
            }

            public String getProductImage() {
                return productImage;
            }

            public void setProductImage(String productImage) {
                this.productImage = productImage;
            }

            public String getProductPrice() {
                return productPrice;
            }

            public void setProductPrice(String productPrice) {
                this.productPrice = productPrice;
            }

            public long getCreatedDateTime() {
                return createdDateTime;
            }

            public void setCreatedDateTime(long createdDateTime) {
                this.createdDateTime = createdDateTime;
            }

            public String getProductDescription() {
                return productDescription;
            }

            public void setProductDescription(String productDescription) {
                this.productDescription = productDescription;
            }

            public static class ProductCategoryBean implements Parcelable {
                /**
                 * id : 1
                 * productCategoryName : Insecticides
                 */

                private int id;
                private String productCategoryName;
                private List<GalleryCategoryBean> gallery;

                protected ProductCategoryBean(Parcel in) {
                    id = in.readInt();
                    productCategoryName = in.readString();
                    gallery = in.createTypedArrayList(GalleryCategoryBean.CREATOR);
                }

                public static final Creator<ProductCategoryBean> CREATOR = new Creator<ProductCategoryBean>() {
                    @Override
                    public ProductCategoryBean createFromParcel(Parcel in) {
                        return new ProductCategoryBean(in);
                    }

                    @Override
                    public ProductCategoryBean[] newArray(int size) {
                        return new ProductCategoryBean[size];
                    }
                };

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getProductCategoryName() {
                    return productCategoryName;
                }

                public void setProductCategoryName(String productCategoryName) {
                    this.productCategoryName = productCategoryName;
                }

                public List<GalleryCategoryBean> getGallery() {
                    return gallery;
                }

                public void setGallery(List<GalleryCategoryBean> gallery) {
                    this.gallery = gallery;
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel parcel, int i) {
                    parcel.writeInt(id);
                    parcel.writeString(productCategoryName);
                    parcel.writeTypedList(gallery);
                }

                public static class GalleryCategoryBean implements Parcelable {

                    private int id;
                    private String pdfUrl;

                    public static final Creator<GalleryCategoryBean> CREATOR = new Creator<GalleryCategoryBean>() {
                        @Override
                        public GalleryCategoryBean createFromParcel(Parcel in) {
                            return new GalleryCategoryBean(in);
                        }

                        @Override
                        public GalleryCategoryBean[] newArray(int size) {
                            return new GalleryCategoryBean[size];
                        }
                    };

                    public int getId() {
                        return id;
                    }

                    public void setId(int id) {
                        this.id = id;
                    }

                    public String getPdfUrl() {
                        return pdfUrl;
                    }

                    public void setPdfUrl(String pdfUrl) {
                        this.pdfUrl = pdfUrl;
                    }

                    protected GalleryCategoryBean(Parcel in) {
                        id = in.readInt();
                        pdfUrl = in.readString();
                    }

                    @Override
                    public int describeContents() {
                        return 0;
                    }

                    @Override
                    public void writeToParcel(Parcel parcel, int flags) {
                        parcel.writeInt(id);
                        parcel.writeString(pdfUrl);
                    }
                }
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeInt(this.id);
                dest.writeParcelable(this.productCategory, flags);
                dest.writeString(this.productName);
                dest.writeString(this.productUnit);
                dest.writeString(this.productQuantity);
                dest.writeString(this.productImage);
                dest.writeString(this.productPrice);
                dest.writeLong(this.createdDateTime);
                dest.writeString(this.productDescription);
            }

            public ProductsBean() {
            }

            protected ProductsBean(Parcel in) {
                this.id = in.readInt();
                this.productCategory = in.readParcelable(ProductCategoryBean.class.getClassLoader());
                this.productName = in.readString();
                this.productUnit = in.readString();
                this.productQuantity = in.readString();
                this.productImage = in.readString();
                this.productPrice = in.readString();
                this.createdDateTime = in.readLong();
                this.productDescription = in.readString();
            }

            public static final Parcelable.Creator<ProductsBean> CREATOR = new Parcelable.Creator<ProductsBean>() {
                @Override
                public ProductsBean createFromParcel(Parcel source) {
                    return new ProductsBean(source);
                }

                @Override
                public ProductsBean[] newArray(int size) {
                    return new ProductsBean[size];
                }
            };
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.message);
        dest.writeInt(this.code);
        dest.writeParcelable(this.responseData, flags);
    }

    public ProductsResponse() {
    }

    protected ProductsResponse(Parcel in) {
        this.message = in.readString();
        this.code = in.readInt();
        this.responseData = in.readParcelable(ResponseDataBean.class.getClassLoader());
    }

    public static final Parcelable.Creator<ProductsResponse> CREATOR = new Parcelable.Creator<ProductsResponse>() {
        @Override
        public ProductsResponse createFromParcel(Parcel source) {
            return new ProductsResponse(source);
        }

        @Override
        public ProductsResponse[] newArray(int size) {
            return new ProductsResponse[size];
        }
    };
}
