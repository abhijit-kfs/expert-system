package com.kfs.expertsystems.models;

import java.util.List;

/**
 * Created by Shylesh on 23-Feb-18.
 */

public class PromotionsResponse {

    /**
     * message : success
     * code : 200
     * responseData : {"promos":[{"id":9,"createdDateTime":1518635748000,"imageName":"AdTwo","promoImageURL":"/promo_image/c2b92b91-15a3-4868-b0e7-3471308c1b5d1518635747818","imageDescription":"Ad images"},{"id":10,"createdDateTime":1518635776000,"imageName":"adThree","promoImageURL":"/promo_image/00592f43-3ce3-41e9-a086-e4d50e4ff5af1518635776087","imageDescription":"Ad images"},{"id":11,"createdDateTime":1518635794000,"imageName":"AdFour","promoImageURL":"/promo_image/e475795c-f50f-4626-974c-c7f4a440e7d91518635793865","imageDescription":"Ad images"},{"id":12,"createdDateTime":1518635905000,"imageName":"AdOne","promoImageURL":"/promo_image/872599ef-6f9a-472c-bf13-7f110522c8fb1518635905248","imageDescription":"Ad images"}],"imageBaseUrl":"http://54.169.217.51:8080/images"}
     */

    private String message;
    private int code;
    private ResponseDataBean responseData;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseDataBean getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseDataBean responseData) {
        this.responseData = responseData;
    }


    public static class ResponseDataBean {

        private String imageBaseUrl;
        private List<PromosBean> promos;


        public String getImageBaseUrl() {
            return imageBaseUrl;
        }

        public void setImageBaseUrl(String imageBaseUrl) {
            this.imageBaseUrl = imageBaseUrl;
        }

        public List<PromosBean> getPromos() {
            return promos;
        }

        public void setPromos(List<PromosBean> promos) {
            this.promos = promos;
        }

        public static class PromosBean {
            /**
             * id : 9
             * createdDateTime : 1518635748000
             * imageName : AdTwo
             * promoImageURL : /promo_image/c2b92b91-15a3-4868-b0e7-3471308c1b5d1518635747818
             * imageDescription : Ad images
             */

            private int id;
            private long createdDateTime;
            private String imageName;
            private String promoImageURL;
            private String imageDescription;

            public long getCreatedDateTime() {
                return createdDateTime;
            }

            public int getId() {
                return id;
            }

            public String getImageDescription() {
                return imageDescription;
            }

            public String getImageName() {
                return imageName;
            }

            public String getPromoImageURL() {
                return promoImageURL;
            }

            public void setCreatedDateTime(long createdDateTime) {
                this.createdDateTime = createdDateTime;
            }

            public void setId(int id) {
                this.id = id;
            }

            public void setImageDescription(String imageDescription) {
                this.imageDescription = imageDescription;
            }

            public void setImageName(String imageName) {
                this.imageName = imageName;
            }

            public void setPromoImageURL(String promoImageURL) {
                this.promoImageURL = promoImageURL;
            }
        }
    }

}
