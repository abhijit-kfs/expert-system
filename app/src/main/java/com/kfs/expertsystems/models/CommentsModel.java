package com.kfs.expertsystems.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Tomesh on 27-09-2017.
 */

public class CommentsModel implements Serializable {

    private int commentId;
    private long createdDateTime;
    private long updatedDateTime;
    private String comDesc;
    private int userType;
    private int enquiriesId;
    private String baseUrl;
    private List<ResolutionResponse.ResponseDataBean.ResolutionListBean.GalleryBean> gallery;

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public long getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(long createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public long getUpdatedDateTime() {
        return updatedDateTime;
    }

    public void setUpdatedDateTime(long updatedDateTime) {
        this.updatedDateTime = updatedDateTime;
    }

    public String getComDesc() {
        return comDesc;
    }

    public void setComDesc(String comDesc) {
        this.comDesc = comDesc;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public int getEnquiriesId() {
        return enquiriesId;
    }

    public void setEnquiriesId(int enquiriesId) {
        this.enquiriesId = enquiriesId;
    }

    public List<ResolutionResponse.ResponseDataBean.ResolutionListBean.GalleryBean> getGallery() {
        return gallery;
    }

    public void setGallery(List<ResolutionResponse.ResponseDataBean.ResolutionListBean.GalleryBean> gallery) {
        this.gallery = gallery;
    }


    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }
}
