package com.kfs.expertsystems.models;

/**
 * Created by Tomesh on 06-10-2017.
 */

public class ProfileUpdateRequest {


    /**
     * address : string
     * name : string
     * phoneNumber : 0
     * profileImage : string
     * uId : string
     */

    private String address;
    private String name;
    private long  phoneNumber;
    private String profileImage;
    private String uId;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getUId() {
        return uId;
    }

    public void setUId(String uId) {
        this.uId = uId;
    }
}
