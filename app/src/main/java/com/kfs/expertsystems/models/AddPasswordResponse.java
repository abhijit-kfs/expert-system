package com.kfs.expertsystems.models;

/**
 * Created by Shylesh on 26-Feb-18.
 */

public class AddPasswordResponse {


    /**
     * message : SUCCESS
     * code : 200
     * responseData : {"passwordExists":0}
     */

    private String message;
    private int code;
    private ResponseDataBean responseData;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public ResponseDataBean getResponseData() {
        return responseData;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setResponseData(ResponseDataBean responseData) {
        this.responseData = responseData;
    }

    public static class ResponseDataBean {
        /**
         * passwordExists : 0
         */

        private int passwordExists;

        public int getPasswordExists() {
            return passwordExists;
        }

        public void setPasswordExists(int passwordExists) {
            this.passwordExists = passwordExists;
        }
    }
}
