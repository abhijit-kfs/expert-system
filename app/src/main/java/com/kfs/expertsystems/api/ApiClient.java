package com.kfs.expertsystems.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Tomesh on 19-09-2017.
 */

public class ApiClient {

    //        public static final String BASE_URL = "http://192.168.0.108:8080/agripro/";
//    public static final String BASE_URL = "http://54.169.217.51:8080/agripro/";


    public static final String BASE_URL = "http://122.166.212.177:9494/agripro/"; //Expert System Production
//    public static final String BASE_URL = "http://192.168.0.115:8080/agripro/"; //Expert System Local

    //IMAGE
    public static final String BASE_IMAGE_URL = "http://122.166.212.177:9494/images/"; //Expert System Production



    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}