package com.kfs.expertsystems.api;

import com.kfs.expertsystems.models.AddComments;
import com.kfs.expertsystems.models.AddEnquiry;
import com.kfs.expertsystems.models.AddPasswordRequest;
import com.kfs.expertsystems.models.AddPasswordResponse;
import com.kfs.expertsystems.models.AddcommentResponse;
import com.kfs.expertsystems.models.BaseResponse;
import com.kfs.expertsystems.models.BasicResponse;
import com.kfs.expertsystems.models.CategoryResponse;
import com.kfs.expertsystems.models.ChangePasswordRequest;
import com.kfs.expertsystems.models.CropResponse;
import com.kfs.expertsystems.models.DealersResponse;
import com.kfs.expertsystems.models.NewsResponse;
import com.kfs.expertsystems.models.ProductsResponse;
import com.kfs.expertsystems.models.ProfileModel;
import com.kfs.expertsystems.models.ProfileUpdateRequest;
import com.kfs.expertsystems.models.PromotionsResponse;
import com.kfs.expertsystems.models.ReportProblemResponse;
import com.kfs.expertsystems.models.ResolutionRequest;
import com.kfs.expertsystems.models.ResolutionResponse;
import com.kfs.expertsystems.models.ResolvedModel;
import com.kfs.expertsystems.models.UserRequest;
import com.kfs.expertsystems.models.UserResponse;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Tomesh on 19-09-2017.
 */

public interface ApiInterface {


    @POST("user/registration")
    Call<UserResponse> registration(@Body HashMap<String, Object> user);

    @POST("user/signIn")
    Call<UserResponse> signIn(@Body UserRequest user);

    @GET("crops/getAllCrops")
    Call<CropResponse> getCrops();

    @GET("crops/getAllPromotionImages")
    Call<PromotionsResponse> getAllPromotion();

    @POST("enquiries/addEnquiry")
    Call<ReportProblemResponse> addEnquiry(@Body AddEnquiry enquiry);

    @POST("enquiries/addComments")
    Call<AddcommentResponse> addComments(@Body AddComments comments);

    @POST("enquiries/updateEnquiry")
    Call<BaseResponse> updateEnquiry(@Body ResolvedModel resolvedModel);

    @POST("enquiries/getResolutionByUserID")
    Call<ResolutionResponse> getEnquiryList(@Body ResolutionRequest resolutionRequest);


    @POST("user/profile")
    Call<ProfileModel> getProfile(@Body ResolutionRequest resolutionRequest);

    @POST("user/updateProfile")
    Call<BaseResponse> updateProfile(@Body ProfileUpdateRequest profileRequest);

    @POST("user/changePassword")
    Call<BasicResponse> changePassword(@Body ChangePasswordRequest request);

    @POST("user/addPassword")
    Call<AddPasswordResponse> addPassword(@Body AddPasswordRequest request);

    @GET("dealers/getAllDealers")
    Call<DealersResponse> getDealers();

    @GET("news/getAllNews")
    Call<NewsResponse> getNews();

    @GET("dealers/getAllProducts")
    Call<ProductsResponse> getProducts();

    @GET("dealers/getProductCategoryList")
    Call<CategoryResponse> getCategories();

}
