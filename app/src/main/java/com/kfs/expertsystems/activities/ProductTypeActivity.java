package com.kfs.expertsystems.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kfs.expertsystems.R;
import com.kfs.expertsystems.callbacks.Interactor;
import com.kfs.expertsystems.fragments.PDFFragment;
import com.kfs.expertsystems.fragments.ProductTypeFragment;
import com.kfs.expertsystems.models.CategoryResponse;
import com.kfs.expertsystems.models.CategoryResponse.ResponseDataBean;
import com.kfs.expertsystems.models.CategoryResponse.ResponseDataBean.GalleryCategoryBean;
import com.kfs.expertsystems.models.ProductsResponse;
import com.kfs.expertsystems.presenter.presenter.ProductsPresenter;
import com.kfs.expertsystems.presenter.presenterImpl.ProductsPresenterImpl;
import com.kfs.expertsystems.presenter.views.ProductsView;
import com.kfs.expertsystems.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ProductTypeActivity extends AppCompatActivity implements ProductsView, View.OnClickListener,
        Interactor.Product, Interactor.ProductType {

    private static final String TAG = ProductTypeActivity.class.getSimpleName();

    private ProductsPresenter productsPresenter;

    private List<ResponseDataBean> productCategoryBeanList = new ArrayList<>();

    private HashMap<Integer, List<GalleryCategoryBean>> map = new HashMap<>();

    private ProgressBar progress;
    private RelativeLayout emptyScreenLayout;
    private TextView emptyText;
    private TextView tryAgain;
    private FrameLayout frameLayout;

    private PDFFragment pdfFragment;
    private ProductTypeFragment productTypeFragment;

    private boolean isFirstScreen = true;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_product_type);

        initToolbar();
        initFragments();
        initUI();
    }

    private void initFragments() {
        pdfFragment = PDFFragment.getInstance();
        productTypeFragment = ProductTypeFragment.getInstance();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initUI() {
        emptyScreenLayout = (RelativeLayout) findViewById(R.id.empty_layout);
        emptyText = (TextView) findViewById(R.id.cant_load_text);
        tryAgain = (TextView) findViewById(R.id.try_again);
        tryAgain.setOnClickListener(this);
        progress = (ProgressBar) findViewById(R.id.progress);
        frameLayout = findViewById(R.id.frame_layout);

        showFragment(true);

        productsPresenter = new ProductsPresenterImpl(this);
        productsPresenter.getCategories();
    }

    private void showFragment(boolean showFirstScreen) {
        isFirstScreen = showFirstScreen;

        emptyScreenLayout.setVisibility(View.GONE);
        frameLayout.setVisibility(View.VISIBLE);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        if (isFirstScreen) {
            if (productTypeFragment.isAdded()) {
                ft.show(productTypeFragment);
            } else {
                ft.add(R.id.frame_layout, productTypeFragment, ProductTypeFragment.class.getSimpleName());
            }

            if (pdfFragment.isAdded())
                ft.hide(pdfFragment);
        } else {
            if (pdfFragment.isAdded()) {
                ft.show(pdfFragment);
            } else {
                ft.add(R.id.frame_layout, pdfFragment, PDFFragment.class.getSimpleName());
            }

            if (productTypeFragment.isAdded())
                ft.hide(productTypeFragment);
        }

        ft.commit();
    }

    @Override
    public void onCategorySelected(int id) {
        if (id > 0) {
            if (map.containsKey(id)) {
                showFragment(false);
                pdfFragment.setCategoryId(id);
            } else {
                Toast.makeText(getApplicationContext(), "No PDF's available", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public List<ResponseDataBean> getProductTypeList() {
        return productCategoryBeanList;
    }

    @Override
    public List<GalleryCategoryBean> getGalleryList(int categoryId) {
        if (map.get(categoryId) != null) {
            return map.get(categoryId);
        } else return new ArrayList<>();
    }

    @Override
    public void onCategoriesFound(CategoryResponse categories) {
        parseResponse(categories);
        updateCategoryList();
    }

    @Override
    public void onDataFound(ProductsResponse products) {

    }

    private void updateCategoryList() {
        if (productCategoryBeanList != null && productCategoryBeanList.size() > 0) {
            if (productTypeFragment != null)
                productTypeFragment.updateAdapter(productCategoryBeanList);
        }
    }

    @Override
    public void onDataNotFound(String msg) {
        Log.d(TAG, "onDataNotFound: " + msg);
        Utils.getMsgPopup("Error", msg, ProductTypeActivity.this);
        emptyScreenLayout.setVisibility(View.VISIBLE);
        emptyText.setText(R.string.no_data_found);
    }

    @Override
    public void showProgress() {
        Log.d(TAG, "showProgress: ");
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        Log.d(TAG, "hideProgress: ");
        progress.setVisibility(View.GONE);
    }

    @Override
    public void setError(String msg) {
        Log.d(TAG, "setError: ");
        Utils.getMsgPopup("Error", msg, ProductTypeActivity.this);
        emptyScreenLayout.setVisibility(View.VISIBLE);
        emptyText.setText(R.string.cant_load);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                overridePendingTransition(0, 0);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void parseResponse(CategoryResponse response) {
        if (response != null && response.getResponseData() != null) {
            productCategoryBeanList.addAll(response.getResponseData());

            List<ResponseDataBean> categories = response.getResponseData();

            for (int i = 0; i < categories.size(); i++) {
                ResponseDataBean responseDataBean = categories.get(i);

                if (responseDataBean != null) {
                    int categoryId = responseDataBean.getId();

                    if (responseDataBean.getGallery() != null) {

                        if (map.containsKey(categoryId)) {
                            //map.get(categoryId).addAll(product.getProductCategory().getGallery());
                        } else {
                            List<GalleryCategoryBean> beanList = new ArrayList<>(responseDataBean.getGallery());
                            if (beanList.size() > 0)
                                map.put(categoryId, beanList);
                        }
                    }
                }
            }
        }
    }

    /*private void parseResponse(ProductsResponse response) {
        if (response != null && response.getResponseData() != null) {

            if (response.getResponseData().getImageUrl() != null &&
                    !response.getResponseData().getImageUrl().isEmpty()) {
                pdfFragment.setBaseUrl(response.getResponseData().getImageUrl());
            }

            if (response.getResponseData().getProducts() != null) {
                List<ProductsBean> products = response.getResponseData().getProducts();

                for (int i = 0; i < products.size(); i++) {
                    ProductsBean product = products.get(i);

                    if (product != null) {
                        int categoryId = product.getProductCategory().getId();

                        if (product.getProductCategory().getGallery() != null) {

                            if (map.containsKey(categoryId)) {
                                //map.get(categoryId).addAll(product.getProductCategory().getGallery());
                            } else {
                                List<GalleryCategoryBean> beanList = new ArrayList<>(product.getProductCategory().getGallery());
                                if (beanList.size() > 0)
                                    map.put(categoryId, beanList);
                            }
                        }
                    }
                }
            }
        }
    }*/

    @Override
    public void onError(String msg) {
        Log.d(TAG, "setError: ");
        Utils.getMsgPopup("Error", msg, ProductTypeActivity.this);
        emptyScreenLayout.setVisibility(View.VISIBLE);
        emptyText.setText(R.string.cant_load);
    }

    @Override
    public void navigateToHome() {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void onDestroy() {
        productsPresenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (isFirstScreen) {
            super.onBackPressed();
            overridePendingTransition(0, 0);
        }
        else
            showFragment(true);
    }
}
