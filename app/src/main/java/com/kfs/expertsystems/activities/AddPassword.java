package com.kfs.expertsystems.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.kfs.expertsystems.R;
import com.kfs.expertsystems.application.AgriProApplication;
import com.kfs.expertsystems.models.AddPasswordRequest;
import com.kfs.expertsystems.models.ProfileModel;
import com.kfs.expertsystems.presenter.presenterImpl.ProfilePresenterImpl;
import com.kfs.expertsystems.presenter.views.ProfileView;
import com.kfs.expertsystems.utils.Constants;
import com.kfs.expertsystems.utils.Utils;

public class AddPassword extends AppCompatActivity implements ProfileView, View.OnClickListener {

    private ProfilePresenterImpl profilePresenter;
    private String TAG = AddPassword.class.getName();
    private EditText mEtPassword;
    private EditText mEtConfirmPassword;
    private ProgressBar progress;

    @Override
    public void hideProgress() {
        progress.setVisibility(View.GONE);
    }

    private void initUI() {
        Log.d(TAG, "initUI: ");

        profilePresenter = new ProfilePresenterImpl(this);
        mEtPassword = (EditText) findViewById(R.id.password);
        mEtConfirmPassword = (EditText) findViewById(R.id.confirm_password);
        progress = (ProgressBar) findViewById(R.id.progress);
        findViewById(R.id.add_password_submit).setOnClickListener(this);

    }

    @Override
    public void navigateToHome() {
        AgriProApplication.getPref().put(Constants.SHOW_ADD_PASSWORD, false);
        startActivity(new Intent(AddPassword.this, DashboardScreen.class));
    }

    @Override
    public void onAddPasswordError(String msg) {
        Log.d(TAG, "onAddPasswordError: ");
        Utils.getMsgPopup("Error", msg, AddPassword.this);
    }

    @Override
    public void onProfileDataFound(ProfileModel profile) {

    }

    @Override
    public void onDataNotFound(String msg) {

    }

    @Override
    public void onProfileUpdate(String msg) {

    }

    @Override
    public void onNameError(String msg) {

    }

    @Override
    public void onPhoneNumberError(String msg) {

    }

    @Override
    public void onAddressError(String msg) {

    }

    @Override
    public void onProfileUpdated() {

    }

    @Override
    public void onProfileUpdateError(String msg) {

    }

    @Override
    public void onPasswordChangeSuccess(String msg) {

    }

    @Override
    public void onPasswordChangeError(String msg) {

    }

    @Override
    public void onOldPasswordError(String msg) {

    }

    @Override
    public void onNewPasswordError(String msg) {

    }

    @Override
    public void onConfirmPasswordError(String msg) {

    }

    @Override
    public void onAddPasswordSuccess(String msg) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_password_submit:
                if (mEtPassword.getText().toString().equals(mEtConfirmPassword.getText().toString())) {
                    profilePresenter.addPassword(new AddPasswordRequest(mEtPassword.getText().toString(), AgriProApplication.getPref().getString(Constants.USER_ID)));
                } else {
                    onAddPasswordError("Password doesn't match");
                }
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initUI();

    }

    @Override
    protected void onDestroy() {
        profilePresenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void setError(String msg) {

    }

    @Override
    public void showProgress() {
        progress.setVisibility(View.VISIBLE);
    }
}
