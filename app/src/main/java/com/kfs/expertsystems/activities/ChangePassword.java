package com.kfs.expertsystems.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.kfs.expertsystems.R;
import com.kfs.expertsystems.application.AgriProApplication;
import com.kfs.expertsystems.models.ChangePasswordRequest;
import com.kfs.expertsystems.models.ProfileModel;
import com.kfs.expertsystems.presenter.presenter.ProfilePresenter;
import com.kfs.expertsystems.presenter.presenterImpl.ProfilePresenterImpl;
import com.kfs.expertsystems.presenter.views.ProfileView;
import com.kfs.expertsystems.utils.Constants;
import com.kfs.expertsystems.utils.Utils;

public class ChangePassword extends AppCompatActivity implements View.OnClickListener, ProfileView {

    private final String TAG = ChangePassword.class.getName();
    private EditText old_password, new_password, confirm_new_password;

    private Button change_password_submit;
    private ImageView change_password_back;
    private ProgressBar progress;
    private ProfilePresenter profilePresenter;

    private boolean submit = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changepasword);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initUI();

    }

    private void initUI() {
        Log.d(TAG, "initUI: ");

        profilePresenter = new ProfilePresenterImpl(this);
        old_password = (EditText) findViewById(R.id.old_password);
        new_password = (EditText) findViewById(R.id.new_password);
        confirm_new_password = (EditText) findViewById(R.id.confirm_new_password);
        progress = (ProgressBar) findViewById(R.id.progress);
        change_password_submit = (Button) findViewById(R.id.change_password_submit);
        change_password_submit.setOnClickListener(this);

        change_password_back = (ImageView) findViewById(R.id.change_password_back);
        change_password_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.change_password_submit:
                if (submit) {
                    submit = false;
                    ChangePasswordRequest request = new ChangePasswordRequest();
                    request.setNewPasswd(new_password.getText().toString());
                    request.setOldPasswd(old_password.getText().toString());
                    request.setConfirmNewPassword(confirm_new_password.getText().toString());
                    request.setUid(AgriProApplication.getPref().getString(Constants.USER_ID));
                    profilePresenter.changePassword(request);
                    Utils.hideKeyboard(ChangePassword.this);
                }
                break;

            case R.id.change_password_back:

                finish();
                break;
        }


    }

    @Override
    public void showProgress() {
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progress.setVisibility(View.GONE);
    }

    @Override
    public void setError(String msg) {

    }

    @Override
    public void navigateToHome() {

    }

    @Override
    public void onProfileDataFound(ProfileModel profile) {

    }

    @Override
    public void onDataNotFound(String msg) {

    }

    @Override
    public void onProfileUpdate(String msg) {

    }

    @Override
    public void onNameError(String msg) {

    }

    @Override
    public void onPhoneNumberError(String msg) {

    }

    @Override
    public void onAddressError(String msg) {

    }

    @Override
    public void onProfileUpdated() {

    }

    @Override
    public void onProfileUpdateError(String msg) {

    }

    @Override
    public void onPasswordChangeSuccess(String msg) {

        Log.d(TAG, "onPasswordChangeSuccess: ");
        submit = true;
        Utils.getMsgPopup("Error", msg, ChangePassword.this);


        old_password.setText("");old_password.requestFocus();
        new_password.setText("");
        confirm_new_password.setText("");
        Utils.hideKeyboard(ChangePassword.this);
    }

    @Override
    public void onPasswordChangeError(String msg) {

        Log.d(TAG, "onPasswordChangeError: ");
        submit = true;
        Utils.getMsgPopup("Error", msg, ChangePassword.this);

    }

    @Override
    public void onOldPasswordError(String msg) {
        Log.d(TAG, "onOldPasswordError: ");
        submit = true;
        Utils.getMsgPopup("Error", msg, ChangePassword.this);
        old_password.requestFocus();
    }

    @Override
    public void onNewPasswordError(String msg) {
        Log.d(TAG, "onNewPasswordError: ");
        submit = true;
        Utils.getMsgPopup("Error", msg, ChangePassword.this);
        new_password.requestFocus();
    }

    @Override
    public void onConfirmPasswordError(String msg) {
        Log.d(TAG, "onConfirmPasswordError: ");
        submit = true;
        Utils.getMsgPopup("Error", msg, ChangePassword.this);
        confirm_new_password.requestFocus();
    }

    @Override
    public void onAddPasswordSuccess(String msg) {

    }

    @Override
    public void onAddPasswordError(String msg) {

    }

    @Override
    protected void onDestroy() {
        profilePresenter.onDestroy();
        super.onDestroy();
    }

}
