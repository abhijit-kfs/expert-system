package com.kfs.expertsystems.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kfs.expertsystems.R;
import com.kfs.expertsystems.adapters.CropStageAdapter;
import com.kfs.expertsystems.callbacks.OnClickItemListener;
import com.kfs.expertsystems.models.CropResponse;

public class CropStageListScreen extends AppCompatActivity implements OnClickItemListener, View.OnClickListener {

    private RecyclerView recyclerView;
    private CropStageAdapter mAdapter;
    private ImageView imageViewBack;
    private CropResponse.ResponseDataBean.CropsBean selectedCrop;
    private RelativeLayout empty_screen_layout;
    private TextView tryAgain;
    private TextView emptyText;
    private String BASE_URL;
    private TextView stage_crop_title;
    private ImageView stage_crop_image;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop_stage_list_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        initUI();
    }

    private void initUI() {
        empty_screen_layout = (RelativeLayout) findViewById(R.id.empty_layout);
        emptyText = (TextView) findViewById(R.id.cant_load_text);
        tryAgain = (TextView) findViewById(R.id.try_again);
        stage_crop_title = (TextView) findViewById(R.id.stage_crop_title);
        stage_crop_image = (ImageView) findViewById(R.id.stage_crop_image);

        if (getIntent().hasExtra("STAGES")) {

            selectedCrop = (CropResponse.ResponseDataBean.CropsBean) getIntent()
                    .getSerializableExtra
                            ("STAGES");
            if (getIntent().hasExtra("BASE_URL")) {
                BASE_URL = getIntent().getStringExtra("BASE_URL");
            }
        } else {
            empty_screen_layout.setVisibility(View.VISIBLE);
            emptyText.setText(R.string.cant_load);
            tryAgain.setVisibility(View.GONE);
        }


        recyclerView = (RecyclerView) findViewById(R.id.recyclerview_crops_stage);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(CropStageListScreen.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());


        imageViewBack = (ImageView) findViewById(R.id.crop_stage_back);

        imageViewBack.setOnClickListener(this);


        setStageData();

    }

    private void setStageData() {

        if (selectedCrop.getStageModel().isEmpty()) {
            empty_screen_layout.setVisibility(View.VISIBLE);
            emptyText.setText(R.string.no_data_available);
            tryAgain.setVisibility(View.GONE);
            return;
        } else {
            empty_screen_layout.setVisibility(View.GONE);
        }
        stage_crop_title.setText(selectedCrop.getCropName());

        mAdapter = new CropStageAdapter(this, selectedCrop, BASE_URL);
        recyclerView.setAdapter(mAdapter);


        Glide.with(this).load(BASE_URL + selectedCrop.getCropImageUrl())
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.ic_image_place_holder)
                .crossFade()
                .into(stage_crop_image);

    }

    @Override
    public void onClick(int adapterPosition, Object data) {
        CropResponse.ResponseDataBean.CropsBean selectedStage = (CropResponse.ResponseDataBean.CropsBean) data;

        Intent intent = new Intent(CropStageListScreen.this, ReportProblemScreen.class);
        intent.putExtra("STAGES", selectedStage);
        intent.putExtra("STAGES_POSITION", adapterPosition+"");
        intent.putExtra("BASE_URL",BASE_URL);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(CropStageListScreen.this, DashboardScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.crop_stage_back:
                onBackPressed();

                break;
        }
    }
}
