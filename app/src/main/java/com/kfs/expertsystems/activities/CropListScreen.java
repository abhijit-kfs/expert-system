package com.kfs.expertsystems.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kfs.expertsystems.R;
import com.kfs.expertsystems.adapters.CropsListAdapter;
import com.kfs.expertsystems.callbacks.OnClickItemListener;
import com.kfs.expertsystems.models.CropResponse;
import com.kfs.expertsystems.presenter.presenter.CropPresenter;
import com.kfs.expertsystems.presenter.presenterImpl.CropPresenterImpl;
import com.kfs.expertsystems.presenter.views.CropsView;
import com.kfs.expertsystems.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class CropListScreen extends AppCompatActivity implements OnClickItemListener, CropsView,
        View.OnClickListener {

    private final String TAG = CropListScreen.this.getClass().getName();
    private RecyclerView recyclerView;
    private GridView gridView;
    private CropsListAdapter mAdapter;
    private ProgressBar progress;
    private CropPresenter presenter;
    private RelativeLayout empty_screen_layout;
    private TextView emptyText;
    private TextView tryAgain;
    private EditText search_crops;

    private CropResponse mMainCropResponse;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop_list_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void initUI() {

        empty_screen_layout = (RelativeLayout) findViewById(R.id.empty_layout);
        emptyText = (TextView) findViewById(R.id.cant_load_text);
        tryAgain = (TextView) findViewById(R.id.try_again);
        tryAgain.setOnClickListener(this);
        progress = (ProgressBar) findViewById(R.id.progress);
//        recyclerView = (RecyclerView) findViewById(R.id.recyclerview_crops_list);
        gridView = (GridView) findViewById(R.id.grid_crop);
        search_crops = (EditText) findViewById(R.id.search_crops);

//        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(CropListScreen.this);
//        recyclerView.setLayoutManager(layoutManager);
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mMainCropResponse = new CropResponse();
        presenter = new CropPresenterImpl(this);

        getCropData();
        getSearchListener();

    }

    private void getSearchListener() {
        search_crops.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                Log.d(TAG, "afterTextChanged: " + editable.toString());
               /* mAdapter.filterList(filter(editable.toString() , mMainCropResponse
                        .getResponseData().getCrops()));*/
                mAdapter.filterList(filter(editable.toString(), mMainCropResponse.getResponseData
                        ().getCrops()));
            }
        });
    }


    private List<CropResponse.ResponseDataBean.CropsBean> filter(String text, List<CropResponse.ResponseDataBean.CropsBean> response) {
        //new array list that will hold the filtered data
        if (text.equalsIgnoreCase("")) {
            mAdapter.filterList(response);
            return response;
        }

        List<CropResponse.ResponseDataBean.CropsBean> filterlist = new ArrayList<>();
        //looping through existing elements
        for (CropResponse.ResponseDataBean.CropsBean s : response) {
            //if the existing elements contains the search input
            if (s.getCropName().toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterlist.add(s);

            }
        }


//        response.getResponseData().setCrops(filterlist);


        //calling a method of the adapter class and passing the filtered list

        return filterlist;

    }

    private void getCropData() {
        presenter.getCrop();
    }


    @Override
    public void onClick(int adapterPosition, Object data) {
//        String item = (String) data;
        CropResponse.ResponseDataBean.CropsBean selectedCrop = (CropResponse.ResponseDataBean.CropsBean) data;
//        Log.d(TAG, "onClick: " + adapterPosition);
//        Intent intent = new Intent(CropListScreen.this, CropStageListScreen.class);
        Intent intent = new Intent(CropListScreen.this, ReportProblemScreen.class);
        intent.putExtra("STAGES", selectedCrop);
        intent.putExtra("BASE_URL", mMainCropResponse.getResponseData().getImageBaseUrl());
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void showProgress() {

        Log.d(TAG, "showProgress: ");
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        Log.d(TAG, "hideProgress: ");
        progress.setVisibility(View.GONE);
    }

    @Override
    public void setError(String msg) {
        Log.d(TAG, "setError: ");
        Utils.getMsgPopup("Error", msg, CropListScreen.this);
        empty_screen_layout.setVisibility(View.VISIBLE);
        emptyText.setText(R.string.cant_load);
    }

    @Override
    public void navigateToHome() {

    }

    @Override
    public void onDataFound(CropResponse crops) {
        Log.d(TAG, "onDataFound: " + crops.getResponseData().getCrops().size());
        empty_screen_layout.setVisibility(View.GONE);

        mMainCropResponse = crops;

        mAdapter = new CropsListAdapter(this, crops.getResponseData().getCrops(), crops
                .getResponseData().getImageBaseUrl());
        gridView.setAdapter(mAdapter);

    }

    @Override
    public void onDataNotFound(String msg) {
        Log.d(TAG, "onDataNotFound: " + msg);
        Utils.getMsgPopup("Error", msg, CropListScreen.this);
        empty_screen_layout.setVisibility(View.VISIBLE);
        emptyText.setText(R.string.no_data_found);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, 0);
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.try_again:
                getCropData();

                break;
        }
    }
}
