package com.kfs.expertsystems.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kfs.expertsystems.R;
import com.kfs.expertsystems.models.ProductsResponse;

public class ProductsDetailScreen extends AppCompatActivity {

    private ProductsResponse.ResponseDataBean.ProductsBean selectedProduct;
    private String BASE_IMAGE_URL;
    private RelativeLayout empty_screen_layout;
    private TextView tryAgain;
    private TextView emptyText;
    private TextView productName, productDescription, productCategory, productQuantity;
    private ImageView productImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products_detail_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");
        initUI();
    }

    public void initUI() {

        empty_screen_layout = (RelativeLayout) findViewById(R.id.empty_layout);
        emptyText = (TextView) findViewById(R.id.cant_load_text);
        tryAgain = (TextView) findViewById(R.id.try_again);

        productName = (TextView) findViewById(R.id.text_product_name);
        productDescription = (TextView) findViewById(R.id.text_product_description);
        productQuantity = (TextView) findViewById(R.id.text_product_quantity);
        productCategory = (TextView) findViewById(R.id.text_product_category);
        productImage = (ImageView) findViewById(R.id.product_image);

        if (getIntent().hasExtra("SELECTED_PRODUCT")) {

            selectedProduct = (ProductsResponse.ResponseDataBean.ProductsBean) getIntent()
                    .getParcelableExtra("SELECTED_PRODUCT");
            if (getIntent().hasExtra("IMAGE_BASE_URL")) {
                BASE_IMAGE_URL = getIntent().getStringExtra("IMAGE_BASE_URL");
            }
        } else {
            empty_screen_layout.setVisibility(View.VISIBLE);
            emptyText.setText(R.string.cant_load);
            tryAgain.setVisibility(View.GONE);
        }

        setProductData();
    }

    private void setProductData() {

        if (selectedProduct.getProductName().isEmpty()) {
            empty_screen_layout.setVisibility(View.VISIBLE);
            emptyText.setText(R.string.no_data_available);
            tryAgain.setVisibility(View.GONE);
            return;
        } else {
            empty_screen_layout.setVisibility(View.GONE);
        }
        productName.setText(selectedProduct.getProductName());
        productQuantity.setText(selectedProduct.getProductQuantity());
        productCategory.setText(selectedProduct.getProductCategory().getProductCategoryName());
        productDescription.setText(selectedProduct.getProductDescription());


        Glide.with(this).load(BASE_IMAGE_URL + selectedProduct.getProductImage())
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.ic_image_place_holder)
                .crossFade()
                .into(productImage);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
