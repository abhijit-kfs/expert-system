package com.kfs.expertsystems.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kfs.expertsystems.R;
import com.kfs.expertsystems.application.AgriProApplication;
import com.kfs.expertsystems.models.ProfileModel;
import com.kfs.expertsystems.models.ProfileUpdateRequest;
import com.kfs.expertsystems.presenter.presenter.ProfilePresenter;
import com.kfs.expertsystems.presenter.presenterImpl.ProfilePresenterImpl;
import com.kfs.expertsystems.presenter.views.ProfileView;
import com.kfs.expertsystems.utils.CircleImageView;
import com.kfs.expertsystems.utils.Constants;
import com.kfs.expertsystems.utils.Utils;

public class ProfileScreen extends AppCompatActivity implements View.OnClickListener, ProfileView {


    private final String TAG = ProfileScreen.class.getName();
    private static final int REQUEST_CAMERA = 102;
    private static final int SELECT_FILE = 103;

    private String userChoosenTask;
    private Uri imageUri;


    private ImageView profile_edit_image, profile_edit;
    private CircleImageView profile_image;

    private EditText profile_name, profile_phone, profile_email, profile_address;
    private boolean profileEdit = true;
    private ProgressBar progressBar;
    private RelativeLayout empty_screen_layout;
    private TextView emptyText;
    private TextView tryAgain;
    private ProfilePresenter profilePresenter;
    private ProfileModel profileModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_screen);
       /* Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/

        initUI();

    }

    private void initUI() {

        progressBar = (ProgressBar) findViewById(R.id.progress);

        empty_screen_layout = (RelativeLayout) findViewById(R.id.empty_layout);
        emptyText = (TextView) findViewById(R.id.cant_load_text);
        tryAgain = (TextView) findViewById(R.id.try_again);
        tryAgain.setOnClickListener(this);

        profilePresenter = new ProfilePresenterImpl(this);

        profile_image = (CircleImageView) findViewById(R.id.profile_image);
        profile_edit_image = (ImageView) findViewById(R.id.profile_edit_image);
        profile_edit_image.setOnClickListener(this);

        profile_edit = (ImageView) findViewById(R.id.profile_edit);
        profile_edit.setOnClickListener(this);

        profile_name = (EditText) findViewById(R.id.profile_name);
        profile_phone = (EditText) findViewById(R.id.profile_phone);
        profile_email = (EditText) findViewById(R.id.profile_email);
        profile_address = (EditText) findViewById(R.id.profile_address);


        profilePresenter.getProfile();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.profile_edit:
                if (profileEdit)
                    refreshEditUI();
                else if (!profileEdit) {
                    saveProfile();

                }
                break;
            case R.id.profile_edit_image:

                getCameraGallery();
                break;
            case R.id.try_again:
                profilePresenter.getProfile();
                break;

        }
    }

    private void saveProfile() {
        ProfileUpdateRequest profileUpdateRequest = new ProfileUpdateRequest();
        profileUpdateRequest.setName(profile_name.getText().toString());
        profileUpdateRequest.setPhoneNumber(Long.parseLong(profile_phone.getText().toString()));
        if (profileModel.getResponseData().isImageChanged())
            profileUpdateRequest.setProfileImage(String.valueOf(imageUri));

        profileUpdateRequest.setAddress(profile_address.getText().toString());
        profileUpdateRequest.setUId(AgriProApplication.getPref().getString(Constants.USER_ID));
        profilePresenter.updateProfile(profileUpdateRequest);
    }


    private void refreshEditUI() {

        profileEdit = false;
        profile_name.setEnabled(true);
        profile_name.requestFocus();
        profile_phone.setEnabled(true);
//        profile_email.setEnabled(true);
        profile_address.setEnabled(true);

        profile_edit.setImageResource(R.drawable.ic_checkmark);
        profile_edit_image.setVisibility(View.VISIBLE);

    }

    private void refreshSaveUI() {

        profileEdit = true;
        profile_name.setEnabled(false);
        profile_name.requestFocus();
        profile_phone.setEnabled(false);
        profile_email.setEnabled(false);
        profile_address.setEnabled(false);


        profile_edit.setImageResource(R.drawable.ic_edit);
        profile_edit_image.setVisibility(View.GONE);

    }


    private void getCameraGallery() {
        selectImage();
    }


    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileScreen.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean cameraResult = Utils.checkCameraPermission(ProfileScreen.this);
                boolean storegeResult = Utils.checkStoragePermission(ProfileScreen.this);
                if (items[item].equals("Take Photo")) {

                    userChoosenTask = "Take Photo";
                    if (cameraResult)
                        cameraIntent();
                } else if (items[item].equals("Choose from Library")) {

                    userChoosenTask = "Choose from Library";
                    if (storegeResult)
                        galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        imageUri = Utils.getImageUri(this);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, REQUEST_CAMERA);
    }


    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utils.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
            case Utils.MY_PERMISSIONS_REQUEST_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            try {
                Log.d(TAG, "onSelectFromGalleryResult: " + data.getData());
                setProfileImage(String.valueOf(data.getData()));
                profileModel.getResponseData().setImageChanged(true);
                imageUri = data.getData();
//                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void onCaptureImageResult(Intent data) {
        Log.d(TAG, "onCaptureImageResult: ");
        try {
            Log.d(TAG, "onCaptureImageResult: ");
            setProfileImage(String.valueOf(imageUri));
            profileModel.getResponseData().setImageChanged(true);
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    private void setProfileImage(String uri) {

        Glide.with(ProfileScreen.this).load(uri)
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.ic_profile_placeholder)
                .crossFade()
                .into(profile_image);


    }


    @Override
    public void showProgress() {

        Log.d(TAG, "showProgress: ");
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {

        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setError(String msg) {
        Utils.getMsgPopup("Error", msg, ProfileScreen.this);
        empty_screen_layout.setVisibility(View.VISIBLE);
        emptyText.setText(R.string.cant_load);
    }

    @Override
    public void navigateToHome() {


    }

    @Override
    public void onProfileDataFound(ProfileModel profile) {

        Log.d(TAG, "onProfileDataFound: ");

        empty_screen_layout.setVisibility(View.GONE);
        emptyText.setText(R.string.cant_load);


        profileModel = new ProfileModel();
        profileModel = profile;
        profile_name.setText(profile.getResponseData().getFirstName());
        profile_phone.setText(profile.getResponseData().getPhoneNumber() + "");
        profile_email.setText(profile.getResponseData().getEmailId());
        profile_address.setText(profile.getResponseData().getAddress());

        setProfileImage(profile.getResponseData().getBaseUrl() + profile.getResponseData()
                .getProfileImage());

    }

    @Override
    public void onDataNotFound(String msg) {
        Utils.getMsgPopup("Error", msg, ProfileScreen.this);
        empty_screen_layout.setVisibility(View.VISIBLE);
        emptyText.setText(R.string.cant_load);
    }

    @Override
    public void onProfileUpdate(String msg) {
        Utils.getMsgPopup("Success", msg, ProfileScreen.this);
        refreshSaveUI();
    }

    @Override
    public void onNameError(String msg) {
        Utils.getMsgPopup("Error", msg, ProfileScreen.this);
        profile_name.requestFocus();
    }

    @Override
    public void onPhoneNumberError(String msg) {
        Utils.getMsgPopup("Error", msg, ProfileScreen.this);
        profile_phone.requestFocus();
    }

    @Override
    public void onAddressError(String msg) {
        Utils.getMsgPopup("Error", msg, ProfileScreen.this);
        profile_address.requestFocus();
    }

    @Override
    public void onProfileUpdated() {

    }

    @Override
    public void onProfileUpdateError(String msg) {
        Utils.getMsgPopup("Error", msg, ProfileScreen.this);
    }

    @Override
    public void onPasswordChangeSuccess(String msg) {

    }

    @Override
    public void onPasswordChangeError(String msg) {

    }

    @Override
    public void onOldPasswordError(String msg) {

    }

    @Override
    public void onNewPasswordError(String msg) {

    }

    @Override
    public void onConfirmPasswordError(String msg) {

    }

    @Override
    public void onAddPasswordSuccess(String msg) {

    }

    @Override
    public void onAddPasswordError(String msg) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, 0);
    }

    @Override
    protected void onDestroy() {
        profilePresenter.onDestroy();
        super.onDestroy();

    }
}
