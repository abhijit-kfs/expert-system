package com.kfs.expertsystems.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kfs.expertsystems.R;
import com.kfs.expertsystems.adapters.NewsListAdapter;
import com.kfs.expertsystems.callbacks.OnClickItemListener;
import com.kfs.expertsystems.models.NewsResponse;
import com.kfs.expertsystems.presenter.presenter.NewsPresenter;
import com.kfs.expertsystems.presenter.presenterImpl.NewsPresenterImpl;
import com.kfs.expertsystems.presenter.views.NewsView;
import com.kfs.expertsystems.utils.Utils;

public class NewsArticlesListScreen extends AppCompatActivity implements OnClickItemListener,
        NewsView, View.OnClickListener {

    private static final String TAG = NewsArticlesListScreen.class.getName();
    private RecyclerView recyclerView;
    private NewsListAdapter mAdapter;
    private NewsPresenter newsPresenter;
    private RelativeLayout empty_screen_layout;
    private TextView emptyText;
    private TextView tryAgain;
    private ProgressBar progressBar;
    private NewsResponse news;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_articles_list_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initUI();


    }

    private void initUI() {
        newsPresenter = new NewsPresenterImpl(this);


        progressBar = (ProgressBar) findViewById(R.id.progress);

        empty_screen_layout = (RelativeLayout) findViewById(R.id.empty_layout);
        emptyText = (TextView) findViewById(R.id.cant_load_text);
        tryAgain = (TextView) findViewById(R.id.try_again);
        tryAgain.setOnClickListener(this);


        recyclerView = (RecyclerView) findViewById(R.id.recyclerview_news_list);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(NewsArticlesListScreen.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        newsPresenter.getNews();


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onClick(int adapterPosition, Object data) {
        Log.d(TAG, "onItemClick: ");
        NewsResponse.ResponseDataBean.NewsBean newsBeen = (NewsResponse.ResponseDataBean.NewsBean) data;
        Intent intent = new Intent(NewsArticlesListScreen.this, NewsArticlesDetailScreen.class);
        intent.putExtra("DATA", newsBeen);
        intent.putExtra("BASEURL", news.getResponseData().getBaseUrl());
        startActivity(intent);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setError(String msg) {
        Log.d(TAG, "onError: " + msg);
        Utils.getMsgPopup("Error", msg, NewsArticlesListScreen.this);
        empty_screen_layout.setVisibility(View.VISIBLE);
        emptyText.setText(R.string.cant_load);
    }

    @Override
    public void navigateToHome() {

    }

    @Override
    public void onDataFound(NewsResponse news) {

        Log.d(TAG, "onDataFound: ");

        empty_screen_layout.setVisibility(View.GONE);
        this.news = news;

        if (!news.getResponseData().getNews().isEmpty()) {
            mAdapter = new NewsListAdapter(this, news);
            recyclerView.setAdapter(mAdapter);

        } else {
            onDataNotFound("No News available.");
        }
    }

    @Override
    public void onError(String msg) {

    }

    @Override
    public void onDataNotFound(String msg) {
        Log.d(TAG, "onDataNotFound: " + msg);
        Utils.getMsgPopup("Error", msg, NewsArticlesListScreen.this);
        empty_screen_layout.setVisibility(View.VISIBLE);
        emptyText.setText(R.string.cant_load);
    }

    @Override
    public void onClick(View view) {
        Log.d(TAG, "onClick:");
        switch (view.getId()) {
            case R.id.try_again:
                newsPresenter.getNews();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, 0);
    }

    @Override
    protected void onDestroy() {
        newsPresenter.onDestroy();
        super.onDestroy();

    }

}
