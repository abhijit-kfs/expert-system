package com.kfs.expertsystems.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kfs.expertsystems.R;
import com.kfs.expertsystems.models.NewsResponse;

public class NewsArticlesDetailScreen extends AppCompatActivity {

    ImageView imageViewBack;
    NewsResponse.ResponseDataBean.NewsBean newsBean;
    private String BaseUrl;
    TextView news_detail_desc, news_detail_title, news_detail_time;
    ImageView news_detail_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_articles_detail_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (getIntent().hasExtra("DATA")) {
            newsBean = (NewsResponse.ResponseDataBean.NewsBean) getIntent().getSerializableExtra("DATA");
            BaseUrl = getIntent().getStringExtra("BASEURL");
        } else {
            finish();
        }

        initUI();


    }

    private void initUI() {

        news_detail_desc = (TextView) findViewById(R.id.news_detail_desc);
        news_detail_desc.setText(newsBean.getNews_Discription());
        news_detail_title = (TextView) findViewById(R.id.news_detail_title);
        news_detail_title.setText(newsBean.getNewsTitle());
        news_detail_time = (TextView) findViewById(R.id.news_detail_time);
        news_detail_image = (ImageView) findViewById(R.id.news_detail_image);


        Glide.with(this).load(BaseUrl + newsBean.getImage())
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.ic_image_place_holder) // can also be a drawable
                .error(R.drawable.ic_image_place_holder)
                .crossFade()
                .into(news_detail_image);

        imageViewBack = (ImageView) findViewById(R.id.news_back);
        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(NewsArticlesDetailScreen.this, DashboardScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
