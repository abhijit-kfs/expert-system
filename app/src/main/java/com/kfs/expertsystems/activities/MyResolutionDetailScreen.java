package com.kfs.expertsystems.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kfs.expertsystems.R;
import com.kfs.expertsystems.adapters.ReportProblemImagesAdapter;
import com.kfs.expertsystems.adapters.ReportProblemVideosAdapter;
import com.kfs.expertsystems.adapters.ResolutionDetailAdapter;
import com.kfs.expertsystems.callbacks.ImageDialogCallback;
import com.kfs.expertsystems.callbacks.ImageReportCallback;
import com.kfs.expertsystems.callbacks.OnClickItemListenerwithType;
import com.kfs.expertsystems.fragments.ImageDialogFramgent;
import com.kfs.expertsystems.fragments.ReportImageDialogFramgent;
import com.kfs.expertsystems.models.AddcommentResponse;
import com.kfs.expertsystems.models.CommentsModel;
import com.kfs.expertsystems.models.ResolutionModel;
import com.kfs.expertsystems.models.ResolutionResponse;
import com.kfs.expertsystems.models.ResolvedModel;
import com.kfs.expertsystems.presenter.presenter.ResolutionPresenter;
import com.kfs.expertsystems.presenter.presenterImpl.ResolutionPresenterImpl;
import com.kfs.expertsystems.presenter.views.ResolutionView;
import com.kfs.expertsystems.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.kfs.expertsystems.utils.Utils.getImageUri;

public class MyResolutionDetailScreen extends AppCompatActivity implements OnClickItemListenerwithType,
        ResolutionView, View.OnClickListener, ImageReportCallback, ImageDialogCallback {

    private final String TAG = MyResolutionDetailScreen.class.getName();
    private static final int REQUEST_CAMERA = 102;
    private static final int SELECT_FILE = 103;

    private RecyclerView recyclerViewResolutionDetail;
    private ResolutionDetailAdapter mAdapterComments;
    private ImageView imageViewBack;
    private Button resolution_detail_marked_resolved;

    private TextView resolution_detail_crop_name, resolution_detail_stage_name,
            resolution_detail_problemId, resolution_detail_status;

    private ImageView resolution_detail_image;

    private ResolutionModel resolutionModel;
    private List<CommentsModel> commentsModelsList;
    private TextView resolution_add_comment;
    private TextView resolution_reopen;
    private CommentsModel baseCommentsModel;
    private ProgressBar progress;

    //addComment Dialog
    private ImageView resolution_add_comment_cancel, resolution_add_comment_send;
    private Button report_problem_add_image, report_problem_add_video;
    private RecyclerView recyclerview_add_photo, recyclerview_add_video;
    private EditText report_problem_desc;
    RecyclerView.LayoutManager layoutManager;

    private RelativeLayout relative_bottom;

    private ReportProblemImagesAdapter mImageAdapter;
    private ReportProblemVideosAdapter mVideoAdapter;
    private List<Uri> uriListImage;
    private List<Uri> uriListVideo;
    private Uri imageUri;
    private String userChoosenTask;
    private String userChoosenTaskVideo;
    private ImageDialogFramgent imageDialogFramgent;
    private ResolutionPresenter resolutionPresenter;
    private String resolutionStatus = "";
    //

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_resolution_detail_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initUI();
    }

    private void initUI() {

        if (getIntent().hasExtra("DATA")) {
            resolutionModel = (ResolutionModel) getIntent().getSerializableExtra("DATA");
            Log.d("DATA", "initUI: " + resolutionModel.getDesc());
        } else {
            finish();
        }

        resolutionPresenter = new ResolutionPresenterImpl(this);
        progress = (ProgressBar) findViewById(R.id.progress);

        imageViewBack = (ImageView) findViewById(R.id.resolution_back);
        imageViewBack.setOnClickListener(this);


        resolution_detail_marked_resolved = (Button) findViewById(R.id
                .resolution_detail_marked_resolved);
        resolution_detail_marked_resolved.setOnClickListener(this);


        resolution_detail_crop_name = (TextView) findViewById(R.id.resolution_detail_crop_name);
        resolution_detail_stage_name = (TextView) findViewById(R.id.resolution_detail_stage_name);
        resolution_detail_problemId = (TextView) findViewById(R.id.resolution_detail_problemId_value);
        resolution_detail_status = (TextView) findViewById(R.id.resolution_detail_status);
        resolution_detail_image = (ImageView) findViewById(R.id.resolution_detail_image);
        resolution_add_comment = (TextView) findViewById(R.id.resolution_add_comment);
        resolution_reopen = (TextView) findViewById(R.id.text_reopen);
        resolution_reopen.setOnClickListener(this);
        resolution_add_comment.setOnClickListener(this);


        relative_bottom = (RelativeLayout) findViewById(R.id.relative_bottom);

        recyclerViewResolutionDetail = (RecyclerView) findViewById(R.id.recyclerview_resolution_detail);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewResolutionDetail.setLayoutManager(layoutManager);
        recyclerViewResolutionDetail.setItemAnimator(new DefaultItemAnimator());

        setResolutionData();


    }

    private void setResolutionData() {
        resolvedLayoutChanges();

        Glide.with(this).load(resolutionModel.getBaseURL() + resolutionModel.getCropImage())
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.ic_image_place_holder)
                .crossFade()
                .into(resolution_detail_image);

        resolution_detail_crop_name.setText(resolutionModel.getCropName());
        resolution_detail_stage_name.setText(resolutionModel.getStageName());
        resolution_detail_problemId.setText("CRPID" + resolutionModel.getProblemId());
        resolutionStatus = resolutionModel.getStatus();
        resolution_detail_status.setText(resolutionModel.getStatus());

        commentsModelsList = new ArrayList<>();
        mAdapterComments = new ResolutionDetailAdapter(commentsModelsList, this);
        recyclerViewResolutionDetail.setAdapter(mAdapterComments);

        //first data from enquiry model
        baseCommentsModel = new CommentsModel();
        baseCommentsModel.setComDesc(resolutionModel.getDesc());
        baseCommentsModel.setGallery(resolutionModel.getResponseDataBean().getGallery());
        baseCommentsModel.setUserType(0);
        baseCommentsModel.setBaseUrl(resolutionModel.getBaseURL());
        commentsModelsList.add(baseCommentsModel);

        //

        mAdapterComments.notifyDataSetChanged();
        List<CommentsModel> tempList = new ArrayList<>();
        if (!resolutionModel.getResponseDataBean().getComments().isEmpty()) {
            for (int i = 0; i < resolutionModel.getResponseDataBean().getComments().size(); i++) {

                CommentsModel model = new CommentsModel();
                List<ResolutionResponse.ResponseDataBean.ResolutionListBean.GalleryBean>
                        galleryBeenList = new ArrayList<>();
                model.setComDesc(resolutionModel.getResponseDataBean().getComments().get(i).getComDesc());
                for (int j = 0; j < resolutionModel.getResponseDataBean().getComments().get(i)
                        .getGallery().size(); j++) {
                    ResolutionResponse.ResponseDataBean.ResolutionListBean.GalleryBean
                            galleryBean = new ResolutionResponse.ResponseDataBean
                            .ResolutionListBean.GalleryBean();
                    galleryBean.setId(resolutionModel.getResponseDataBean().getComments().get(i)
                            .getGallery().get(j).getId());
                    if(resolutionModel
                            .getResponseDataBean()
                            .getComments()
                            .get(i)
                            .getGallery().get(j).getImageUrl() != null) {
                        galleryBean.setImageUrl(resolutionModel
                                .getResponseDataBean()
                                .getComments()
                                .get(i)
                                .getGallery().get(j).getImageUrl());
                    } else {
                        galleryBean.setVideoUrl(resolutionModel
                                .getResponseDataBean()
                                .getComments()
                                .get(i)
                                .getGallery().get(j).getVideoUrl());
                    }
                    galleryBeenList.add(galleryBean);
                }
                model.setCreatedDateTime(resolutionModel.getResponseDataBean().getComments().get
                        (i).getCreatedDateTime());
                model.setGallery(galleryBeenList);
                model.setBaseUrl(resolutionModel.getBaseURL());
                model.setUserType(resolutionModel.getResponseDataBean().getComments().get(i).getUserType());
                tempList.add(model);


            }
            Collections.sort(tempList, commentsComparator);
            commentsModelsList.addAll(tempList);
            mAdapterComments.notifyDataSetChanged();
        }

    }


    private void resolvedLayoutChanges() {
        if (resolutionModel.getStatus().equalsIgnoreCase("closed")) {
            resolution_reopen.setVisibility(View.VISIBLE);
            relative_bottom.setVisibility(View.VISIBLE);
            resolution_add_comment.setVisibility(View.GONE);
            resolution_detail_marked_resolved.setText("PROBLEM RESOLVED");
            resolution_detail_marked_resolved.setBackgroundColor(ContextCompat.getColor(this, R
                    .color.buttonDisabledGray));
            resolution_detail_marked_resolved.setEnabled(false);
            relative_bottom.setBackgroundColor(ContextCompat.getColor(this, R.color.closed_layout_color));
            recyclerViewResolutionDetail.setBackgroundColor(ContextCompat.getColor(this, R.color
                    .closed_layout_color));

        }
    }


    Comparator<CommentsModel> commentsComparator = new Comparator<CommentsModel>() {

        @Override
        public int compare(CommentsModel h1, CommentsModel h2) {
            boolean value = h2.getCreatedDateTime() < h1.getCreatedDateTime();
            return value ? 1 : -1;
        }
    };

    @Override
    public void showProgress() {
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progress.setVisibility(View.GONE);
    }

    @Override
    public void setError(String msg) {

    }

    @Override
    public void navigateToHome() {
        finish();
    }

    @Override
    public void onEnquiryFetchSuccess(ResolutionResponse resolutionResponse) {

    }

    @Override
    public void onEnquiryFetchError(String msg) {

    }

    @Override
    public void onEnquiryDataNotFound(String msg) {

    }

    @Override
    public void onProblemResolvedSucess() {
        Log.d(TAG, "onProblemResolvedSucess: ");
        showproblemResolveDialog();
    }

    @Override
    public void onProblemResolvedError(String msg) {
        Utils.getMsgPopup("Error", "Unable to resolve Problem", MyResolutionDetailScreen
                .this);
    }


    @Override
    public void onAddCommentSuccess(Object data) {

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        AddcommentResponse addcommentResponse = (AddcommentResponse) data;
        Log.d(TAG, "onAddCommentSuccess: ");

        commentsModelsList.clear();
        getCommentsRefreshed(addcommentResponse);
    }

    @Override
    public void onAddCommentError(String msg) {

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        Log.d(TAG, "onAddCommentError: ");
        Utils.getMsgPopup("Error", "Unable to resolve Problem", MyResolutionDetailScreen
                .this);
    }


    private void getCommentsRefreshed(AddcommentResponse addcommentResponse) {
        Log.d(TAG, "getCommentsRefreshed: ");
        List<CommentsModel> tempList = new ArrayList<>();
        commentsModelsList.add(baseCommentsModel);
        if (!addcommentResponse.getResponseData().getComments().isEmpty()) {
            for (int i = 0; i < addcommentResponse.getResponseData().getComments().size(); i++) {

                CommentsModel model = new CommentsModel();
                List<ResolutionResponse.ResponseDataBean.ResolutionListBean.GalleryBean>
                        galleryBeenList = new ArrayList<>();
                model.setComDesc(addcommentResponse.getResponseData().getComments().get(i).getComDesc());
                for (int j = 0; j < addcommentResponse.getResponseData().getComments().get(i)
                        .getGallery().size(); j++) {
                    ResolutionResponse.ResponseDataBean.ResolutionListBean.GalleryBean
                            galleryBean = new ResolutionResponse.ResponseDataBean
                            .ResolutionListBean.GalleryBean();
                    galleryBean.setId(addcommentResponse.getResponseData().getComments().get(i)
                            .getGallery().get(j).getId());
                    if(addcommentResponse.getResponseData()
                            .getComments()
                            .get(i)
                            .getGallery().get(j).getImageUrl() != null) {
                        galleryBean.setImageUrl(addcommentResponse.getResponseData()
                                .getComments()
                                .get(i)
                                .getGallery().get(j).getImageUrl());
                    } else {
                        galleryBean.setVideoUrl(addcommentResponse.getResponseData()
                                .getComments()
                                .get(i)
                                .getGallery().get(j).getVideoUrl());
                    }
                    galleryBeenList.add(galleryBean);
                }
                model.setCreatedDateTime(addcommentResponse.getResponseData().getComments().get
                        (i).getCreatedDateTime());
                model.setGallery(galleryBeenList);
                model.setBaseUrl(addcommentResponse.getResponseData().getBaseUrl());
                model.setUserType(addcommentResponse.getResponseData().getComments().get(i).getUserType());
                tempList.add(model);

            }
            Collections.sort(tempList, commentsComparator);
            commentsModelsList.addAll(tempList);
            mAdapterComments.notifyDataSetChanged();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(MyResolutionDetailScreen.this, DashboardScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(int adapterPosition, Boolean image, Object data) {
        Log.d(TAG, "onClick: ");
        Uri uri = (Uri) data;
        getEnlargeImageAddComment(uri, image);
    }

    private void getEnlargeImageAddComment(Uri uri, Boolean isImage) {

        FragmentManager fm = getSupportFragmentManager();
        // FragmentTransaction ft = fm.beginTransaction();
        imageDialogFramgent = ImageDialogFramgent.newInstance(uri, isImage);
        // Show DialogFragment
        imageDialogFramgent.setCancelable(true);
        imageDialogFramgent.show(fm, "Dialog Fragment");

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.resolution_back:
                onBackPressed();
                break;
            case R.id.resolution_add_comment:
                showSuccessDialog("");
                break;
            case R.id.resolution_detail_marked_resolved:
                ResolvedModel resolvedModel = new ResolvedModel();
                resolvedModel.setEnquiryId(resolutionModel.getProblemId());
                resolvedModel.setStatus("closed");
                resolutionStatus = "closed";
                resolutionPresenter.markAsResolved(resolvedModel);
                break;
            case R.id.text_reopen:
                ResolvedModel reopenModel = new ResolvedModel();
                reopenModel.setEnquiryId(resolutionModel.getProblemId());
                reopenModel.setStatus("open");
                resolutionStatus = "open";
                resolutionPresenter.markAsResolved(reopenModel);
                break;
        }
    }


    public void getEnlargeImage(Uri uri, Boolean isImage) {

        FragmentManager fm = getSupportFragmentManager();
        // FragmentTransaction ft = fm.beginTransaction();
        ReportImageDialogFramgent imageDialogFramgent = ReportImageDialogFramgent.newInstance(uri, isImage);
        // Show DialogFragment
        imageDialogFramgent.setCancelable(true);
        imageDialogFramgent.show(fm, "Dialog Fragment");

    }

    @Override
    public void startImageEnlarge(Uri uri, Boolean isImage) {
        getEnlargeImage(uri, isImage);
    }

    public void showSuccessDialog(String problemId) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.popup_add_comment, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        final AlertDialog b = dialogBuilder.create();

        uriListImage = new ArrayList<>();
        uriListVideo = new ArrayList<>();

        resolution_add_comment_cancel = (ImageView) dialogView.findViewById(R.id
                .resolution_add_comment_cancel);

        resolution_add_comment_send = (ImageView) dialogView.findViewById(R.id
                .resolution_add_comment_send);

        report_problem_add_image = (Button) dialogView.findViewById(R.id.report_problem_add_image);

        report_problem_add_video = (Button) dialogView.findViewById(R.id.report_problem_add_video);

        //add comment add image
        recyclerview_add_photo = (RecyclerView) dialogView.findViewById(R.id
                .recyclerview_add_photo);
        recyclerview_add_video = (RecyclerView) dialogView.findViewById(R.id.recyclerview_add_video);
        RecyclerView.LayoutManager layoutManager_add_image = new LinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL, false);
        recyclerview_add_photo.setLayoutManager(layoutManager_add_image);
        recyclerview_add_photo.setItemAnimator(new DefaultItemAnimator());
        mImageAdapter = new ReportProblemImagesAdapter(uriListImage, this);
        recyclerview_add_photo.setAdapter(mImageAdapter);

        mVideoAdapter = new ReportProblemVideosAdapter(uriListVideo, this);
        recyclerview_add_video.setAdapter(mVideoAdapter);

        //add comment add video
        RecyclerView.LayoutManager layoutManager_add_video = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        recyclerview_add_video = (RecyclerView) dialogView.findViewById(R.id
                .recyclerview_add_video);
        recyclerview_add_video.setLayoutManager(layoutManager_add_video);
        recyclerview_add_video.setItemAnimator(new DefaultItemAnimator());

        report_problem_desc = (EditText) dialogView.findViewById(R.id.report_problem_desc);


        resolution_add_comment_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                b.dismiss();
            }
        });


        resolution_add_comment_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                b.dismiss();

                if (uriListImage.isEmpty()) {
                    Utils.getMsgPopup("Error", "Please Add at least one Image.",
                            MyResolutionDetailScreen.this);
                    return;
                }
                if (TextUtils.isEmpty(report_problem_desc.getText().toString())) {
                    Utils.getMsgPopup("Error", "Please provide description.",
                            MyResolutionDetailScreen.this);
                    report_problem_desc.requestFocus();
                    return;
                }

                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                resolutionPresenter.addComment(uriListImage, uriListVideo, report_problem_desc
                        .getText()
                        .toString(), resolutionModel.getProblemId());
                b.dismiss();

            }
        });


        report_problem_add_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uriListVideo.size() >= 1) {
                    Utils.getMsgPopup("Error", "You can upload only one video for a problem , " +
                            "" +
                            "", MyResolutionDetailScreen.this);
                } else {
                    selectVideo();
                }
            }
        });

        report_problem_add_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uriListImage.size() > 4) {
                    Utils.getMsgPopup("Error", "You have exceeded image limit, Please Delete  " +
                            "image to add more. ", MyResolutionDetailScreen.this);
                } else {
                    getCameraGallery();


                }
            }
        });


        b.show();
    }


    private void getCameraGallery() {
        selectImage();
    }


    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(MyResolutionDetailScreen.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean cameraResult = Utils.checkCameraPermission(MyResolutionDetailScreen.this);
                boolean storegeResult = Utils.checkStoragePermission(MyResolutionDetailScreen.this);
                if (items[item].equals("Take Photo")) {

                    userChoosenTask = "Take Photo";
                    if (cameraResult)
                        cameraIntent();
                } else if (items[item].equals("Choose from Library")) {

                    userChoosenTask = "Choose from Library";
                    if (storegeResult)
                        galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void selectVideo() {
        final CharSequence[] items = {"Take Video",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(MyResolutionDetailScreen.this);
        builder.setTitle("Add Video!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean cameraResult = Utils.checkCameraPermission(MyResolutionDetailScreen.this);
//                boolean storegeResult = Utils.checkStoragePermission(MyResolutionDetailScreen.this);
                if (items[item].equals("Take Video")) {

                    userChoosenTaskVideo = "Take Video";
                    if (cameraResult)
                        dispatchTakeVideoIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    static final int REQUEST_VIDEO_CAPTURE = 1;

    private void dispatchTakeVideoIntent() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
            takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 30);
            takeVideoIntent.putExtra(android.provider.MediaStore.EXTRA_VIDEO_QUALITY, 0);
            startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
        }
    }

    private void cameraIntent() {
        boolean storegeResult = Utils.checkStoragePermission(MyResolutionDetailScreen.this);
        if(storegeResult) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            imageUri = getImageUri(this);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(intent, REQUEST_CAMERA);
        }
    }


    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utils.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                    else if (userChoosenTaskVideo.equals("Take Video"))
                        dispatchTakeVideoIntent();
                } else {
                    //code for deny
                }
                break;
            case Utils.MY_PERMISSIONS_REQUEST_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                    else if (userChoosenTaskVideo.equals("Take Video"))
                        dispatchTakeVideoIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
            else if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_OK) {
                Uri videoUri = data.getData();
//                mVideoView.setVideoURI(videoUri);
                mVideoAdapter.addList(videoUri);
                mVideoAdapter.notifyDataSetChanged();
                Log.d(TAG, "onActivityResult video: " + uriListVideo.size());
            }
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            try {
                Log.d(TAG, "onSelectFromGalleryResult: " + data.getData());
//                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                mImageAdapter.addList(data.getData());
                mImageAdapter.notifyDataSetChanged();
                Log.d(TAG, "onCaptureImageResult: " + uriListImage.size());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void onCaptureImageResult(Intent data) {
        Log.d(TAG, "onCaptureImageResult: ");
        try {
            mImageAdapter.addList(imageUri);
            mImageAdapter.notifyDataSetChanged();
            Log.d(TAG, "onCaptureImageResult: " + uriListImage.size());

        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    @Override
    public void onDelete(Uri uri, Boolean isImage) {

        if(isImage) {
            mImageAdapter.deleteList(uri);
            imageDialogFramgent.dismiss();
            imageDialogFramgent = null;
        } else {
            mVideoAdapter.deleteList(uri);
            imageDialogFramgent.dismiss();
            imageDialogFramgent = null;
        }
    }

    public void showproblemResolveDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.popup_report_problem, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView report_problem_popup_problem_id = (TextView) dialogView.findViewById(R.id
                .report_problem_popup_problem_id);
        TextView report_problem_popup_title = (TextView) dialogView.findViewById(R.id
                .report_problem_popup_title);
        ImageView success_image = (ImageView) dialogView.findViewById(R.id.success_image);
        success_image.setImageResource(R.drawable.ic_smiley);
        LinearLayout report_problem_ok_layout = (LinearLayout) dialogView.findViewById(
                R.id.report_problem_ok_layout);

        if(resolutionStatus == "closed") {
            report_problem_popup_title.setText("Problem marked\nas resolved.");
        } else {
            report_problem_popup_title.setText("Problem has been\nre-opened.");
        }
        report_problem_popup_problem_id.setText("Glad we could help! ");

        report_problem_ok_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToHome();
            }
        });


        AlertDialog b = dialogBuilder.create();
        b.show();
    }
}
