package com.kfs.expertsystems.activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kfs.expertsystems.R;
import com.kfs.expertsystems.callbacks.OnClickItemListener;
import com.kfs.expertsystems.fragments.DealersListFragment;
import com.kfs.expertsystems.fragments.DealersMapFragment;
import com.kfs.expertsystems.models.DealersResponse;
import com.kfs.expertsystems.presenter.presenter.DealersPresenter;
import com.kfs.expertsystems.presenter.presenterImpl.DealersPresenterImpl;
import com.kfs.expertsystems.presenter.views.DealersView;
import com.kfs.expertsystems.utils.Utils;

public class MyDealersScreen extends AppCompatActivity implements OnClickItemListener,
        DealersView, View.OnClickListener {

    private final String TAG = MyDealersScreen.class.getName();
    Fragment fr;
    Boolean map = false;
    FloatingActionButton fab;
    private ProgressBar progressBar;
    private DealersPresenter dealersPresenter;
    private DealersResponse dealers;
    private RelativeLayout empty_screen_layout;
    private TextView emptyText;
    private TextView tryAgain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_dealers_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initUi();

    }

    private void initUi() {


        dealersPresenter = new DealersPresenterImpl(this);
        progressBar = (ProgressBar) findViewById(R.id.progress);

        empty_screen_layout = (RelativeLayout) findViewById(R.id.empty_layout);
        emptyText = (TextView) findViewById(R.id.cant_load_text);
        tryAgain = (TextView) findViewById(R.id.try_again);
        tryAgain.setOnClickListener(this);


        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setVisibility(View.GONE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!map) {
                    map = true;
                    fab.setImageResource(R.drawable.ic_hamburger);
                    DealersMapFragment dmf = new DealersMapFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("DATA", dealers);
                    dmf.setArguments(bundle);
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.frame_list_map, dmf, "DMF");
                    fragmentTransaction.commit();
                } else {
                    map = false;
                    fab.setImageResource(R.drawable.ic_map);
                  /*  fr = new DealersListFragment();
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.anim.flip_right_in, R.anim.flip_right_out, R.anim.flip_left_in, R.anim.flip_left_out);
                    fragmentTransaction.replace(R.id.frame_list_map, fr);
                    fragmentTransaction.commit();*/
                    DealersListFragment dlf = new DealersListFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("DATA", dealers);
                    dlf.setArguments(bundle);
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.frame_list_map, dlf, "DLF");
                    fragmentTransaction.commit();
                }
            }
        });

        dealersPresenter.getDealer();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(int adapterPosition, Object data) {
        DealersResponse.ResponseDataBean.DealerBean selectedDealer = (DealersResponse.ResponseDataBean.DealerBean) data;
        Intent i = new Intent(MyDealersScreen.this, MapsActivity.class);
        i.putExtra("LAT", selectedDealer.getDealerLat());
        i.putExtra("LONG", selectedDealer.getDealerLong());
        i.putExtra("NAME", selectedDealer.getDealerName());
        startActivity(i);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setError(String msg) {

    }

    @Override
    public void navigateToHome() {

    }

    @Override
    public void onDataFound(DealersResponse dealers) {
        Log.d(TAG, "onDataFound: " + dealers.getResponseData().getDealer().size());
        empty_screen_layout.setVisibility(View.GONE);
        this.dealers = dealers;
        if (!this.dealers.getResponseData().getDealer().isEmpty()) {
            fab.setVisibility(View.VISIBLE);
            DealersListFragment dlf = new DealersListFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("DATA", dealers);
            dlf.setArguments(bundle);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame_list_map, dlf, "DLF");
            fragmentTransaction.commit();
        } else {
            onDataNotFound("No Dealers found.");
        }

    }

    @Override
    public void onError(String msg) {

        Log.d(TAG, "onError: " + msg);
        Utils.getMsgPopup("Error", msg, MyDealersScreen.this);
        empty_screen_layout.setVisibility(View.VISIBLE);
        emptyText.setText(R.string.cant_load);
    }

    @Override
    public void onDataNotFound(String msg) {

        Log.d(TAG, "onDataNotFound: " + msg);
        Utils.getMsgPopup("Error", msg, MyDealersScreen.this);
        empty_screen_layout.setVisibility(View.VISIBLE);
        emptyText.setText(R.string.cant_load);
    }


    @Override
    protected void onDestroy() {
        dealersPresenter.onDestroy();
        super.onDestroy();

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.try_again:
                dealersPresenter.getDealer();
                break;
        }
    }
}
