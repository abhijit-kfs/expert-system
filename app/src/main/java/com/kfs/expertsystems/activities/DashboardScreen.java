package com.kfs.expertsystems.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.kfs.expertsystems.R;
import com.kfs.expertsystems.adapters.DashboardAdapter;
import com.kfs.expertsystems.models.PromotionsResponse;
import com.kfs.expertsystems.presenter.presenter.PromotionPresenter;
import com.kfs.expertsystems.presenter.presenterImpl.PromotionPresenterImpl;
import com.kfs.expertsystems.presenter.views.PromotionsView;
import com.kfs.expertsystems.utils.AlertDialogHelper;
import com.kfs.expertsystems.utils.Constants;
import com.kfs.expertsystems.utils.Utils;


public class DashboardScreen extends AppCompatActivity implements View.OnClickListener,
        View.OnTouchListener, PromotionsView {

    private final String TAG = DashboardScreen.class.getName();
    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;
    Toolbar mToolbar;
    private RecyclerView recyclerView;
    private DashboardAdapter mAdapter;
    private AppBarLayout appBarLayout;
    private TextView textViewFindDealers, textViewMyResolution, textViewReportProblem, textViewNews, textViewProducts;
    private LinearLayout linearLayoutReportProblem, linearLayoutMyresolution, linearLayoutDealers, linearLayoutNews, linearLayoutProducts;
    private RelativeLayout relativeLayoutAppBar;
    private PromotionPresenter presenter;
    private ActionBarDrawerToggle mDrawerToggle;
    private LinearLayout drawer_layout_my_profile, drawer_layout_my_notification,
            drawer_layout_change_password, drawer_layout_delete_account,
            drawer_layout_contacts_us, drawer_layout_about;
    private TextView drawer_layout_title_eng, drawer_layout_title_bur;
    private Switch drawer_layout_language_switch;
    private ProgressBar progress;
    private RelativeLayout empty_screen_layout;
    private TextView emptyText;
    private ImageView hamburgerIcon;

    private int viewId = 0;

    private PromotionsResponse promotionsResponse;

    public void getPromotions() {
        presenter.getPromotions();
    }

    @Override
    public void hideProgress() {
        progress.setVisibility(View.GONE);
    }

    private void initUI() {
        setUpNavigationDrawer();
        setupToolbar();
//        setupNavigationView();
        setupDrawerLayout();


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(DashboardScreen.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        presenter = new PromotionPresenterImpl(this);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(final AppBarLayout appBarLayout, int verticalOffset) {
                //Initialize the size of the scroll
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                //Check if the view is collapsed
                if (scrollRange + verticalOffset == 0) {
                    appBarLayout.setBackgroundColor(ContextCompat.getColor(DashboardScreen.this, R.color.white));
                    textViewFindDealers.setTextColor(ContextCompat.getColor(DashboardScreen.this, R.color.textDarkGray));
                    textViewReportProblem.setTextColor(ContextCompat.getColor(DashboardScreen.this, R.color.textDarkGray));
                    textViewMyResolution.setTextColor(ContextCompat.getColor(DashboardScreen.this, R.color.textDarkGray));
                    textViewNews.setTextColor(ContextCompat.getColor(DashboardScreen.this, R.color.textDarkGray));
                    textViewProducts.setTextColor(ContextCompat.getColor(DashboardScreen.this, R.color.textDarkGray));

                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) linearLayoutReportProblem.getLayoutParams();
                    params.setMargins(0, 30, 0, 50);
                    linearLayoutReportProblem.setLayoutParams(params);

                    LinearLayout.LayoutParams params1 = (LinearLayout.LayoutParams) linearLayoutMyresolution.getLayoutParams();
                    params1.setMargins(0, 30, 0, 50);
                    linearLayoutMyresolution.setLayoutParams(params1);

                    LinearLayout.LayoutParams params2 = (LinearLayout.LayoutParams) linearLayoutProducts.getLayoutParams();
                    params2.setMargins(0, 30, 0, 50);
                    linearLayoutProducts.setLayoutParams(params2);

                    LinearLayout.LayoutParams params3 = (LinearLayout.LayoutParams) linearLayoutDealers.getLayoutParams();
                    params3.setMargins(10, 30, 10, 50);
                    linearLayoutDealers.setLayoutParams(params3);

                    LinearLayout.LayoutParams params4 = (LinearLayout.LayoutParams) linearLayoutNews.getLayoutParams();
                    params4.setMargins(10, 30, 10, 50);
                    linearLayoutNews.setLayoutParams(params4);

                    relativeLayoutAppBar.setBackgroundColor(ContextCompat.getColor(DashboardScreen.this, R.color.transparent));
                } else {
                    appBarLayout.setBackgroundColor(ContextCompat.getColor(DashboardScreen.this, R.color.transparent));
                    textViewFindDealers.setTextColor(ContextCompat.getColor(DashboardScreen.this, R.color.white));
                    textViewReportProblem.setTextColor(ContextCompat.getColor(DashboardScreen.this, R.color.white));
                    textViewMyResolution.setTextColor(ContextCompat.getColor(DashboardScreen.this, R.color.white));
                    textViewNews.setTextColor(ContextCompat.getColor(DashboardScreen.this, R.color.white));
                    textViewProducts.setTextColor(ContextCompat.getColor(DashboardScreen.this, R.color.white));


                    LinearLayout.LayoutParams params3 = (LinearLayout.LayoutParams) linearLayoutDealers.getLayoutParams();
                    params3.setMargins(10, 10, 0, 20);
                    linearLayoutDealers.setLayoutParams(params3);

                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) linearLayoutReportProblem.getLayoutParams();
                    params.setMargins(0, 35, 0, 20);
                    linearLayoutReportProblem.setLayoutParams(params);

                    LinearLayout.LayoutParams params1 = (LinearLayout.LayoutParams) linearLayoutMyresolution.getLayoutParams();
                    params1.setMargins(0, 40, 0, -10);
                    linearLayoutMyresolution.setLayoutParams(params1);

                    LinearLayout.LayoutParams params4 = (LinearLayout.LayoutParams) linearLayoutNews.getLayoutParams();
                    params4.setMargins(0, 35, 10, 40);
                    linearLayoutNews.setLayoutParams(params4);

                    LinearLayout.LayoutParams params2 = (LinearLayout.LayoutParams) linearLayoutProducts.getLayoutParams();
                    params2.setMargins(0, 10, 10, 40);
                    linearLayoutProducts.setLayoutParams(params2);

                    relativeLayoutAppBar.setBackgroundResource(R.drawable.dashboard_gradient);
                }
            }
        });

        getPromotions();
    }

    @Override
    public void navigateToHome() {

    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerVisible(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (viewId == v.getId()) {
            return true;
        }
        else {
            viewId = v.getId();
        }

        switch (v.getId()) {
            case R.id.linear_dealers:
                //startActivity(new Intent(DashboardScreen.this, MyDealersScreen.class));
                openLinkInBrowser();
                return true;
            case R.id.linear_report_problem:
                startActivity(new Intent(DashboardScreen.this, CropListScreen.class));
                return true;
            case R.id.linear_my_resolution:
                startActivity(new Intent(DashboardScreen.this, MyResolutionsListScreen.class));
                return true;
            case R.id.linear_news:
                startActivity(new Intent(DashboardScreen.this, NewsArticlesListScreen.class));
                return true;
            case R.id.linear_products:
                startActivity(new Intent(DashboardScreen.this, ProductTypeActivity.class));
                return true;
            case R.id.hamburger_icon:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
            case R.id.drawer_layout_my_profile:
                Log.d(TAG, "onClick: drawer_layout_my_profile");
                mDrawerLayout.closeDrawer(GravityCompat.START);
                startActivity(new Intent(DashboardScreen.this, ProfileScreen.class));
                break;

            case R.id.drawer_layout_my_notification:
                Log.d(TAG, "onClick: drawer_layout_my_notification");
                mDrawerLayout.closeDrawer(GravityCompat.START);
                break;

            case R.id.drawer_layout_change_password:
                Log.d(TAG, "onClick: drawer_layout_change_password");
                mDrawerLayout.closeDrawer(GravityCompat.START);
                startActivity(new Intent(DashboardScreen.this, ChangePassword.class));
                break;

            case R.id.drawer_layout_delete_account:
                Log.d(TAG, "onClick: drawer_layout_delete_account");
                mDrawerLayout.closeDrawer(GravityCompat.START);
                AlertDialogHelper.logoutDialog(DashboardScreen.this, LoginScreen.class);
                break;

            case R.id.drawer_layout_about:
                Log.d(TAG, "onClick: drawer_layout_about");
                mDrawerLayout.closeDrawer(GravityCompat.START);
                startActivity(new Intent(DashboardScreen.this, AboutUsScreen.class));
                break;
        }
        return false;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.linear_dealers:
                //startActivity(new Intent(DashboardScreen.this, MyDealersScreen.class));
                openLinkInBrowser();
                break;
            case R.id.linear_report_problem:
                startActivity(new Intent(DashboardScreen.this, CropListScreen.class));
                break;
            case R.id.linear_my_resolution:
                startActivity(new Intent(DashboardScreen.this, MyResolutionsListScreen.class));
                break;
            case R.id.linear_news:
                startActivity(new Intent(DashboardScreen.this, NewsArticlesListScreen.class));
                break;

            case R.id.linear_products:
                startActivity(new Intent(DashboardScreen.this, ProductTypeActivity.class));
                break;

            case R.id.drawer_layout_my_profile:
                Log.d(TAG, "onClick: drawer_layout_my_profile");
                mDrawerLayout.closeDrawer(GravityCompat.START);
                startActivity(new Intent(DashboardScreen.this, ProfileScreen.class));
                break;

            case R.id.drawer_layout_my_notification:
                Log.d(TAG, "onClick: drawer_layout_my_notification");
                mDrawerLayout.closeDrawer(GravityCompat.START);
                break;

            case R.id.drawer_layout_change_password:
                Log.d(TAG, "onClick: drawer_layout_change_password");
                mDrawerLayout.closeDrawer(GravityCompat.START);
                startActivity(new Intent(DashboardScreen.this, ChangePassword.class));
                break;

            case R.id.drawer_layout_delete_account:
                Log.d(TAG, "onClick: drawer_layout_delete_account");
                mDrawerLayout.closeDrawer(GravityCompat.START);
                AlertDialogHelper.logoutDialog(DashboardScreen.this, LoginScreen.class);
                break;

            case R.id.drawer_layout_about:
                Log.d(TAG, "onClick: drawer_layout_about");
                mDrawerLayout.closeDrawer(GravityCompat.START);
                startActivity(new Intent(DashboardScreen.this, AboutUsScreen.class));
                break;
        }
    }

    private void openLinkInBrowser() {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(Constants.WEBSITE_URL_PROMOTIONAL));
        startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_drawer_layout);
        appBarLayout = (AppBarLayout) findViewById(R.id.ablHome);
        textViewFindDealers = (TextView) findViewById(R.id.textview_find_dealers);
        textViewMyResolution = (TextView) findViewById(R.id.textview_report_problem);
        textViewReportProblem = (TextView) findViewById(R.id.textview_my_resolution);
        textViewNews = (TextView) findViewById(R.id.textview_news);
        textViewProducts = (TextView) findViewById(R.id.textview_products);

        hamburgerIcon = findViewById(R.id.hamburger_icon);
        hamburgerIcon.setOnTouchListener(this);


        linearLayoutReportProblem = (LinearLayout) findViewById(R.id.linear_report_problem);
        linearLayoutReportProblem.setOnTouchListener(this);
//        linearLayoutReportProblem.setOnClickListener(this);

        linearLayoutMyresolution = (LinearLayout) findViewById(R.id.linear_my_resolution);
        linearLayoutMyresolution.setOnTouchListener(this);
//        linearLayoutMyresolution.setOnClickListener(this);

        linearLayoutDealers = (LinearLayout) findViewById(R.id.linear_dealers);
        linearLayoutDealers.setOnTouchListener(this);
//        linearLayoutDealers.setOnClickListener(this);

        linearLayoutNews = (LinearLayout) findViewById(R.id.linear_news);
        linearLayoutNews.setOnTouchListener(this);
//        linearLayoutNews.setOnClickListener(this);

        linearLayoutProducts = (LinearLayout) findViewById(R.id.linear_products);
        linearLayoutProducts.setOnTouchListener(this);
//        linearLayoutProducts.setOnClickListener(this);

        relativeLayoutAppBar = (RelativeLayout) findViewById(R.id.relative_main);
        progress = (ProgressBar) findViewById(R.id.progress);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview_dashboard);
        empty_screen_layout = (RelativeLayout) findViewById(R.id.empty_layout);
        emptyText = (TextView) findViewById(R.id.cant_load_text);
        initUI();

    }

    @Override
    protected void onResume() {
        super.onResume();

        viewId = 0;

        if (ActivityCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission_group.STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(getApplicationContext(),
                        Manifest.permission_group.LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this
                    , new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION}
                    , 1000);
        }
    }

    @Override
    public void onDataFound(PromotionsResponse promotions) {
        Log.d(TAG, "onDataFound: " + promotions.getResponseData().getPromos().size());
        empty_screen_layout.setVisibility(View.GONE);

        promotionsResponse = promotions;

        mAdapter = new DashboardAdapter(DashboardScreen.this, promotionsResponse.getResponseData().getPromos(),
                promotionsResponse.getResponseData().getImageBaseUrl());
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onDataNotFound(String msg) {
        Log.d(TAG, "onDataNotFound: " + msg);
        if (!msg.isEmpty()) {
            Utils.getMsgPopup("Error", msg, DashboardScreen.this);
        }
        empty_screen_layout.setVisibility(View.VISIBLE);
        emptyText.setText(R.string.no_data_found);
    }

    @Override
    public void setError(String msg) {
        Log.d(TAG, "setError: ");
        if (!msg.isEmpty()) {
            Utils.getMsgPopup("Error", msg, DashboardScreen.this);
        }
        empty_screen_layout.setVisibility(View.VISIBLE);
        emptyText.setText(R.string.cant_load);
    }

    private void setUpNavigationDrawer() {

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);

        drawer_layout_my_profile = (LinearLayout) mNavigationView.findViewById(R.id
                .drawer_layout_my_profile);
        drawer_layout_my_profile.setOnTouchListener(this);

        drawer_layout_my_notification = (LinearLayout) mNavigationView.findViewById(R.id
                .drawer_layout_my_notification);
        drawer_layout_my_notification.setOnTouchListener(this);

        drawer_layout_change_password = (LinearLayout) mNavigationView.findViewById(R.id
                .drawer_layout_change_password);
        drawer_layout_change_password.setOnTouchListener(this);

        drawer_layout_delete_account = (LinearLayout) mNavigationView.findViewById(R.id
                .drawer_layout_delete_account);
        drawer_layout_delete_account.setOnTouchListener(this);


        drawer_layout_contacts_us = (LinearLayout) mNavigationView.findViewById(R.id
                .drawer_layout_contacts_us);
        drawer_layout_contacts_us.setOnTouchListener(this);

        drawer_layout_about = (LinearLayout) mNavigationView.findViewById(R.id
                .drawer_layout_about);
        drawer_layout_about.setOnTouchListener(this);

        drawer_layout_language_switch = (Switch) mNavigationView.findViewById(R.id
                .drawer_layout_language_switch);

        drawer_layout_title_eng = (TextView) mNavigationView.findViewById(R.id
                .drawer_layout_title_eng);
        drawer_layout_title_bur = (TextView) mNavigationView.findViewById(R.id
                .drawer_layout_title_bur);

        drawer_layout_language_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    drawer_layout_title_bur.setTypeface(null, Typeface.BOLD);
                    drawer_layout_title_eng.setTypeface(null, Typeface.NORMAL);
                } else {
                    drawer_layout_title_eng.setTypeface(null, Typeface.BOLD);
                    drawer_layout_title_bur.setTypeface(null, Typeface.NORMAL);
                }
            }
        });

    }

    private void setupDrawerLayout() {
        mDrawerToggle = setupDrawerToggle();
        mDrawerLayout.addDrawerListener(mDrawerToggle);
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string
                .navigation_drawer_open, R.string.navigation_drawer_close);
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar();
    }

    @Override
    public void showProgress() {
        progress.setVisibility(View.VISIBLE);
    }
}
