package com.kfs.expertsystems.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kfs.expertsystems.R;
import com.kfs.expertsystems.adapters.ResolutionListAdapter;
import com.kfs.expertsystems.callbacks.OnClickItemListener;
import com.kfs.expertsystems.models.ResolutionModel;
import com.kfs.expertsystems.models.ResolutionResponse;
import com.kfs.expertsystems.presenter.presenter.ResolutionPresenter;
import com.kfs.expertsystems.presenter.presenterImpl.ResolutionPresenterImpl;
import com.kfs.expertsystems.presenter.views.ResolutionView;

import java.util.ArrayList;
import java.util.List;

public class MyResolutionsListScreen extends AppCompatActivity implements OnClickItemListener,
        ResolutionView, View.OnClickListener {

    private final String TAG = MyResolutionsListScreen.class.getName();
    private RecyclerView mRecyclerView;
    private ResolutionListAdapter mAdapter;
    private List<ResolutionModel> resolutionList ;
    private ProgressBar progress;
    private ResolutionPresenter resolutionPresenter;
    private RelativeLayout empty_screen_layout;
    private TextView emptyText;
    private TextView tryAgain;
    private ResolutionResponse resolutionResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_resolutions_list_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initUI();
//        prepareResolutionData();

    }

    private void initUI() {
        resolutionPresenter = new ResolutionPresenterImpl(this);
        progress = (ProgressBar) findViewById(R.id.progress);
        empty_screen_layout = (RelativeLayout) findViewById(R.id.empty_layout);
        emptyText = (TextView) findViewById(R.id.cant_load_text);
        tryAgain = (TextView) findViewById(R.id.try_again);
        tryAgain.setOnClickListener(this);


        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview_myresolution);



        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MyResolutionsListScreen.this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

    }


    void setResolutionData() {

        resolutionPresenter.getEnquiryList();

    }

    @Override
    protected void onResume() {
        super.onResume();
        setResolutionData();
    }

    private void prepareResolutionData(ResolutionResponse resolutionResponse) {
        Log.d(TAG, "prepareResolutionData: ");

        resolutionList = new ArrayList<>();
        mAdapter = new ResolutionListAdapter(this, resolutionList);
        mRecyclerView.setAdapter(mAdapter);



        if (!resolutionResponse.getResponseData().getResolutionList().isEmpty()) {
            showProgress();
            for (int i = 0; i < resolutionResponse.getResponseData().getResolutionList().size(); i++) {

                ResolutionModel resolutionModel = new ResolutionModel();

                resolutionModel.setProblemId(resolutionResponse.getResponseData().getResolutionList().get(i).getId());
                resolutionModel.setDesc(resolutionResponse.getResponseData().getResolutionList()
                        .get(i).getEqDesc());
                resolutionModel.setStatus(resolutionResponse.getResponseData().getResolutionList().get(i)
                        .getEqStatus());
                resolutionModel.setCreatedDate(resolutionResponse.getResponseData().getResolutionList().get(i)
                        .getCreatedDateTime());
                resolutionModel.setBaseURL(resolutionResponse.getResponseData().getImageBaseUrl());
//crops
                resolutionModel.setCropName(resolutionResponse.getResponseData().getResolutionList().get(i)
                        .getCropModel().getCropName());
                resolutionModel.setCropId(resolutionResponse.getResponseData().getResolutionList().get(i)
                        .getCropModel().getId());
                resolutionModel.setCropImage(resolutionResponse.getResponseData().getResolutionList().get(i)
                        .getCropModel().getCropImageUrl());

//stage
//                resolutionModel.setStageName(resolutionResponse.getResponseData().getResolutionList().get(i)
//                        .getStages().getStageName());
//                resolutionModel.setStageId(resolutionResponse.getResponseData().getResolutionList().get(i)
//                        .getStages().getId());


                resolutionModel.setResponseDataBean(resolutionResponse.getResponseData()
                        .getResolutionList().get(i));

                resolutionList.add(resolutionModel);


            }
            mAdapter.notifyDataSetChanged();
            hideProgress();
        } else {
            Log.d(TAG, "prepareResolutionData: no enquiry available ");
            onEnquiryDataNotFound("no enquiry available");
        }


    }


    @Override
    public void showProgress() {
        Log.d(TAG, "showProgress: ");
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        Log.d(TAG, "hideProgress: ");
        progress.setVisibility(View.GONE);
    }

    @Override
    public void setError(String msg) {

    }

    @Override
    public void navigateToHome() {
        Intent intent = new Intent(MyResolutionsListScreen.this, DashboardScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onEnquiryFetchSuccess(ResolutionResponse resolutionResponse) {
        Log.d(TAG, "onEnquiryFetchSuccess: " + resolutionResponse.getResponseData().getResolutionList().size());
        empty_screen_layout.setVisibility(View.GONE);
        this.resolutionResponse = resolutionResponse;
        prepareResolutionData(resolutionResponse);
    }

    @Override
    public void onEnquiryFetchError(String msg) {
        Log.d(TAG, "onEnquiryFetchError: ");
        empty_screen_layout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onEnquiryDataNotFound(String msg) {
        Log.d(TAG, "onEnquiryDataNotFound: ");
        empty_screen_layout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onProblemResolvedSucess() {

    }

    @Override
    public void onProblemResolvedError(String msg) {

    }

    @Override
    public void onAddCommentSuccess(Object data) {
        Log.d(TAG, "onAddCommentSuccess: ");
    }

    @Override
    public void onAddCommentError(String msg) {
        Log.d(TAG, "onAddCommentError: ");
    }


    @Override
    public void onClick(int adapterPosition, Object data) {
        ResolutionModel resolutionModel = (ResolutionModel) data;

        Intent intent = new Intent(MyResolutionsListScreen.this, MyResolutionDetailScreen.class);
        intent.putExtra("DATA",resolutionModel);
        startActivity(intent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                navigateToHome();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        switch ((view.getId())) {

            case R.id.try_again:
                setResolutionData();
                break;


        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, 0);
    }

    @Override
    protected void onDestroy() {

        resolutionPresenter.onDestroy();
        mAdapter =null;
        super.onDestroy();
    }
}
