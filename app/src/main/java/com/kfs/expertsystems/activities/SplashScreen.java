package com.kfs.expertsystems.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;

import com.kfs.expertsystems.R;
import com.kfs.expertsystems.application.AgriProApplication;
import com.kfs.expertsystems.utils.Constants;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

public class SplashScreen extends AppCompatActivity {

    private void checkLoginStatus() {

        if (AgriProApplication.getPref().getBoolean(Constants.LOGIN)) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (AgriProApplication.getPref().getBoolean(Constants.SHOW_ADD_PASSWORD)) {
                        startActivity(new Intent(SplashScreen.this, AddPassword.class));
                    } else {
                        startActivity(new Intent(SplashScreen.this, DashboardScreen.class));
                    }
                    finish();
                }
            }, 3000);

        } else {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(SplashScreen.this, LoginScreen.class));
                    finish();
                }
            }, 2000);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        if (AgriProApplication.getPref().getInt("language") == 1) {
            setUpLanguageMode("en");
        } else if (AgriProApplication.getPref().getInt("language") == 2) {
            setUpLanguageMode("kn");
        } else {
            AgriProApplication.getPref().put("language", 1);
            setUpLanguageMode("en");
        }

//        Log.d("SPLASH", "onCreate: "+printKeyHash(SplashScreen.this));
    }

    public void setUpLanguageMode(String mode) {
        if (mode != null && !mode.isEmpty()) {
            Configuration config = getBaseContext().getResources().getConfiguration();

            Locale locale = new Locale(mode);
            Locale.setDefault(locale);
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
            checkLoginStatus();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public static String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getApplicationContext().getPackageName());
            Log.e("Package Name=", context.getApplicationContext().getPackageName());

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash=", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }
}
