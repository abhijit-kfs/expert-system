package com.kfs.expertsystems.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.login.widget.LoginButton;
import com.kfs.expertsystems.R;
import com.kfs.expertsystems.application.AgriProApplication;
import com.kfs.expertsystems.presenter.presenter.LoginPresenter;
import com.kfs.expertsystems.presenter.presenterImpl.LoginPresenterImpl;
import com.kfs.expertsystems.presenter.views.LoginView;
import com.kfs.expertsystems.utils.Constants;
import com.kfs.expertsystems.utils.Utils;

import java.util.Locale;

public class LoginScreen extends AppCompatActivity implements LoginView, View.OnClickListener {

    private final String TAG = LoginScreen.this.getClass().getName();

    private EditText editTextUserName;

    private EditText editTextPassword;

    private LoginPresenter mLoginPresenter;

    private ProgressBar mProgressBar;

    private TextView mLanguage_bur;

    private TextView mLanguage_eng;
    private LoginButton mLoginButton;
    private CallbackManager mCallbackManager;

    String langMode = "en";

    private void changeLanguage(int type) {

        mLoginPresenter.changeLanguage(type);

    }

    @Override
    public void hideProgress() {
        Log.d(TAG, "hideProgress: ");
        findViewById(R.id.login_click).setEnabled(true);
        mProgressBar.setVisibility(View.GONE);
    }

    private void initUI() {
        findViewById(R.id.login_click).setOnClickListener(this);
        findViewById(R.id.register_click).setOnClickListener(this);
        editTextUserName = (EditText) findViewById(R.id.editText_username);
        editTextPassword = (EditText) findViewById(R.id.editText_password);
        mProgressBar = (ProgressBar) findViewById(R.id.progress);

        mLanguage_eng = (TextView) findViewById(R.id.lang_eng);
        mLanguage_eng.setOnClickListener(this);

        mLanguage_bur = (TextView) findViewById(R.id.lang_bur);
        mLanguage_bur.setOnClickListener(this);

        mLoginButton = (LoginButton) findViewById(R.id.login_button);

        mLoginPresenter = new LoginPresenterImpl(LoginScreen.this, this);
        mCallbackManager = CallbackManager.Factory.create();

        mLoginPresenter.fbRegisterCallback(mCallbackManager);

        if (AgriProApplication.getPref().getInt("language") == 1) {
            langEnglish();
        } else if (AgriProApplication.getPref().getInt("language") == 2) {
            langBurma();
        }
    }

    @Override
    public void languageBurma() {
        Log.d(TAG, "languageBurma: ");
        mLanguage_eng.setTextColor(ContextCompat.getColor(LoginScreen.this, R.color
                .white));

        mLanguage_bur.setTextColor(ContextCompat.getColor(LoginScreen.this, R.color
                .lang_text_color));
        langMode = "kn";
        setLocale();
    }

    public void langBurma() {
        Log.d(TAG, "languageBurma: ");
        mLanguage_eng.setTextColor(ContextCompat.getColor(LoginScreen.this, R.color
                .white));

        mLanguage_bur.setTextColor(ContextCompat.getColor(LoginScreen.this, R.color
                .lang_text_color));
        langMode = "kn";
    }

    @Override
    public void languageEnglish() {
        Log.d(TAG, "languageEnglish: ");
        mLanguage_eng.setTextColor(ContextCompat.getColor(LoginScreen.this, R.color
                .lang_text_color));

        mLanguage_bur.setTextColor(ContextCompat.getColor(LoginScreen.this, R.color
                .white));
        langMode = "en";
        setLocale();
    }

    public void langEnglish() {
        Log.d(TAG, "languageEnglish: ");
        mLanguage_eng.setTextColor(ContextCompat.getColor(LoginScreen.this, R.color
                .lang_text_color));

        mLanguage_bur.setTextColor(ContextCompat.getColor(LoginScreen.this, R.color
                .white));
        langMode = "en";
    }

    public void setLocale() {
        Locale myLocale = new Locale(langMode);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        startActivity(new Intent(LoginScreen.this, SplashScreen.class));
        finish();
    }

    public void loginClick() {
//        if(!editTextUserName.getText().toString().isEmpty() && !editTextPassword.getText().toString().isEmpty()) {
//            startActivity(new Intent(LoginScreen.this, DashboardScreen.class));
//        }
        Utils.hideKeyboard(LoginScreen.this);
        mLoginPresenter.validateLoginCredentials(editTextUserName.getText().toString().trim(), editTextPassword
                .getText().toString().trim());

    }

    @Override
    public void navigateToHome() {
        Log.d(TAG, "navigateToHome: ");
        if (AgriProApplication.getPref().getBoolean(Constants.SHOW_ADD_PASSWORD)) {
            startActivity(new Intent(LoginScreen.this, AddPassword.class));
        } else {
            startActivity(new Intent(LoginScreen.this, DashboardScreen.class));
        }
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login_click:
                loginClick();
                break;
            case R.id.register_click:
                registerClick();
                break;

            case R.id.lang_eng:
                changeLanguage(1);
                break;
            case R.id.lang_bur:
                changeLanguage(2);
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);

        initUI();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy: ");
        mLoginPresenter.onDestroy();
        super.onDestroy();
    }

    public void registerClick() {
        Log.d(TAG, "registerClick: ");
        startActivity(new Intent(LoginScreen.this, RegisterScreen.class));
        finish();
    }

    @Override
    public void setError(String msg) {
        Log.d(TAG, "setError: ");
        Utils.getMsgPopup("Error", msg, LoginScreen.this);
    }

    @Override
    public void showProgress() {
        Log.d(TAG, "showProgress: ");
        findViewById(R.id.login_click).setEnabled(false);
        mProgressBar.setVisibility(View.VISIBLE);
    }
}
