package com.kfs.expertsystems.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.kfs.expertsystems.R;
import com.kfs.expertsystems.adapters.ReportProblemImagesAdapter;
import com.kfs.expertsystems.adapters.ReportProblemVideosAdapter;
import com.kfs.expertsystems.api.ApiClient;
import com.kfs.expertsystems.application.AgriProApplication;
import com.kfs.expertsystems.callbacks.ImageDialogCallback;
import com.kfs.expertsystems.callbacks.OnClickItemListenerwithType;
import com.kfs.expertsystems.fragments.ImageDialogFramgent;
import com.kfs.expertsystems.models.CropResponse;
import com.kfs.expertsystems.models.ReportProblemResponse;
import com.kfs.expertsystems.presenter.presenter.ReportProblemPresenter;
import com.kfs.expertsystems.presenter.presenterImpl.ReportProblemPresenterImpl;
import com.kfs.expertsystems.presenter.views.ReportProblem;
import com.kfs.expertsystems.utils.Constants;
import com.kfs.expertsystems.utils.DownloadTask;
import com.kfs.expertsystems.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.kfs.expertsystems.utils.Utils.getImageUri;

public class ReportProblemScreen extends AppCompatActivity implements View.OnClickListener,
        OnClickItemListenerwithType, ImageDialogCallback, ReportProblem, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final int REQUEST_CAMERA = 102;
    private static final int SELECT_FILE = 103;
    private static final String TAG = ReportProblemScreen.class.getName();
    private RecyclerView recyclerViewImages, recyclerViewVideos;
    private ReportProblemImagesAdapter mImageAdapter;
    private ReportProblemVideosAdapter mVideoAdapter;
    private ImageView imageViewBack;
    private ImageView report_problem_crop_image;
    private TextView report_problem_crop_title;
    private String BASE_URL;
    private Button report_problem_add_image, report_problem_add_video;
    private List<Uri> uriListImage;
    private List<Uri> uriListVideo;
    private CropResponse.ResponseDataBean.CropsBean selectedCrop;
    private CropResponse.ResponseDataBean.CropsBean.StageModelBean selectedStage;

    private String userChoosenTask = "";
    private String userChoosenTaskVideo = "";
    private Uri imageUri;

    private Button report_problem_submit;
    private EditText report_problem_desc;
    private ProgressBar progress;
    private TextView tvPdfName, tvPlayStoreLink;

    private ImageDialogFramgent imageDialogFramgent;

    private ReportProblemPresenter reportProblemPresenter;

    private LocationRequest locationRequest;
    private GoogleApiClient googleApiClient;

    private final int LOCATION_SETTINGS = 99;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_problem_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initUI();

        createLocationRequest();
        checkLocationSettings();
    }

    private void createApiClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    private void createLocationRequest() {
        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
    }

    private void checkLocationSettings() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

        task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                createApiClient();
            }
        });

        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    ResolvableApiException resolvable = (ResolvableApiException) e;
                    resolvable.startResolutionForResult(ReportProblemScreen.this,
                            LOCATION_SETTINGS);
                } catch (IntentSender.SendIntentException sendEx) {

                }
            }
        });
    }

    private void requestLocationUpdates() {
        if (googleApiClient != null && googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    googleApiClient,
                    locationRequest,
                    this
            );
        }
    }

    private void stopLocationUpdates() {
        if (googleApiClient != null && googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
            googleApiClient.disconnect();
        }
    }

    private void initUI() {

        try {
            if (getIntent().hasExtra("STAGES")) {

                selectedCrop = (CropResponse.ResponseDataBean.CropsBean) getIntent()
                        .getSerializableExtra
                                ("STAGES");


                if (getIntent().hasExtra("BASE_URL")) {
                    BASE_URL = getIntent().getStringExtra("BASE_URL");
                }
            } else {
                finish();
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
        reportProblemPresenter = new ReportProblemPresenterImpl(this);
        progress = (ProgressBar) findViewById(R.id.progress);
        recyclerViewImages = (RecyclerView) findViewById(R.id.recyclerview_add_photo);
        recyclerViewVideos = (RecyclerView) findViewById(R.id.recyclerview_add_video);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ReportProblemScreen.this, LinearLayoutManager.HORIZONTAL, false);
        recyclerViewImages.setLayoutManager(layoutManager);

        RecyclerView.LayoutManager layoutManagerVideo = new LinearLayoutManager(ReportProblemScreen
                .this, LinearLayoutManager.HORIZONTAL, false);
        recyclerViewImages.setLayoutManager(layoutManager);

        recyclerViewImages.setItemAnimator(new DefaultItemAnimator());


        recyclerViewVideos.setLayoutManager(layoutManagerVideo);
        recyclerViewVideos.setItemAnimator(new DefaultItemAnimator());
        imageViewBack = (ImageView) findViewById(R.id.report_problem_back);
        tvPdfName = findViewById(R.id.tv_pdf_name);
        tvPlayStoreLink = findViewById(R.id.tv_play_store_link);

        imageViewBack.setOnClickListener(this);

        report_problem_crop_image = (ImageView) findViewById(R.id.report_problem_crop_image);
        report_problem_crop_title = (TextView) findViewById(R.id.report_problem_crop_title);

        report_problem_add_image = (Button) findViewById(R.id.report_problem_add_image);
        report_problem_add_image.setOnClickListener(this);

        report_problem_add_video = (Button) findViewById(R.id.report_problem_add_video);
        report_problem_add_video.setOnClickListener(this);

        report_problem_submit = (Button) findViewById(R.id.report_problem_submit);
        report_problem_submit.setOnClickListener(this);

        report_problem_desc = (EditText) findViewById(R.id.report_problem_desc);

        uriListImage = new ArrayList<>();
        uriListVideo = new ArrayList<>();
        setCropData();


    }

    private void setCropData() {

        report_problem_crop_title.setText(selectedCrop.getCropName());
        Glide.with(this).load(BASE_URL + selectedCrop.getCropImageUrl())
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.ic_image_place_holder)
                .crossFade()
                .into(report_problem_crop_image);

        if (selectedCrop.getCropPDFUrl() != null && !selectedCrop.getCropPDFUrl().isEmpty())
            tvPdfName.setVisibility(View.VISIBLE);
        else tvPdfName.setVisibility(View.GONE);

        if (selectedCrop.getCropPlaystoreLink() != null && !selectedCrop.getCropPlaystoreLink().isEmpty())
            tvPlayStoreLink.setVisibility(View.VISIBLE);
        else tvPlayStoreLink.setVisibility(View.GONE);


        tvPdfName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DownloadTask(tvPdfName.getContext(),
                        BASE_URL + "/pdfs" + selectedCrop.getCropPDFUrl(),
                        selectedCrop.getCropName().replaceAll("/", "-") + ".PDF");
            }
        });

        tvPlayStoreLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*final String appPackageName = "com.loginets.property"; // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }*/

                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(selectedCrop.getCropPlaystoreLink())));
            }
        });

        mImageAdapter = new ReportProblemImagesAdapter(uriListImage, this);
        recyclerViewImages.setAdapter(mImageAdapter);

        mVideoAdapter = new ReportProblemVideosAdapter(uriListVideo, this);
        recyclerViewVideos.setAdapter(mVideoAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                navigateToHome();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.report_problem_back:
                onBackPressed();
                break;

            case R.id.report_problem_add_video:
                if (uriListVideo.size() >= 1) {
                    Utils.getMsgPopup("Error", "You can upload only one video for a problem , " +
                            "" +
                            "", ReportProblemScreen.this);
                } else {
                    selectVideo();
                }
                break;

            case R.id.report_problem_add_image:
                if (uriListImage.size() > 4) {
                    Utils.getMsgPopup("Error", "You have exceeded image limit, Please Delete  " +
                            "image to add more. ", ReportProblemScreen.this);
                } else {
                    getCameraGallery();
                }
                break;
            case R.id.report_problem_submit:
                report_problem_submit.setEnabled(false);
                submitEnquiry();
                break;
        }
    }

    private void submitEnquiry() {

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        reportProblemPresenter.validateProblem(uriListImage, report_problem_desc.getText()
                .toString(), uriListVideo, selectedCrop);

    }

    private void getCameraGallery() {
        selectImage();
    }


    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(ReportProblemScreen.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean cameraResult = Utils.checkCameraPermission(ReportProblemScreen.this);
                boolean storegeResult = Utils.checkStoragePermission(ReportProblemScreen.this);
                if (items[item].equals("Take Photo")) {

                    userChoosenTask = "Take Photo";
                    if (cameraResult && storegeResult)
                        cameraIntent();
                } else if (items[item].equals("Choose from Library")) {

                    userChoosenTask = "Choose from Library";
                    if (storegeResult)
                        galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void selectVideo() {
        final CharSequence[] items = {"Take Video",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(ReportProblemScreen.this);
        builder.setTitle("Add Video!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean cameraResult = Utils.checkCameraPermission(ReportProblemScreen.this);
//                boolean storegeResult = Utils.checkStoragePermission(ReportProblemScreen.this);
                if (items[item].equals("Take Video")) {

                    userChoosenTaskVideo = "Take Video";
                    if (cameraResult)
                        dispatchTakeVideoIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent() {
        boolean storegeResult = Utils.checkStoragePermission(ReportProblemScreen.this);
        if(storegeResult) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            imageUri = getImageUri(this);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(intent, REQUEST_CAMERA);
        }
    }

    static final int REQUEST_VIDEO_CAPTURE = 1;

    private void dispatchTakeVideoIntent() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
            takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 30);
            takeVideoIntent.putExtra(android.provider.MediaStore.EXTRA_VIDEO_QUALITY, 0);
            startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
        }
    }


    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utils.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                    else if (userChoosenTaskVideo.equals("Take Video"))
                        dispatchTakeVideoIntent();

                } else {
                    //code for deny
                }
                break;
            case Utils.MY_PERMISSIONS_REQUEST_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                    else if (userChoosenTaskVideo.equals("Take Video"))
                        dispatchTakeVideoIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
            else if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_OK) {
                Uri videoUri = data.getData();
                uriListVideo.add(videoUri);

                mVideoAdapter = new ReportProblemVideosAdapter(uriListVideo, this);
                recyclerViewVideos.setAdapter(mVideoAdapter);
            } else if (requestCode == LOCATION_SETTINGS) {
                checkLocationSettings();
            }
        } else if (resultCode == RESULT_CANCELED) {
            if (requestCode == LOCATION_SETTINGS) {
                checkLocationSettings();
            }
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            try {
                Log.d(TAG, "onSelectFromGalleryResult: " + data.getData());
//                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                mImageAdapter.addList(data.getData());
                Log.d(TAG, "onCaptureImageResult: " + uriListImage.size());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void onCaptureImageResult(Intent data) {
        Log.d(TAG, "onCaptureImageResult: ");
        try {
            mImageAdapter.addList(imageUri);
            Log.d(TAG, "onCaptureImageResult: " + uriListImage.size());
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    @Override
    public void onClick(int adapterPosition, Boolean image, Object data) {

        Log.d(TAG, data + "gg");

        Uri uri = (Uri) data;
        getEnlargeImage(uri, image);
    }

    private void getEnlargeImage(Uri uri, Boolean image) {

        FragmentManager fm = getSupportFragmentManager();
        // FragmentTransaction ft = fm.beginTransaction();
        imageDialogFramgent = ImageDialogFramgent.newInstance(uri, image);
        // Show DialogFragment
        imageDialogFramgent.setCancelable(true);
        imageDialogFramgent.show(fm, "");

    }


    @Override
    public void onDelete(Uri uri, Boolean isImage) {

        if(isImage) {
            mImageAdapter.deleteList(uri);
        } else {
            mVideoAdapter.deleteList(uri);
        }
        imageDialogFramgent.dismiss();
        imageDialogFramgent = null;

    }


    @Override
    public void showProgress() {
        Log.d(TAG, "showProgress: ");
        Utils.hideKeyboard(ReportProblemScreen.this);
        progress.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideProgress() {
        progress.setVisibility(View.GONE);
    }

    @Override
    public void setError(String msg) {
        Utils.getMsgPopup("Error", msg, ReportProblemScreen
                .this);
        report_problem_submit.setEnabled(true);
    }

    @Override
    public void navigateToHome() {
        Intent intent = new Intent(ReportProblemScreen.this, DashboardScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onSuccess(Object data) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        Log.d(TAG, "onSuccess: ");

        ReportProblemResponse problemResponse = (ReportProblemResponse) data;
        showSuccessDialog(((ReportProblemResponse) data).getResponseData().getProblemId());
    }

    @Override
    public void onFailure() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        Log.d(TAG, "onFailure: ");
        Utils.getMsgPopup("Error", "Internal Error.", ReportProblemScreen
                .this);
        report_problem_submit.setEnabled(true);
    }

    @Override
    public void onDescriptionError() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        Log.d(TAG, "onDescriptionError: ");
        Utils.getMsgPopup("Error", "Please add description for the problem.", ReportProblemScreen
                .this);
        report_problem_desc.requestFocus();
        report_problem_submit.setEnabled(true);
    }

    @Override
    public void onImageEmpty() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        Log.d(TAG, "onImageEmpty: ");
        Utils.getMsgPopup("Error", "Please add at least one picture.", ReportProblemScreen
                .this);
        report_problem_submit.setEnabled(true);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        reportProblemPresenter.onDestroy();
    }


    public void showSuccessDialog(String problemId) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.popup_report_problem, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView report_problem_popup_problem_id = (TextView) dialogView.findViewById(R.id
                .report_problem_popup_problem_id);
        LinearLayout report_problem_ok_layout = (LinearLayout) dialogView.findViewById(
                R.id.report_problem_ok_layout);


        report_problem_popup_problem_id.setText("Problem id: " + problemId);

        report_problem_ok_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToHome();
            }
        });


        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

        if (location != null)
            handleNewLocation(location);
        else requestLocationUpdates();
    }

    private void handleNewLocation(Location location) {
        AgriProApplication.getPref().put(Constants.LATLNG, location.getLatitude() + ","
                + location.getLongitude());

        stopLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);
    }
}
