package com.kfs.expertsystems.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kfs.expertsystems.R;
import com.kfs.expertsystems.adapters.CategoryAdapter;
import com.kfs.expertsystems.adapters.ProductsGridAdapter;
import com.kfs.expertsystems.callbacks.CategoriesOnClickListener;
import com.kfs.expertsystems.callbacks.OnClickItemListener;
import com.kfs.expertsystems.models.CategoryResponse;
import com.kfs.expertsystems.models.ProductsResponse;
import com.kfs.expertsystems.presenter.presenter.ProductsPresenter;
import com.kfs.expertsystems.presenter.presenterImpl.ProductsPresenterImpl;
import com.kfs.expertsystems.presenter.views.ProductsView;
import com.kfs.expertsystems.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class ProductsScreen extends AppCompatActivity implements ProductsView, View.OnClickListener, OnClickItemListener, CategoriesOnClickListener {

    private static final String TAG = NewsArticlesListScreen.class.getName();
    private GridView gridView;
    private RecyclerView rvCategory;
    private ProductsGridAdapter mAdapter;
    private CategoryAdapter mCategoryAdapter;
    private ProductsPresenter productsPresenter;
    private RelativeLayout empty_screen_layout;
    private TextView emptyText;
    private TextView tryAgain;
    private TextView selectedCategory;
    private ProgressBar progressBar;
    private ProductsResponse products;
    private CategoryResponse categories;
    private CardView cardViewFilter;
    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initUI();
    }

    private void initUI() {

        empty_screen_layout = (RelativeLayout) findViewById(R.id.empty_layout);
        emptyText = (TextView) findViewById(R.id.cant_load_text);
        tryAgain = (TextView) findViewById(R.id.try_again);
        productsPresenter = new ProductsPresenterImpl(this);

        progressBar = (ProgressBar) findViewById(R.id.progress);
        cardViewFilter = (CardView) findViewById(R.id.card_filter_category);
        cardViewFilter.setOnClickListener(this);

        empty_screen_layout = (RelativeLayout) findViewById(R.id.empty_layout);
        emptyText = (TextView) findViewById(R.id.cant_load_text);
        tryAgain = (TextView) findViewById(R.id.try_again);
        tryAgain.setOnClickListener(this);

        gridView = (GridView) findViewById(R.id.grid_products);
        selectedCategory = (TextView) findViewById(R.id.text_selected_category);

        productsPresenter.getProducts();
        productsPresenter.getCategories();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setError(String msg) {
        Log.d(TAG, "onError: " + msg);
        Utils.getMsgPopup("Error", msg, ProductsScreen.this);
        empty_screen_layout.setVisibility(View.VISIBLE);
        emptyText.setText(R.string.cant_load);
    }

    @Override
    public void navigateToHome() {

    }

    @Override
    public void onDataFound(ProductsResponse products) {
        Log.d(TAG, "onDataFound: ");

        empty_screen_layout.setVisibility(View.GONE);
        this.products = products;

        if (!products.getResponseData().getProducts().isEmpty()) {
            mAdapter = new ProductsGridAdapter(this, products.getResponseData().getProducts(), products.getResponseData().getImageUrl());
            gridView.setAdapter(mAdapter);

        } else {
            onDataNotFound("No News available.");
        }
    }

    @Override
    public void onCategoriesFound(CategoryResponse categories) {
        this.categories = categories;

        if (!categories.getResponseData().isEmpty()) {

        } else {
            onDataNotFound("No News available.");
        }
    }

    @Override
    public void onError(String msg) {

    }

    @Override
    public void onDataNotFound(String msg) {
        Log.d(TAG, "onDataNotFound: " + msg);
        Utils.getMsgPopup("Error", msg, ProductsScreen.this);
        empty_screen_layout.setVisibility(View.VISIBLE);
        emptyText.setText(R.string.cant_load);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.try_again:
                productsPresenter.getProducts();
                break;

            case R.id.card_filter_category:
                setUpPopUp();
                break;
        }
    }

    private void search(SearchView searchView) {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.filterList(filter(newText.toString(), products.getResponseData
                        ().getProducts()));
                return true;
            }

        });
    }

    private List<ProductsResponse.ResponseDataBean.ProductsBean> filter(String text, List<ProductsResponse.ResponseDataBean.ProductsBean> response) {
        //new array list that will hold the filtered data
        if (text.equalsIgnoreCase("")) {
            mAdapter.filterList(response);
            return response;
        }

        List<ProductsResponse.ResponseDataBean.ProductsBean> filterlist = new ArrayList<>();
        //looping through existing elements
        for (ProductsResponse.ResponseDataBean.ProductsBean s : response) {
            //if the existing elements contains the search input
            if (s.getProductName().toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterlist.add(s);

            }
        }

//        response.getResponseData().setCrops(filterlist);

        //calling a method of the adapter class and passing the filtered list

        return filterlist;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search, menu);
        MenuItem search = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        search(searchView);
        return true;
    }

    @Override
    public void onClick(int adapterPosition, Object data) {
        ProductsResponse.ResponseDataBean.ProductsBean selectedProduct = (ProductsResponse.ResponseDataBean.ProductsBean) data;

        Intent intent = new Intent(ProductsScreen.this, ProductsDetailScreen.class);
        intent.putExtra("IMAGE_BASE_URL", products.getResponseData().getImageUrl());
        intent.putExtra("SELECTED_PRODUCT", selectedProduct);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //MARK:- popup dialog for category
    public void setUpPopUp() {

        dialog = new Dialog(ProductsScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //setting custom layout to dialog
        dialog.setContentView(R.layout.pop_up_window);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.black);
        dialog.setCancelable(true);
        Button btnDismissPopUp = (Button) dialog.findViewById(R.id.btn_dismiss_popup);
        rvCategory = (RecyclerView) dialog.findViewById(R.id.rv_category);

        rvCategory.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvCategory.setHasFixedSize(true);
//        mDataList.clear();
        initView();

        dialog.show();

        btnDismissPopUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

    private void initView() {
        mCategoryAdapter = new CategoryAdapter(this, categories.getResponseData());
        rvCategory.setAdapter(mCategoryAdapter);
    }

    @Override
    public void onClickCategories(int adapterPosition, Object data) {
        dialog.dismiss();
        selectedCategory.setText(data.toString());
        mAdapter.filterList(filterCategories(data.toString(), products.getResponseData
                ().getProducts()));
    }

    private List<ProductsResponse.ResponseDataBean.ProductsBean> filterCategories(String text, List<ProductsResponse.ResponseDataBean.ProductsBean> response) {
        //new array list that will hold the filtered data
        if (text.equalsIgnoreCase("")) {
            mAdapter.filterList(response);
            return response;
        }

        List<ProductsResponse.ResponseDataBean.ProductsBean> filterlist = new ArrayList<>();
        //looping through existing elements
        for (ProductsResponse.ResponseDataBean.ProductsBean s : response) {
            //if the existing elements contains the search input
            if (s.getProductCategory().getProductCategoryName().equals(text)) {
                //adding the element to filtered list
                filterlist.add(s);

            }
        }
        //calling a method of the adapter class and passing the filtered list

        return filterlist;

    }
}
