package com.kfs.expertsystems.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kfs.expertsystems.R;
import com.kfs.expertsystems.presenter.presenter.RegisterPresenter;
import com.kfs.expertsystems.presenter.presenterImpl.RegisterPresenterImpl;
import com.kfs.expertsystems.presenter.views.RegisterView;
import com.kfs.expertsystems.utils.Utils;

public class RegisterScreen extends AppCompatActivity implements RegisterView, View.OnClickListener{

    private final String TAG = RegisterScreen.class.getName();

    private RegisterPresenter registerPresenter;

    private EditText edittext_name;

    private EditText edittext_email;

    private EditText edittext_phone;

    private EditText edittext_address;

    private EditText edittext_password;

    private EditText edittext_confirm_password;

    private TextView textview_name;

    private TextView textview_email;

    private TextView textview_phone;

    private TextView textview_address;

    private TextView textview_password;

    private TextView textview_confirm_password;

    private ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initUI();

    }

    private void initUI() {

        edittext_name = (EditText) findViewById(R.id.edittext_name);
        textview_name = (TextView) findViewById(R.id.textview_name);

        edittext_email = (EditText) findViewById(R.id.edittext_email);
        textview_email = (TextView) findViewById(R.id.textview_email);

        edittext_phone = (EditText) findViewById(R.id.edittext_phone);
        textview_phone = (TextView) findViewById(R.id.textview_phone);

        edittext_address = (EditText) findViewById(R.id.edittext_address);
        textview_address = (TextView) findViewById(R.id.textview_address);

        edittext_password = (EditText) findViewById(R.id.edittext_password);
        textview_password = (TextView) findViewById(R.id.textview_password);

        edittext_confirm_password = (EditText) findViewById(R.id.edittext_confirm_password);
        textview_confirm_password = (TextView) findViewById(R.id.textview_confirm_password);

        findViewById(R.id.register_click).setOnClickListener(this);

        progressBar = (ProgressBar) findViewById(R.id.progress);


        registerPresenter = new RegisterPresenterImpl(this);

    }



    @Override
    public void onClick(View view) {

        if(view.getId() == R.id.register_click){

            registrationClick();
        }
    }

    private void registrationClick() {

        Utils.hideKeyboard(RegisterScreen.this);
        registerPresenter.registerUser(edittext_name.getText().toString(),edittext_email.getText
                ().toString(),edittext_phone.getText().toString(),edittext_address.getText()
                .toString(),edittext_password.getText().toString(),edittext_confirm_password
                .getText().toString());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home :
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void showProgress() {
        Log.d(TAG, "showProgress: ");
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        Log.d(TAG, "hideProgress: ");
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setError(String msg) {
        Log.d(TAG, "setError: ");
        Utils.getMsgPopup("Error",msg,RegisterScreen.this);
    }

    @Override
    public void navigateToHome() {
        Log.d(TAG, "navigateToHome: ");
        startActivity(new Intent(RegisterScreen.this,DashboardScreen.class));finish();
    }

    @Override
    public void onEmailError(String msg) {
        Utils.getMsgPopup("Error",msg,RegisterScreen.this);
        edittext_email.requestFocus();
        Utils.openKeyboard(RegisterScreen.this,edittext_email);
    }

    @Override
    public void onNameError(String msg) {
        Utils.getMsgPopup("Error",msg,RegisterScreen.this);
        edittext_name.requestFocus();
        Utils.openKeyboard(RegisterScreen.this,edittext_name);
    }

    @Override
    public void onPhoneNumberError(String msg) {
        Utils.getMsgPopup("Error",msg,RegisterScreen.this);
        edittext_phone.requestFocus();
        Utils.openKeyboard(RegisterScreen.this,edittext_phone);
    }

    @Override
    public void onAddressError(String msg) {
        Utils.getMsgPopup("Error",msg,RegisterScreen.this);
        edittext_address.requestFocus();
        Utils.openKeyboard(RegisterScreen.this,edittext_address);
    }

    @Override
    public void onPasswordError(String msg) {
        Utils.getMsgPopup("Error",msg,RegisterScreen.this);
        edittext_password.requestFocus();
        Utils.openKeyboard(RegisterScreen.this,edittext_password);
    }

    @Override
    public void onConfirmPasswordError(String msg) {
        Utils.getMsgPopup("Error",msg,RegisterScreen.this);
        edittext_confirm_password.requestFocus();
        Utils.openKeyboard(RegisterScreen.this,edittext_confirm_password);
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy: ");
        registerPresenter.onDestroy();
        super.onDestroy();
    }


    @Override
    public void onBackPressed() {
        startActivity(new Intent(RegisterScreen.this,LoginScreen.class));finish();
    }
}
