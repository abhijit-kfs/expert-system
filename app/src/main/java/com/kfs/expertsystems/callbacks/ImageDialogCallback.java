package com.kfs.expertsystems.callbacks;

import android.net.Uri;

/**
 * Created by Tomesh on 26-09-2017.
 */

public interface ImageDialogCallback {

    void onDelete(Uri uri, Boolean isImage);
}
