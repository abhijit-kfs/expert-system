package com.kfs.expertsystems.callbacks;

public interface OnClickItemListener {

    void onClick(int adapterPosition, Object data);
}
