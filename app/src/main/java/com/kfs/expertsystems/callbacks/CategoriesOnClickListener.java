package com.kfs.expertsystems.callbacks;

public interface CategoriesOnClickListener {

    void onClickCategories(int adapterPosition, Object data);
}
