package com.kfs.expertsystems.callbacks;

import java.util.List;
import com.kfs.expertsystems.models.CategoryResponse.ResponseDataBean;
import com.kfs.expertsystems.models.CategoryResponse.ResponseDataBean.GalleryCategoryBean;

public interface Interactor {
    interface Product {
        List<GalleryCategoryBean> getGalleryList(int categoryId);
    }

    interface ProductType {

        List<ResponseDataBean> getProductTypeList();

        void onCategorySelected(int id);
    }
}
