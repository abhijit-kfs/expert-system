package com.kfs.expertsystems.callbacks;

/**
 * Created by Tomesh on 03-11-2017.
 */

public interface AWSListener {

    public void onComplete(String key, String url);
    public void onStart(String key);
    public void onError(String key, Exception ex);
    public void publicProgress(String key, long bytesCurrent, long bytesTotal);
}
