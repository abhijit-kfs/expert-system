package com.kfs.expertsystems.callbacks;

import android.net.Uri;

/**
 * Created by Tomesh on 28-09-2017.
 */

public interface ImageReportCallback {

    void startImageEnlarge(Uri uri, Boolean isImage);
}
