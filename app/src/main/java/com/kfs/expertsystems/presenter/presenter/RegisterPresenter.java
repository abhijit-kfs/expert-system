package com.kfs.expertsystems.presenter.presenter;

/**
 * Created by Tomesh on 20-09-2017.
 */

public interface RegisterPresenter {

    void registerUser(String name, String email, String phone, String address, String password,
                      String confirmPassword);

    void onDestroy();
}
