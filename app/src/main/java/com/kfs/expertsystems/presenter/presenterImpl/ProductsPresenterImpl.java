package com.kfs.expertsystems.presenter.presenterImpl;

import com.kfs.expertsystems.models.CategoryResponse;
import com.kfs.expertsystems.models.ProductsResponse;
import com.kfs.expertsystems.presenter.interactor.ProductsInteractor;
import com.kfs.expertsystems.presenter.interactorImpl.ProductsInteractorImpl;
import com.kfs.expertsystems.presenter.presenter.ProductsPresenter;
import com.kfs.expertsystems.presenter.views.ProductsView;

/**
 * Created by swaroop on 17/04/18.
 */

public class ProductsPresenterImpl implements ProductsPresenter, ProductsInteractor.onProductsFinish, ProductsInteractor.onCategoryFinish {


    private ProductsView productsView;
    private ProductsInteractor productsInteractor;

    public ProductsPresenterImpl(ProductsView productsView) {
        this.productsView = productsView;
        this.productsInteractor = new ProductsInteractorImpl();
    }

    @Override
    public void getProducts() {
        if (productsView != null) {
            productsView.showProgress();
        }
        productsInteractor.getProducts(this);
    }

    @Override
    public void getCategories() {
        if (productsView != null) {
            productsView.showProgress();
        }
        productsInteractor.getCategory( this);
    }

    @Override
    public void onDestroy() {
        productsView = null;
    }

    @Override
    public void onDataFound(ProductsResponse products) {
        if (productsView != null) {
            productsView.hideProgress();
            productsView.onDataFound(products);
        }
    }

    @Override
    public void onCategoryDataFound(CategoryResponse category) {
        if (productsView != null) {
            productsView.hideProgress();
            productsView.onCategoriesFound(category);
        }
    }

    @Override
    public void onError(String msg) {
        if (productsView != null) {
            productsView.hideProgress();
            productsView.setError(msg);
        }
    }

    @Override
    public void onCategoryDataNotFound(String msg) {
        if (productsView != null) {
            productsView.hideProgress();
            productsView.onDataNotFound(msg);
        }
    }

    @Override
    public void onDataNotFound(String msg) {
        if (productsView != null) {
            productsView.hideProgress();
            productsView.onDataNotFound(msg);
        }
    }
}
