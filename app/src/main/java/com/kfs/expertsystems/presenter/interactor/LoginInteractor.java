package com.kfs.expertsystems.presenter.interactor;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;

import org.json.JSONObject;

/**
 * Created by Tomesh on 20-09-2017.
 */

public interface LoginInteractor {


    void changeLanguage(int type, OnLanguageChangeListener listener);

    void fbGraphCallback(
            AccessToken accessToken,
            GraphRequest.GraphJSONObjectCallback callback);

    void fbRegisterCallback(CallbackManager manager, fbLoginCallback listener);

    void login(String username, String password, OnLoginFinishedListener listener);

    void register(String name, String email, String phone, String address, String password,
                  String confirmPassword, int facebookLogin, OnRegisterFinishedListener listener);

    interface OnLoginFinishedListener {

        void onError(String msg);

        void onLoginSuccess();
    }

    interface OnRegisterFinishedListener {

        void onAddressError(String msg);

        void onConfirmPasswordError(String msg);

        void onEmailError(String msg);

        void onError(String msg);

        void onNameError(String msg);

        void onPasswordError(String msg);

        void onPhoneNumberError(String msg);

        void onRegisterSuccess();
    }

    interface OnLanguageChangeListener {

        void languageBurma();

        void languageEnglish();
    }

    interface fbLoginCallback extends FacebookCallback<LoginResult> {
        @Override
        void onCancel();

        @Override
        void onError(FacebookException error);

        @Override
        void onSuccess(LoginResult loginResult);
    }

    interface fbGraphCallback extends GraphRequest.GraphJSONObjectCallback {
        @Override
        void onCompleted(JSONObject object, GraphResponse response);
    }
}
