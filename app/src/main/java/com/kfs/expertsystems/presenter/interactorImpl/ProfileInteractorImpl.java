package com.kfs.expertsystems.presenter.interactorImpl;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;

import com.kfs.expertsystems.R;
import com.kfs.expertsystems.application.AgriProApplication;
import com.kfs.expertsystems.models.AddPasswordRequest;
import com.kfs.expertsystems.models.AddPasswordResponse;
import com.kfs.expertsystems.models.BaseResponse;
import com.kfs.expertsystems.models.BasicResponse;
import com.kfs.expertsystems.models.ChangePasswordRequest;
import com.kfs.expertsystems.models.ProfileModel;
import com.kfs.expertsystems.models.ProfileUpdateRequest;
import com.kfs.expertsystems.models.ResolutionRequest;
import com.kfs.expertsystems.presenter.interactor.ProfileInteractor;
import com.kfs.expertsystems.utils.Constants;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Tomesh on 05-10-2017.
 */

public class ProfileInteractorImpl implements ProfileInteractor {

    private final String TAG = ProfileInteractorImpl.class.getName();

    @Override
    public void addPassword(final AddPasswordRequest addPasswordRequest, final onProfileFinish listener) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


                if (addPasswordRequest.getPassword().isEmpty()) {
                    listener.onAddPasswordError("Please Provide password.");
                    return;
                }

                Call<AddPasswordResponse> call = AgriProApplication.getApiClient().addPassword
                        (addPasswordRequest);

                call.enqueue(new Callback<AddPasswordResponse>() {
                    @Override
                    public void onFailure(Call<AddPasswordResponse> call, Throwable t) {
                        listener.onPasswordChangeError(AgriProApplication.getAppContext()
                                .getResources().getString(R.string.unable_to_connect));
                    }

                    @Override
                    public void onResponse(Call<AddPasswordResponse> call, Response<AddPasswordResponse> response) {
                        Log.d(TAG, "onResponse: ");

                        try {

                            if (response.body() != null) {
                                if (response.body().getCode() == 200) {
                                    listener.onAddPasswordSuccess("");
                                } else {
                                    listener.onAddPasswordError(AgriProApplication.getAppContext()
                                            .getResources().getString(R.string.unable_to_add_password));
                                }
                            } else {
                                listener.onAddPasswordError(AgriProApplication.getAppContext()
                                        .getResources().getString(R.string.unable_to_connect_server));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onAddPasswordError(AgriProApplication.getAppContext()
                                    .getResources().getString(R.string.internal_error));
                        }
                    }
                });


            }
        }, 500);
    }

    @Override
    public void changePassword(final ChangePasswordRequest changePasswordRequest, final onProfileFinish listener) {


        Log.d(TAG, "changePassword: ");


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


                if (changePasswordRequest.getOldPasswd().isEmpty()) {
                    listener.onOldPasswordError("Please Provide password.");
                    return;
                }
                if (changePasswordRequest.getNewPasswd().isEmpty()) {
                    listener.onNewPasswordError("Please Provide new password.");
                    return;
                }
                if (changePasswordRequest.getConfirmNewPassword().isEmpty()) {
                    listener.onConfirmPasswordError("Please Provide confirm new password.");
                    return;
                }

                if (!changePasswordRequest.getConfirmNewPassword().equals(changePasswordRequest
                        .getNewPasswd())) {
                    listener.onConfirmPasswordError("Please Provide identical new password.");
                    return;
                }
//                ChangePasswordRequest request = new ChangePasswordRequest();
//                request.setUid(AgriProApplication.getPref().getString(Constants.USER_ID));

                Call<BasicResponse> call = AgriProApplication.getApiClient().changePassword
                        (changePasswordRequest);

                call.enqueue(new Callback<BasicResponse>() {
                    @Override
                    public void onFailure(Call<BasicResponse> call, Throwable t) {
                        listener.onPasswordChangeError(AgriProApplication.getAppContext()
                                .getResources().getString(R.string.unable_to_connect));
                    }

                    @Override
                    public void onResponse(Call<BasicResponse> call, Response<BasicResponse> response) {
                        Log.d(TAG, "onResponse: ");

                        try {

                            if (response.body() != null) {
                                if (response.body().getCode().equalsIgnoreCase("200")) {
                                    listener.onPasswordChangeSuccess(response.body().getResponseData());
                                } else if (response.body().getCode().equalsIgnoreCase("202")) {
                                    listener.onNewPasswordError(response.body().getResponseData());
                                } else if (response.body().getCode().equalsIgnoreCase("201")) {
                                    listener.onOldPasswordError(response.body().getResponseData());
                                } else {
                                    listener.onPasswordChangeError(response.body().getResponseData());
                                }
                            } else {
                                listener.onPasswordChangeError(AgriProApplication.getAppContext()
                                        .getResources().getString(R.string.unable_to_connect_server));
                            }
                        } catch (
                                Exception e)

                        {
                            e.printStackTrace();
                            listener.onPasswordChangeError(AgriProApplication.getAppContext()
                                    .getResources().getString(R.string.internal_error));
                        }
                    }
                });


            }
        }, 500);


    }

    private String getBitmapFromUri(Uri uri) {


        Bitmap thumbnail = null;
        String base64 = "";
        try {
            thumbnail = MediaStore.Images.Media.getBitmap(AgriProApplication.getAppContext()
                            .getContentResolver()
                    , Uri.parse
                            (uri.toString()));
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 70, bytes);
            base64 = Base64.encodeToString(bytes.toByteArray(), Base64.DEFAULT);
            return base64;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void getProfile(final onProfileFinish listener) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


                ResolutionRequest request = new ResolutionRequest();
                request.setUid(AgriProApplication.getPref().getString(Constants.USER_ID));
                Call<ProfileModel> call = AgriProApplication.getApiClient().getProfile(request);

                call.enqueue(new Callback<ProfileModel>() {
                    @Override
                    public void onFailure(Call<ProfileModel> call, Throwable t) {
                        listener.onDataNotFound(AgriProApplication.getAppContext()
                                .getResources().getString(R.string.unable_to_connect));
                    }

                    @Override
                    public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                        Log.d(TAG, "onResponse: ");

                        try {

                            if (response.body() != null) {
                                if (response.body().getCode() == 200) {
                                    listener.onDataFound(response.body());
                                } else {
                                    listener.onDataNotFound(response.body().getMessage());
                                }
                            } else {
                                listener.onDataNotFound(AgriProApplication.getAppContext()
                                        .getResources().getString(R.string.unable_to_connect_server));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onDataNotFound(AgriProApplication.getAppContext()
                                    .getResources().getString(R.string.internal_error));
                        }
                    }
                });


            }
        }, 500);

    }

    @Override
    public void saveProfile(final ProfileUpdateRequest profile, final onProfileFinish listener) {
        Log.d(TAG, "saveProfile: ");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


                if (profile.getName().isEmpty()) {
                    listener.onNameError("Please provide name.");
                    return;
                }
                if (profile.getAddress().isEmpty()) {
                    listener.onAddressError("Please provide Address.");
                    return;
                }
                if (profile.getPhoneNumber() == 0) {
                    listener.onPhoneNumberError("Please provide phone number.");
                    return;
                }

                if (profile.getProfileImage() != null) {
                    profile.setProfileImage(getBitmapFromUri(Uri.parse(profile.getProfileImage())));
                }


                Call<BaseResponse> call = AgriProApplication.getApiClient().updateProfile(profile);

                call.enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {

                        listener.onProfileUpdateError(AgriProApplication.getAppContext()
                                .getResources().getString(R.string.unable_to_connect));
                    }

                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        Log.d(TAG, "onResponse: ");

                        try {

                            if (response.body() != null) {
                                if (response.body().getCode().equalsIgnoreCase("200")) {
                                    listener.onProfileUpdate(response.body().getMessage());
                                } else {
                                    listener.onProfileUpdateError(response.body().getMessage());
                                }
                            } else {
                                listener.onProfileUpdateError(AgriProApplication.getAppContext()
                                        .getResources().getString(R.string.internal_error));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onProfileUpdateError(AgriProApplication.getAppContext()
                                    .getResources().getString(R.string.internal_error));
                        }
                    }
                });


            }
        }, 500);


    }

}
