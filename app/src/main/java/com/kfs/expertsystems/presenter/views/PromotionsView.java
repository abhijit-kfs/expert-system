package com.kfs.expertsystems.presenter.views;

import com.kfs.expertsystems.models.PromotionsResponse;

/**
 * Created by Tomesh on 21-09-2017.
 */

public interface PromotionsView extends BaseView{

    void onDataFound(PromotionsResponse promotions);

    void onDataNotFound(String msg);
}

