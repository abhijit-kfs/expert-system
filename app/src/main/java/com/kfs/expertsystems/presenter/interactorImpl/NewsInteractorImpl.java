package com.kfs.expertsystems.presenter.interactorImpl;

import android.os.Handler;
import android.util.Log;

import com.kfs.expertsystems.R;
import com.kfs.expertsystems.application.AgriProApplication;
import com.kfs.expertsystems.models.NewsResponse;
import com.kfs.expertsystems.models.ResolutionRequest;
import com.kfs.expertsystems.presenter.interactor.NewsInteractor;
import com.kfs.expertsystems.utils.Constants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * Created by Tomesh on 11-10-2017.
 */

public class NewsInteractorImpl  implements NewsInteractor {




    @Override
    public void getNews(final onNewsFinish listener) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


                ResolutionRequest request = new ResolutionRequest();
                request.setUid(AgriProApplication.getPref().getString(Constants.USER_ID));
                Call<NewsResponse> call = AgriProApplication.getApiClient().getNews();

                call.enqueue(new Callback<NewsResponse>() {
                    @Override
                    public void onResponse(Call<NewsResponse> call, Response<NewsResponse> response) {
                        Log.d(TAG, "onResponse: ");

                        try {

                            if (response.body() != null) {
                                if (response.body().getCode() == 200) {
                                    listener.onDataFound(response.body());
                                } else {
                                    listener.onDataNotFound(response.body().getMessage());
                                }
                            } else {
                                listener.onDataNotFound(AgriProApplication.getAppContext()
                                        .getResources().getString(R.string.unable_to_connect_server));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onDataNotFound(AgriProApplication.getAppContext()
                                    .getResources().getString(R.string.internal_error));
                        }
                    }

                    @Override
                    public void onFailure(Call<NewsResponse> call, Throwable t) {
                        Log.d(TAG, "onFailure: " + t.getMessage());
                        t.printStackTrace();
                        listener.onDataNotFound(AgriProApplication.getAppContext()
                                .getResources().getString(R.string.unable_to_connect));
                    }
                });


            }
        }, 500);



    }
}
