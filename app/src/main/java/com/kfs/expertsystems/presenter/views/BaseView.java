package com.kfs.expertsystems.presenter.views;

/**
 * Created by Tomesh on 20-09-2017.
 */

public interface BaseView {

    void showProgress();

    void hideProgress();

    void setError(String msg);

    void navigateToHome();
}
