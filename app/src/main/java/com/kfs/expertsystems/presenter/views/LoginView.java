package com.kfs.expertsystems.presenter.views;

/**
 * Created by Tomesh on 20-09-2017.
 */

public interface LoginView extends BaseView {

    void languageEnglish();

    void languageBurma();
}
