package com.kfs.expertsystems.presenter.presenter;

/**
 * Created by Tomesh on 09-10-2017.
 */

public interface NewsPresenter {


    void getNews();

    void onDestroy();

}
