package com.kfs.expertsystems.presenter.interactor;

import android.net.Uri;

import com.kfs.expertsystems.models.CropResponse;

import java.util.List;

/**
 * Created by Tomesh on 27-09-2017.
 */

public interface ReportProblemInteractor {


    void validateProblem(List<Uri> imageUriList, String problemDesc , List<Uri> videoUriList,
                         CropResponse.ResponseDataBean.CropsBean selectedCrop ,
                         OnProblemFinishedListener listener);




    interface OnProblemFinishedListener {

       void onDescriptionError();

        void onImageEmpty();

        void onError(String msg);

        void onSuccess(Object data);



    }



}
