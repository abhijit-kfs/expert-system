package com.kfs.expertsystems.presenter.views;

import com.kfs.expertsystems.models.ResolutionResponse;

/**
 * Created by Tomesh on 27-09-2017.
 */

public interface ResolutionView extends BaseView {


    void onEnquiryFetchSuccess(ResolutionResponse resolutionResponse);
    void onEnquiryFetchError(String msg);
    void onEnquiryDataNotFound(String msg);

    void onProblemResolvedSucess();

    void onProblemResolvedError(String msg);

    void onAddCommentSuccess(Object data);
    void onAddCommentError(String msg);
}
