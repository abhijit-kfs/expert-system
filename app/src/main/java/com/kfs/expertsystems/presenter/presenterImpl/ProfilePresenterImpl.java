package com.kfs.expertsystems.presenter.presenterImpl;

import com.kfs.expertsystems.models.AddPasswordRequest;
import com.kfs.expertsystems.models.ChangePasswordRequest;
import com.kfs.expertsystems.models.ProfileModel;
import com.kfs.expertsystems.models.ProfileUpdateRequest;
import com.kfs.expertsystems.presenter.interactor.ProfileInteractor;
import com.kfs.expertsystems.presenter.interactorImpl.ProfileInteractorImpl;
import com.kfs.expertsystems.presenter.presenter.ProfilePresenter;
import com.kfs.expertsystems.presenter.views.ProfileView;

/**
 * Created by Tomesh on 05-10-2017.
 */

public class ProfilePresenterImpl implements ProfilePresenter, ProfileInteractor.onProfileFinish {

    private ProfileView profileView;
    private ProfileInteractor profileInteractor;


    public ProfilePresenterImpl(ProfileView profileView) {
        this.profileView = profileView;
        this.profileInteractor = new ProfileInteractorImpl();
    }


    @Override
    public void getProfile() {
        if (profileView != null) {
            profileView.showProgress();

        }
        profileInteractor.getProfile(this);
    }

    @Override
    public void updateProfile(ProfileUpdateRequest profile) {
        if (profileView != null) {
            profileView.showProgress();

        }
        profileInteractor.saveProfile(profile, this);
    }

    @Override
    public void changePassword(ChangePasswordRequest changePasswordRequest) {
        if (profileView != null) {
            profileView.showProgress();

        }

        profileInteractor.changePassword(changePasswordRequest,this);
    }

    @Override
    public void addPassword(AddPasswordRequest addPasswordRequest) {
        if (profileView != null) {
            profileView.showProgress();

        }

        profileInteractor.addPassword(addPasswordRequest,this);
    }

    @Override
    public void onDestroy() {
        profileView = null;
    }

    @Override
    public void onDataFound(ProfileModel profile) {

        if (profileView != null) {
            profileView.hideProgress();
            profileView.onProfileDataFound(profile);
        }
    }

    @Override
    public void onError(String msg) {
        if (profileView != null) {
            profileView.hideProgress();
            profileView.setError(msg);
        }
    }

    @Override
    public void onDataNotFound(String msg) {
        if (profileView != null) {
            profileView.hideProgress();
            profileView.setError(msg);
        }
    }

    @Override
    public void onProfileUpdate(String msg) {
        if (profileView != null) {
            profileView.hideProgress();
            profileView.onProfileUpdate(msg);
        }
    }

    @Override
    public void onNameError(String msg) {
        if (profileView != null) {
            profileView.hideProgress();
            profileView.onNameError(msg);
        }
    }

    @Override
    public void onPhoneNumberError(String msg) {
        if (profileView != null) {
            profileView.hideProgress();
            profileView.onPhoneNumberError(msg);
        }
    }

    @Override
    public void onAddressError(String msg) {
        if (profileView != null) {
            profileView.hideProgress();
            profileView.onAddressError(msg);
        }
    }

    @Override
    public void onProfileUpdated() {
        if (profileView != null) {
            profileView.hideProgress();
            profileView.onProfileUpdate("Profile Updated successfully.");
        }
    }

    @Override
    public void onProfileUpdateError(String msg) {
        if (profileView != null) {
            profileView.hideProgress();
            profileView.onProfileUpdateError(msg);
        }
    }

    @Override
    public void onPasswordChangeSuccess(String msg) {
        if (profileView != null) {
            profileView.hideProgress();
            profileView.onPasswordChangeSuccess(msg);
        }
    }

    @Override
    public void onPasswordChangeError(String msg) {
        if (profileView != null) {
            profileView.hideProgress();
            profileView.onPasswordChangeError(msg);
        }
    }

    @Override
    public void onOldPasswordError(String msg) {
        if (profileView != null) {
            profileView.hideProgress();
            profileView.onOldPasswordError(msg);
        }

    }

    @Override
    public void onNewPasswordError(String msg) {
        if (profileView != null) {
            profileView.hideProgress();
            profileView.onNewPasswordError(msg);
        }
    }

    @Override
    public void onAddPasswordSuccess(String msg) {
        if (profileView != null) {
            profileView.hideProgress();
            profileView.navigateToHome();
        }
    }

    @Override
    public void onAddPasswordError(String msg) {
        if (profileView != null) {
            profileView.hideProgress();
            profileView.onAddPasswordError(msg);
        }
    }

    @Override
    public void onConfirmPasswordError(String msg) {
        if (profileView != null) {
            profileView.hideProgress();
            profileView.onConfirmPasswordError(msg);
        }
    }
}
