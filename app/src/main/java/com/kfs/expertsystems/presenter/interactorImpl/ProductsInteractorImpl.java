package com.kfs.expertsystems.presenter.interactorImpl;

import android.os.Handler;
import android.util.Log;

import com.kfs.expertsystems.R;
import com.kfs.expertsystems.application.AgriProApplication;
import com.kfs.expertsystems.models.CategoryResponse;
import com.kfs.expertsystems.models.ProductsResponse;
import com.kfs.expertsystems.models.ResolutionRequest;
import com.kfs.expertsystems.presenter.interactor.ProductsInteractor;
import com.kfs.expertsystems.utils.Constants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * Created by swaroop on 17/04/18.
 */

public class ProductsInteractorImpl implements ProductsInteractor {
    @Override
    public void getProducts(final onProductsFinish listener) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


                ResolutionRequest request = new ResolutionRequest();
                request.setUid(AgriProApplication.getPref().getString(Constants.USER_ID));
                Call<ProductsResponse> call = AgriProApplication.getApiClient().getProducts();

                call.enqueue(new Callback<ProductsResponse>() {
                    @Override
                    public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {
                        Log.d(TAG, "onResponse: ");

                        try {

                            if (response.body() != null) {
                                if (response.body().getCode() == 200) {
                                    listener.onDataFound(response.body());
                                } else {
                                    listener.onDataNotFound(response.body().getMessage());
                                }
                            } else {
                                listener.onDataNotFound(AgriProApplication.getAppContext()
                                        .getResources().getString(R.string.unable_to_connect_server));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onDataNotFound(AgriProApplication.getAppContext()
                                    .getResources().getString(R.string.internal_error));
                        }
                    }

                    @Override
                    public void onFailure(Call<ProductsResponse> call, Throwable t) {
                        Log.d(TAG, "onFailure: " + t.getMessage());
                        t.printStackTrace();
                        listener.onDataNotFound(AgriProApplication.getAppContext()
                                .getResources().getString(R.string.unable_to_connect));
                    }
                });


            }
        }, 500);

    }

    @Override
    public void getCategory(final onCategoryFinish listener) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


                ResolutionRequest request = new ResolutionRequest();
                request.setUid(AgriProApplication.getPref().getString(Constants.USER_ID));
                Call<CategoryResponse> call = AgriProApplication.getApiClient().getCategories();

                call.enqueue(new Callback<CategoryResponse>() {
                    @Override
                    public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                        Log.d(TAG, "onResponse: ");

                        try {

                            if (response.body() != null) {
                                if (response.body().getCode() == 200) {
                                    listener.onCategoryDataFound(response.body());
                                } else {
                                    listener.onCategoryDataNotFound(response.body().getMessage());
                                }
                            } else {
                                listener.onCategoryDataNotFound(AgriProApplication.getAppContext()
                                        .getResources().getString(R.string.unable_to_connect_server));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onCategoryDataNotFound(AgriProApplication.getAppContext()
                                    .getResources().getString(R.string.internal_error));
                        }
                    }

                    @Override
                    public void onFailure(Call<CategoryResponse> call, Throwable t) {
                        Log.d(TAG, "onFailure: " + t.getMessage());
                        t.printStackTrace();
                        listener.onCategoryDataNotFound(AgriProApplication.getAppContext()
                                .getResources().getString(R.string.unable_to_connect));
                    }
                });


            }
        }, 500);
    }
}
