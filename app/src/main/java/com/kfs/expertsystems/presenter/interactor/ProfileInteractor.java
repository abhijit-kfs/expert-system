package com.kfs.expertsystems.presenter.interactor;

import com.kfs.expertsystems.models.AddPasswordRequest;
import com.kfs.expertsystems.models.ChangePasswordRequest;
import com.kfs.expertsystems.models.ProfileModel;
import com.kfs.expertsystems.models.ProfileUpdateRequest;

/**
 * Created by Tomesh on 05-10-2017.
 */

public interface ProfileInteractor {


    interface onProfileFinish {
        void onDataFound(ProfileModel profile);

        void onError(String msg);

        void onDataNotFound(String msg);

        void onProfileUpdate(String msg);

        void onNameError(String msg);

        void onPhoneNumberError(String msg);

        void onAddressError(String msg);

        void onProfileUpdated();

        void onProfileUpdateError(String msg);

        void onPasswordChangeSuccess(String msg);

        void onPasswordChangeError(String msg);

        void onOldPasswordError(String msg);

        void onNewPasswordError(String msg);

        void onAddPasswordSuccess(String msg);

        void onAddPasswordError(String msg);

        void onConfirmPasswordError(String msg);
    }

    void saveProfile(ProfileUpdateRequest profile, onProfileFinish listener);

    void getProfile(onProfileFinish listener);


    void changePassword(ChangePasswordRequest changePasswordRequest , onProfileFinish listener);

    void addPassword(AddPasswordRequest addPasswordRequest , onProfileFinish listener);
}
