package com.kfs.expertsystems.presenter.views;

import com.kfs.expertsystems.models.CategoryResponse;
import com.kfs.expertsystems.models.ProductsResponse;

/**
 * Created by swaroop on 17/04/18.
 */

public interface ProductsView extends BaseView {

    void onDataFound(ProductsResponse products);

    void onCategoriesFound(CategoryResponse categories);

    void onError(String msg);

    void onDataNotFound(String msg);
}
