package com.kfs.expertsystems.presenter.presenter;

import android.net.Uri;

import com.kfs.expertsystems.models.ResolvedModel;

import java.util.List;

/**
 * Created by Tomesh on 27-09-2017.
 */

public interface ResolutionPresenter {

    void getEnquiryList();

    void onDestroy();

    void addComment(List<Uri> uriListImage, List<Uri> uriListVideo, String s, int problemId);

    void markAsResolved(ResolvedModel resolvedModel);


}
