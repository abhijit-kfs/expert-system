package com.kfs.expertsystems.presenter.presenterImpl;

import android.app.Activity;
import android.util.Log;

import com.facebook.CallbackManager;
import com.facebook.FacebookException;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.kfs.expertsystems.presenter.interactor.LoginInteractor;
import com.kfs.expertsystems.presenter.interactorImpl.LoginInteractorImpl;
import com.kfs.expertsystems.presenter.presenter.LoginPresenter;
import com.kfs.expertsystems.presenter.views.LoginView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * Created by Tomesh on 20-09-2017.
 */

public class LoginPresenterImpl implements LoginPresenter, LoginInteractor.OnLoginFinishedListener, LoginInteractor.OnLanguageChangeListener, LoginInteractor.fbLoginCallback, LoginInteractor.fbGraphCallback, LoginInteractor.OnRegisterFinishedListener {

    public final List<String> IMP_PERMISSIONS = new ArrayList<String>() {
        {
            add("public_profile");
            add("email");
        }
    };
    private LoginView loginView;
    private LoginInteractor loginInteractor;
    private Activity mActivity;

    public LoginPresenterImpl(Activity activity, LoginView loginView) {
        mActivity = activity;
        this.loginView = loginView;
        this.loginInteractor = new LoginInteractorImpl();
    }

    @Override
    public void changeLanguage(int type) {
        Log.d(TAG, "changeLanguage: " + type);
        loginInteractor.changeLanguage(type, this);
    }

    @Override
    public void fbRegisterCallback(CallbackManager manager) {
        loginInteractor.fbRegisterCallback(manager, this);
    }

    @Override
    public void languageBurma() {
        loginView.languageBurma();
    }

    @Override
    public void languageEnglish() {
        loginView.languageEnglish();
    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onCompleted(JSONObject object, GraphResponse response) {
        if (response.getError() != null) {

        } else {
            loginView.showProgress();
            try {
                String name = object.getString("first_name") + " " + object.getString("last_name");
                String email = object.getString("email");

                loginInteractor.register(name,email,"","","","",1,this);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDestroy() {
        loginView = null;
    }

    @Override
    public void onError(FacebookException error) {
        Log.d(TAG, "onError: Facebook login");
        LoginManager.getInstance().logInWithReadPermissions(mActivity, IMP_PERMISSIONS);
    }

    @Override
    public void onAddressError(String msg) {

    }

    @Override
    public void onConfirmPasswordError(String msg) {

    }

    @Override
    public void onEmailError(String msg) {

    }

    @Override
    public void onError(String msg) {
        if (loginView != null) {
            loginView.setError(msg);
            loginView.hideProgress();
        }
    }

    @Override
    public void onNameError(String msg) {

    }

    @Override
    public void onPasswordError(String msg) {

    }

    @Override
    public void onPhoneNumberError(String msg) {

    }

    @Override
    public void onRegisterSuccess() {
        Log.d(TAG, "onSuccess: ");
        if (loginView != null) {
            loginView.navigateToHome();
            loginView.hideProgress();
        }
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        if (loginResult != null) {
            if (loginResult.getAccessToken() != null && loginResult.getAccessToken().getPermissions().containsAll(IMP_PERMISSIONS)) {
                loginInteractor.fbGraphCallback(loginResult.getAccessToken(), this);
            } else {
                LoginManager.getInstance().logInWithReadPermissions(mActivity, IMP_PERMISSIONS);
            }
        }
    }

    @Override
    public void onLoginSuccess() {
        if (loginView != null) {
            loginView.hideProgress();
            loginView.navigateToHome();
        }
    }

    @Override
    public void validateLoginCredentials(String userName, String password) {
        if (loginView != null) {
            loginView.showProgress();
        }

        loginInteractor.login(userName, password, this);
    }
}
