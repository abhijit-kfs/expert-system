package com.kfs.expertsystems.presenter.interactorImpl;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.kfs.expertsystems.application.AgriProApplication;
import com.kfs.expertsystems.models.UserRequest;
import com.kfs.expertsystems.models.UserResponse;
import com.kfs.expertsystems.presenter.interactor.LoginInteractor;
import com.kfs.expertsystems.utils.Constants;
import com.kfs.expertsystems.utils.Utils;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Tomesh on 20-09-2017.
 */

public class LoginInteractorImpl implements LoginInteractor {

    private final String TAG = LoginInteractorImpl.class.getName();

    @Override
    public void changeLanguage(int type, OnLanguageChangeListener listener) {
        if (type == 1) {
            AgriProApplication.getPref().put("language", 1);
            listener.languageEnglish();
        } else if (type == 2) {
            AgriProApplication.getPref().put("language", 2);
            listener.languageBurma();
        }
    }

    @Override
    public void fbGraphCallback(AccessToken accessToken, GraphRequest.GraphJSONObjectCallback callback) {
        GraphRequest request = GraphRequest.newMeRequest(accessToken, callback);
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,first_name,last_name,email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    public void fbRegisterCallback(CallbackManager manager, fbLoginCallback listener) {
        LoginManager.getInstance().registerCallback(manager, listener);
    }

    @Override
    public void login(final String username, final String password, final OnLoginFinishedListener listener) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                boolean error = false;
                /*if (TextUtils.isEmpty(username)) {
                    listener.onError("Please enter Username.");
                    error = true;
                    return;
                } else {
                    if (!Utils.isValidEmail(username)) {
                        listener.onError("Please enter valid Username.");
                        error = true;
                        return;
                    }
                }
                if (TextUtils.isEmpty(password)) {
                    listener.onError("Please enter Password.");
                    error = true;
                    return;
                }*/

                if (TextUtils.isEmpty(username)) {
                    listener.onError("Please enter Phone Number.");
                    error = true;
                    return;
                } else if (username.length() != 10) {
                    listener.onError("Please enter valid Phone number.");
                    error = true;
                    return;
                }

                if (!error) {

                    UserRequest request = new UserRequest();
                    request.setUserName(username);
                    request.setPassword(username);
                    request.setLoginType("0");

                    Call<UserResponse> call = AgriProApplication.getApiClient().signIn(request);
                    call.enqueue(new Callback<UserResponse>() {
                        @Override
                        public void onFailure(Call<UserResponse> call, Throwable t) {
                            Log.d(TAG, "onFailure: ");
                            listener.onError("Unable to connect please try again later.");
                        }

                        @Override
                        public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                            try {
                                if (response.body() != null) {

//                                    Log.d(TAG, "response code: " + response.body().getCode());

                                    if (response.body().getCode().equalsIgnoreCase("200")) {

                                        AgriProApplication.getPref().put(Constants.USER_ID, response.body()
                                                .getResponseData
                                                        ().getUserId());

                                        AgriProApplication.getPref().put(Constants.LOGIN, true);

                                        listener.onLoginSuccess();

                                    } else {
                                        listener.onError(response.body()
                                                .getResponseData().getErrorMsg());
                                    }
                                } else {
                                    listener.onError("Unable to connect please try again later.");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                listener.onError("Unable to connect please try again later.");
                            }
                        }
                    });


                }
            }
        }, 500);
    }

    @Override
    public void register(final String name, String email, final String phone, String address, String
            password, String confirmPassword, final int facebookLogin, final OnRegisterFinishedListener listener) {

//        email = "mobile@expertsystems.com";
        email = phone;
        address = "Not Available";
        password = phone;
        confirmPassword = phone;

        final String finalEmail = email;
        final String finalPassword = password;
        final String finalConfirmPassword = confirmPassword;
        final String finalAddress = address;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                boolean error = false;

                if (TextUtils.isEmpty(name)) {
                    listener.onNameError("Please enter Name.");
                    error = true;
                    return;
                }


                if (TextUtils.isEmpty(finalEmail)) {
                    listener.onEmailError("Please enter Email-ID.");
                    error = true;
                    return;
                } /*else {
                    if (!Utils.isValidEmail(finalEmail)) {
                        listener.onEmailError("Please enter valid Email-ID.");
                        error = true;
                        return;
                    }
                }*/

                if (facebookLogin != 1) {
                    if (TextUtils.isEmpty(phone)) {
                        listener.onPhoneNumberError("Please enter Phone number.");
                        error = true;
                        return;
                    }

                    if (phone.length() != 10) {
                        listener.onPhoneNumberError("Please enter valid Phone number.");
                        error = true;
                        return;
                    }

                    if (TextUtils.isEmpty(finalAddress)) {
                        listener.onAddressError("Please enter Address.");
                        error = true;
                        return;
                    }
                    if (TextUtils.isEmpty(finalPassword)) {
                        listener.onPasswordError("Please enter Password.");
                        error = true;
                        return;
                    }
                    if (TextUtils.isEmpty(finalConfirmPassword)) {
                        listener.onConfirmPasswordError("Please enter Confirm Password.");
                        error = true;
                        return;
                    }
                    if (!finalPassword.equals(finalConfirmPassword)) {
                        listener.onConfirmPasswordError("Please enter valid Password.");
                        error = true;
                        return;
                    }
                }


                if (!error) {

//                    UserRequest request = new UserRequest();
//                    request.setEmail(email);
//                    request.setFirstName(name);
//                    request.setLoginType("1");
//                    request.setUserType("0");
//                    if (facebookLogin != 1) {
//                        request.setAddress(address);
//                        request.setPhoneNumber(Long.parseLong(phone));
//                        request.setPassword(password);
//                    } else {
//                        request.setFacebookLogin(facebookLogin);
//                    }

                    HashMap<String, Object> userRequest = new HashMap<String, Object>();
                    userRequest.put("firstName", name);
                    userRequest.put("email", finalEmail);
                    userRequest.put("loginType", 1);
                    userRequest.put("userType", 0);
                    if (facebookLogin != 1) {
                        userRequest.put("address", finalAddress);
                        userRequest.put("phoneNumber", Long.parseLong(phone));
                        userRequest.put("password", finalPassword);
                    } else {
                        userRequest.put("facebookLogin", facebookLogin);
                    }

                    Call<UserResponse> call = AgriProApplication.getApiClient().registration(userRequest);
                    call.enqueue(new Callback<UserResponse>() {
                        @Override
                        public void onFailure(Call<UserResponse> call, Throwable t) {
                            Log.d(TAG, "onFailure: ");
                            listener.onError("Unable to connect please try again later.");
                        }

                        @Override
                        public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {

                            Log.d(TAG, "response code: " + response.body()
                                    .getResponseData
                                            ().getUserId());
                            try {
                                if (response.body().getCode().equalsIgnoreCase("200")) {
                                    AgriProApplication.getPref().put(Constants.USER_ID, response.body()
                                            .getResponseData
                                                    ().getUserId());
                                    AgriProApplication.getPref().put(Constants.LOGIN, true);
                                    if (facebookLogin == 1 && response.body().getResponseData().getPasswordExists() == 0) {
                                        AgriProApplication.getPref().put(Constants.SHOW_ADD_PASSWORD, true);
                                    } else {
                                        AgriProApplication.getPref().put(Constants.SHOW_ADD_PASSWORD, false);
                                    }
                                    listener.onRegisterSuccess();

                                } else {
                                    listener.onError(response.body()
                                            .getResponseData().getErrorMsg());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                listener.onError("Unable to connect please try again later.");
                            }
                        }
                    });
                }
            }
        }, 500);

    }
}
