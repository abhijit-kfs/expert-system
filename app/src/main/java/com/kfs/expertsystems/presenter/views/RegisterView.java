package com.kfs.expertsystems.presenter.views;

/**
 * Created by Tomesh on 20-09-2017.
 */

public interface RegisterView extends BaseView {


    void onEmailError(String msg);

    void onNameError(String msg);

    void onPhoneNumberError(String msg);

    void onAddressError(String msg);

    void onPasswordError(String msg);

    void onConfirmPasswordError(String msg);
}
