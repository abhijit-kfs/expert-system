package com.kfs.expertsystems.presenter.presenterImpl;

import android.util.Log;

import com.kfs.expertsystems.models.CropResponse;
import com.kfs.expertsystems.presenter.interactor.CropInteractor;
import com.kfs.expertsystems.presenter.interactorImpl.CropInteractorImpl;
import com.kfs.expertsystems.presenter.presenter.CropPresenter;
import com.kfs.expertsystems.presenter.views.CropsView;

/**
 * Created by Tomesh on 21-09-2017.
 */

public class CropPresenterImpl implements CropPresenter, CropInteractor.OnFinishedCrops {

    private final String TAG = CropPresenterImpl.class.getName();
    private CropsView cropsView;
    private CropInteractor cropInteractor;

    public CropPresenterImpl(CropsView cropsView) {

        this.cropsView = cropsView;
        cropInteractor = new CropInteractorImpl();
    }


    @Override
    public void getCrop() {
        Log.d(TAG, "getCrop: ");

        if (cropsView != null) {
            cropsView.showProgress();
        }
        cropInteractor.getCrops(this);

    }

    @Override
    public void onDestroy() {
        cropsView = null;
    }

    @Override
    public void onSuccess(CropResponse crops) {
        if (cropsView != null) {
            cropsView.hideProgress();
            cropsView.onDataFound(crops);

        }
    }

    @Override
    public void onError(String msg) {
        if (cropsView != null) {
            cropsView.hideProgress();
            cropsView.setError(msg);
        }

    }

    @Override
    public void onDataNotFound(String msg) {
        if (cropsView != null) {
            cropsView.hideProgress();
            cropsView.onDataNotFound(msg);
        }
    }
}
