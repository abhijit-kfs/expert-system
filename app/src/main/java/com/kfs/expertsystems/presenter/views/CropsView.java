package com.kfs.expertsystems.presenter.views;

import com.kfs.expertsystems.models.CropResponse;

/**
 * Created by Tomesh on 21-09-2017.
 */

public interface CropsView extends BaseView{

    void onDataFound(CropResponse crops);

    void onDataNotFound(String msg);
}

