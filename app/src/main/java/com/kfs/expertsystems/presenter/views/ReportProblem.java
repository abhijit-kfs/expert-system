package com.kfs.expertsystems.presenter.views;

/**
 * Created by Tomesh on 27-09-2017.
 */

public interface ReportProblem  extends BaseView{

    void onSuccess(Object data);

    void onFailure();

    void onDescriptionError();

    void onImageEmpty();



}
