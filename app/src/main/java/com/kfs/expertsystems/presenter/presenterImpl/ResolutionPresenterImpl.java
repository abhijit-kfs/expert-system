package com.kfs.expertsystems.presenter.presenterImpl;

import android.net.Uri;

import com.kfs.expertsystems.models.ResolutionResponse;
import com.kfs.expertsystems.models.ResolvedModel;
import com.kfs.expertsystems.presenter.interactor.ResolutionInteractor;
import com.kfs.expertsystems.presenter.interactorImpl.ResolutionInteractorImpl;
import com.kfs.expertsystems.presenter.presenter.ResolutionPresenter;
import com.kfs.expertsystems.presenter.views.ResolutionView;

import java.util.List;

/**
 * Created by Tomesh on 27-09-2017.
 */

public class ResolutionPresenterImpl implements ResolutionPresenter ,ResolutionInteractor
        .OnFinishedEnquiry , ResolutionInteractor.OnAddComment,ResolutionInteractor.OnUpdateResolution{

    private ResolutionView resolutionView;
    private ResolutionInteractor resolutionInteractor;


   public ResolutionPresenterImpl(ResolutionView resolutionView){

       this.resolutionView = resolutionView;
       resolutionInteractor = new ResolutionInteractorImpl();
   }

    @Override
    public void markAsResolved(ResolvedModel resolvedModel) {
        if (resolutionView != null) {
            resolutionView.showProgress();
        }

        resolutionInteractor.markAsResolved( resolvedModel ,this);
    }
    @Override
    public void getEnquiryList() {
        if(resolutionView!= null){
            resolutionView.showProgress();
        }
        resolutionInteractor.getEnquiryList(this);
    }

    @Override
    public void onDestroy() {
        resolutionView = null;
    }

    @Override
    public void addComment(List<Uri> uriListImage, List<Uri> uriListVideo, String desc, int
            problemId) {
        if(resolutionView!= null){
            resolutionView.showProgress();
        }
        resolutionInteractor.addComment(uriListImage,uriListVideo,desc,problemId,this);

    }

    @Override
    public void onEnquiryFetchSuccess(ResolutionResponse resolutionResponse) {
        if(resolutionView!= null){
            resolutionView.hideProgress();
            resolutionView.onEnquiryFetchSuccess(resolutionResponse);
        }
    }

    @Override
    public void onEnquiryFetchError(String msg) {
        if(resolutionView!= null){
            resolutionView.hideProgress();
            resolutionView.onEnquiryFetchError(msg);
        }
    }

    @Override
    public void onEnquiryDataNotFound(String msg) {
        if(resolutionView!= null){
            resolutionView.hideProgress();
            resolutionView.onEnquiryDataNotFound(msg);
        }
    }

    @Override
    public void onAddCommentSuccess(Object data) {
        if(resolutionView!= null){
            resolutionView.hideProgress();
            resolutionView.onAddCommentSuccess(data);
        }
    }

    @Override
    public void onAddCommentError(String msg) {
        if(resolutionView!= null){
            resolutionView.hideProgress();
            resolutionView.onAddCommentError(msg);
        }
    }



    @Override
    public void onProblemResolvedSucess() {
        if (resolutionView != null) {
            resolutionView.hideProgress();
            resolutionView.onProblemResolvedSucess();
        }
    }

    @Override
    public void onProblemResolvedError(String msg) {
        if (resolutionView != null) {
            resolutionView.hideProgress();
            resolutionView.onProblemResolvedError(msg);
        }
    }
}
