package com.kfs.expertsystems.presenter.interactorImpl;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.kfs.expertsystems.R;
import com.kfs.expertsystems.application.AgriProApplication;
import com.kfs.expertsystems.models.AddComments;
import com.kfs.expertsystems.models.AddcommentResponse;
import com.kfs.expertsystems.models.BaseResponse;
import com.kfs.expertsystems.models.ResolutionRequest;
import com.kfs.expertsystems.models.ResolutionResponse;
import com.kfs.expertsystems.models.ResolvedModel;
import com.kfs.expertsystems.presenter.interactor.ResolutionInteractor;
import com.kfs.expertsystems.utils.Constants;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Tomesh on 27-09-2017.
 */

public class ResolutionInteractorImpl implements ResolutionInteractor {
    private final String TAG = ResolutionInteractorImpl.class.getName();

    @Override
    public void getEnquiryList(final OnFinishedEnquiry listener) {


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                ResolutionRequest request =  new ResolutionRequest();
                request.setUid(AgriProApplication.getPref().getString(Constants.USER_ID));
                Call<ResolutionResponse> call = AgriProApplication.getApiClient().getEnquiryList(request);

                call.enqueue(new Callback<ResolutionResponse>() {
                    @Override
                    public void onResponse(Call<ResolutionResponse> call, Response<ResolutionResponse> response) {
                        try {

                        if (response.body() != null) {
                            if (response.body().getCode() == 200) {
                                listener.onEnquiryFetchSuccess(response.body());
                            } else {
                                listener.onEnquiryFetchError(AgriProApplication.getAppContext()
                                        .getResources().getString(R.string.no_data_available));
                            }
                        } else {
                            listener.onEnquiryFetchError(AgriProApplication.getAppContext()
                                    .getResources().getString(R.string.unable_to_connect_server));
                        }
                    }catch (Exception e){
                            e.printStackTrace();
                            listener.onEnquiryDataNotFound(AgriProApplication.getAppContext()
                                    .getResources().getString(R.string.internal_error));
                        }
                    }

                    @Override
                    public void onFailure(Call<ResolutionResponse> call, Throwable t) {
                        listener.onEnquiryDataNotFound(AgriProApplication.getAppContext()
                                .getResources().getString(R.string.unable_to_connect));
                    }
                });


            }
        }, 500);
    }



    @Override
    public void markAsResolved(final ResolvedModel resolvedModel, final OnUpdateResolution listener) {

        Log.d(TAG, "markAsResolved: ");

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                boolean error = false;

                if(resolvedModel!= null){

                    if(TextUtils.isEmpty(resolvedModel.getStatus())){
                        listener.onProblemResolvedError("Issue with problem status. Please try again later.");
                        return;
                    }

                    if(resolvedModel.getEnquiryId() == 0 ){
                        listener.onProblemResolvedError("Issue with problem Id. Please try again later.");
                        return;
                    }


                    if(!error){

                        Call<BaseResponse> call = AgriProApplication.getApiClient()
                                .updateEnquiry(resolvedModel);

                        call.enqueue(new Callback<BaseResponse>() {
                            @Override
                            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                                try {
                                    if (response.body() != null) {

                                        Log.d(TAG, "response code: " + response.body().getCode());

                                        if (response.body().getCode().equalsIgnoreCase("200")) {

                                            listener.onProblemResolvedSucess();

                                        } else {
                                            listener.onProblemResolvedError(response.message());
                                        }
                                    } else {
                                        listener.onProblemResolvedError("Internal Error, please try again later.");
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(Call<BaseResponse> call, Throwable t) {
                                listener.onProblemResolvedError("Unable to connect please try again later.");
                            }
                        });


                    }

                }else{
                    listener.onProblemResolvedError("Unable to connect please try again later.");
                    return;
                }


            }
        }, 500);

    }


    @Override
    public void addComment(final List<Uri> uriListImage, final List<Uri> uriListVideo, final String desc, final int problemId, final OnAddComment listener) {


        Log.d(TAG, "addComment: ");


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                boolean error = false;

                AddComments addComments = new AddComments();

                addComments.setEnquiryId(problemId);
                addComments.setDescription(desc);
                addComments.setUserType(0);
                addComments.setUserId(AgriProApplication.getPref().getString(Constants.USER_ID));

                List<AddComments.CommentsImagesBean> imageList = new ArrayList<>();

                for(Uri uri : uriListImage){
                    AddComments.CommentsImagesBean model = new AddComments.CommentsImagesBean();
                    model.setImage(getBitmapFromUri(uri, true));
                    imageList.add(model);
                }
                addComments.setCommentsImages(imageList);

                List<AddComments.CommentsVideosBean> videoList = new ArrayList<>();
                for(Uri uri : uriListVideo){
                    AddComments.CommentsVideosBean model = new AddComments.CommentsVideosBean();
                    model.setVideo(getBitmapFromUri(uri, false));
                    videoList.add(model);
                }
                addComments.setCommentsVideos(videoList);

                Call<AddcommentResponse> call = AgriProApplication.getApiClient()
                        .addComments(addComments);

                call.enqueue(new Callback<AddcommentResponse>() {
                    @Override
                    public void onResponse(Call<AddcommentResponse> call, Response<AddcommentResponse> response) {
                        try {
                            if (response.body() != null) {

                                Log.d(TAG, "response code: " + response.body().getCode());

                                if (response.body().getCode() == 200) {

                                    listener.onAddCommentSuccess(response.body());

                                } else {
                                    listener.onAddCommentError(response.message());
                                }
                            } else {
                                listener.onAddCommentError("Internal Error, please try again later.");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<AddcommentResponse> call, Throwable t) {
                        listener.onAddCommentError("No network available, please try again later.");
                    }
                });

            }
        }, 500);

    }




    private String getBitmapFromUri(Uri uri, Boolean isImage) {


        Bitmap thumbnail = null;
        String base64 = "";
        try {
            thumbnail = MediaStore.Images.Media.getBitmap(AgriProApplication.getAppContext()
                            .getContentResolver()
                    , Uri.parse
                            (uri.toString()));
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();

            if(isImage) {
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 70, bytes);
                base64 = Base64.encodeToString(bytes.toByteArray(), Base64.DEFAULT);
            } else {
                InputStream inputStream = null;
                try
                {
                    inputStream = AgriProApplication.getAppContext().getContentResolver().openInputStream(uri);
                }
                catch (FileNotFoundException e)
                {
                    e.printStackTrace();
                }
                int bufferSize = 1024;
                byte[] buffer = new byte[bufferSize];
                ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
                int len = 0;
                try
                {
                    while ((len = inputStream.read(buffer)) != -1)
                    {
                        byteBuffer.write(buffer, 0, len);
                    }
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }

                //Converting bytes into base64
                base64 = Base64.encodeToString(byteBuffer.toByteArray(), Base64.DEFAULT);
            }

            return base64;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
