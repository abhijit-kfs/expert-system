package com.kfs.expertsystems.presenter.presenter;

/**
 * Created by shylesh on 17/04/18.
 */

public interface ProductsPresenter {

    void getProducts();

    void getCategories();

    void onDestroy();
}
