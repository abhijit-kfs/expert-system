package com.kfs.expertsystems.presenter.presenter;


import com.facebook.CallbackManager;

/**
 * Created by Tomesh on 20-09-2017.
 */

public interface LoginPresenter {


    void validateLoginCredentials(String userName, String password);

    void changeLanguage(int type);

    void onDestroy();

    void fbRegisterCallback(CallbackManager manager);
}
