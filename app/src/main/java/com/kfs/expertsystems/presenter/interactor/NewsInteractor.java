package com.kfs.expertsystems.presenter.interactor;

import com.kfs.expertsystems.models.NewsResponse;

/**
 * Created by Tomesh on 11-10-2017.
 */

public interface NewsInteractor {


    interface onNewsFinish {

        void onDataFound(NewsResponse dealers);

        void onError(String msg);

        void onDataNotFound(String msg);
    }

    void getNews(onNewsFinish listener);
}
