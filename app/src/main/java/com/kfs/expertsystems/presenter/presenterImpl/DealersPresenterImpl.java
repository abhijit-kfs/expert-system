package com.kfs.expertsystems.presenter.presenterImpl;

import com.kfs.expertsystems.models.DealersResponse;
import com.kfs.expertsystems.presenter.interactor.DealersInteractor;
import com.kfs.expertsystems.presenter.interactorImpl.DealersInteractorImpl;
import com.kfs.expertsystems.presenter.presenter.DealersPresenter;
import com.kfs.expertsystems.presenter.views.DealersView;

/**
 * Created by Tomesh on 09-10-2017.
 */

public class DealersPresenterImpl implements DealersPresenter , DealersInteractor.onDealersFinish {


    private DealersView dealersView;
    private DealersInteractor dealersInteractor;


    public DealersPresenterImpl(DealersView dealersView) {
        this.dealersView = dealersView;
        this.dealersInteractor = new DealersInteractorImpl();
    }




    @Override
    public void getDealer() {

        if(dealersView != null){
            dealersView.showProgress();
        }
        dealersInteractor.getDealers(this);
    }

    @Override
    public void onDestroy() {

        dealersView = null;
    }

    @Override
    public void onDataFound(DealersResponse dealers) {
        if(dealersView != null){
            dealersView.hideProgress();
            dealersView.onDataFound(dealers);
        }
    }

    @Override
    public void onError(String msg) {
        if(dealersView != null){
            dealersView.hideProgress();
            dealersView.setError(msg);
        }
    }

    @Override
    public void onDataNotFound(String msg) {
        if(dealersView != null){
            dealersView.hideProgress();
            dealersView.onDataNotFound(msg);
        }
    }
}
