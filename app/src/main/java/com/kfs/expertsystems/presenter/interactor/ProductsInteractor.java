package com.kfs.expertsystems.presenter.interactor;

import com.kfs.expertsystems.models.CategoryResponse;
import com.kfs.expertsystems.models.ProductsResponse;

/**
 * Created by swaroop on 17/04/18.
 */

public interface ProductsInteractor {

    interface onProductsFinish {

        void onDataFound(ProductsResponse products);

        void onError(String msg);

        void onDataNotFound(String msg);
    }

    interface onCategoryFinish {

        void onCategoryDataFound(CategoryResponse category);

        void onError(String msg);

        void onCategoryDataNotFound(String msg);
    }

    void getProducts(ProductsInteractor.onProductsFinish listener);
    void getCategory(ProductsInteractor.onCategoryFinish listener1);
}
