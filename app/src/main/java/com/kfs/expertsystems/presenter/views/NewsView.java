package com.kfs.expertsystems.presenter.views;

import com.kfs.expertsystems.models.NewsResponse;

/**
 * Created by Tomesh on 09-10-2017.
 */

public interface NewsView extends BaseView{



    void onDataFound(NewsResponse news);

    void onError(String msg);

    void onDataNotFound(String msg);
}


