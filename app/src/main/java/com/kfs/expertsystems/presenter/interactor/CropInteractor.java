package com.kfs.expertsystems.presenter.interactor;

import com.kfs.expertsystems.models.CropResponse;

/**
 * Created by Tomesh on 21-09-2017.
 */

public interface CropInteractor {




    interface OnFinishedCrops {

        void onSuccess(CropResponse crops);
        void onError(String msg);
        void onDataNotFound(String msg);

    }


    void getCrops(OnFinishedCrops listener);
}
