package com.kfs.expertsystems.presenter.presenterImpl;

import com.kfs.expertsystems.models.NewsResponse;
import com.kfs.expertsystems.presenter.interactor.NewsInteractor;
import com.kfs.expertsystems.presenter.interactorImpl.NewsInteractorImpl;
import com.kfs.expertsystems.presenter.presenter.NewsPresenter;
import com.kfs.expertsystems.presenter.views.NewsView;

/**
 * Created by Tomesh on 09-10-2017.
 */

public class NewsPresenterImpl implements NewsPresenter, NewsInteractor.onNewsFinish {


    private NewsView newsView;
    private NewsInteractor newsInteractor;


    public NewsPresenterImpl(NewsView dealersView) {
        this.newsView = dealersView;
        this.newsInteractor = new NewsInteractorImpl();
    }


    @Override
    public void getNews() {

        if (newsView != null) {
            newsView.showProgress();
        }
        newsInteractor.getNews(this);
    }

    @Override
    public void onDestroy() {

        newsView = null;
    }

    @Override
    public void onDataFound(NewsResponse news) {
        if (newsView != null) {
            newsView.hideProgress();
            newsView.onDataFound(news);
        }
    }

    @Override
    public void onError(String msg) {
        if (newsView != null) {
            newsView.hideProgress();
            newsView.setError(msg);
        }
    }

    @Override
    public void onDataNotFound(String msg) {
        if (newsView != null) {
            newsView.hideProgress();
            newsView.onDataNotFound(msg);
        }
    }
}
