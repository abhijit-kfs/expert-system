package com.kfs.expertsystems.presenter.interactor;

import com.kfs.expertsystems.models.PromotionsResponse;

/**
 * Created by Tomesh on 21-09-2017.
 */

public interface PromotionInteractor {




    interface OnFinishedPromotions {

        void onSuccess(PromotionsResponse crops);
        void onError(String msg);
        void onDataNotFound(String msg);

    }


    void getPromotions(OnFinishedPromotions listener);
}
