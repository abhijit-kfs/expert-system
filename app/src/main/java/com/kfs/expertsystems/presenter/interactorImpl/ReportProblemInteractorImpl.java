package com.kfs.expertsystems.presenter.interactorImpl;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.kfs.expertsystems.application.AgriProApplication;
import com.kfs.expertsystems.models.AddEnquiry;
import com.kfs.expertsystems.models.CropResponse;
import com.kfs.expertsystems.models.ReportProblemResponse;
import com.kfs.expertsystems.presenter.interactor.ReportProblemInteractor;
import com.kfs.expertsystems.utils.Constants;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Tomesh on 27-09-2017.
 */

public class ReportProblemInteractorImpl implements ReportProblemInteractor {

    private final String TAG = ReportProblemInteractorImpl.class.getName();


    @Override
    public void validateProblem(final List<Uri> imageUriList, final String problemDesc, final List<Uri> videoUriList, final CropResponse.ResponseDataBean.CropsBean selectedCrop, final OnProblemFinishedListener listener) {
        Log.d(TAG, "validateProblem: ");

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                boolean error = false;
                if (imageUriList.isEmpty()) {
                    listener.onImageEmpty();
                    return;
                }

                if (TextUtils.isEmpty(problemDesc)) {
                    listener.onDescriptionError();
                    return;
                }

                if (!error) {

                    AddEnquiry addEnquiry = new AddEnquiry();

                    addEnquiry.setCropId(String.valueOf(selectedCrop.getId()));
                    addEnquiry.setEnquiriesDesc(problemDesc);
//                    addEnquiry.setStageId(String.valueOf(selectedStage.getId()));
                    addEnquiry.setStatus("open");
                    String latlng = AgriProApplication.getPref().getString(Constants.LATLNG);
                    String[] arr = latlng.split(",");
                    String latitude = arr[0];
                    String longitude = arr[1];
                    addEnquiry.setLatitude(latitude);
                    addEnquiry.setLongitude(longitude);
                    addEnquiry.setUserId(AgriProApplication.getPref().getString(Constants.USER_ID));
                    List<AddEnquiry.EnquiriesImagesBean> enquiriesImages = new ArrayList<>();

                    for (int i = 0; i < imageUriList.size(); i++) {
                        AddEnquiry.EnquiriesImagesBean enquiriesImagesBean = new AddEnquiry
                                .EnquiriesImagesBean();
                        String base64 = getBitmapFromUri(imageUriList.get(i), true);
                        if (base64 != null) {
                            enquiriesImagesBean.setImage(base64);
                            enquiriesImages.add(enquiriesImagesBean);
                        }

                    }
                    addEnquiry.setEnquiriesImages(enquiriesImages);

                    if(videoUriList.size() > 0) {
                        List<AddEnquiry.EnquiriesVideosBean> enquiriesVideos = new ArrayList<>();

                        for (int i = 0; i < imageUriList.size(); i++) {
                            AddEnquiry.EnquiriesVideosBean enquiriesVideosBean = new AddEnquiry
                                    .EnquiriesVideosBean();
                            String base64 = getBitmapFromUri(videoUriList.get(i), false);
                            if (base64 != null) {
                                enquiriesVideosBean.setVideo(base64);
                                enquiriesVideos.add(enquiriesVideosBean);
                            }

                        }
                        addEnquiry.setEnquiriesVideos(enquiriesVideos);
                    }


                   /* Gson gson = new Gson();
                    String mainHolder = gson.toJson(addEnquiry);
                    Log.d(TAG, "submitEnquiry: " + mainHolder);*/
                    Call<ReportProblemResponse> call = AgriProApplication.getApiClient().addEnquiry(addEnquiry);
                    call.enqueue(new Callback<ReportProblemResponse>() {
                        @Override
                        public void onResponse(Call<ReportProblemResponse> call, Response<ReportProblemResponse> response) {
                            try {
                                if (response.body() != null) {

                                    Log.d(TAG, "response code: " + response.body().getCode());

                                    if (response.body().getCode() == 200) {


                                        listener.onSuccess(response.body());

                                    } else {

                                        listener.onError(response.code() + "");
                                    }
                                } else {
                                    listener.onError("Internal Error, please try again later.");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(Call<ReportProblemResponse> call, Throwable t) {
                            listener.onError("Unable to connect please try again later.");
                        }
                    });

                }


            }
        }, 500);


    }


    private String getBitmapFromUri(Uri uri, Boolean isImage) {


        Bitmap thumbnail = null;
        String base64 = "";
        try {
            thumbnail = MediaStore.Images.Media.getBitmap(AgriProApplication.getAppContext()
                            .getContentResolver()
                    , Uri.parse
                            (uri.toString()));
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            if(isImage) {
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 70, bytes);
                base64 = Base64.encodeToString(bytes.toByteArray(), Base64.DEFAULT);
            } else {
                InputStream inputStream = null;
                try
                {
                    inputStream = AgriProApplication.getAppContext().getContentResolver().openInputStream(uri);
                }
                catch (FileNotFoundException e)
                {
                    e.printStackTrace();
                }
                int bufferSize = 1024;
                byte[] buffer = new byte[bufferSize];
                ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
                int len = 0;
                try
                {
                    while ((len = inputStream.read(buffer)) != -1)
                    {
                        byteBuffer.write(buffer, 0, len);
                    }
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }

                //Converting bytes into base64
                base64 = Base64.encodeToString(byteBuffer.toByteArray(), Base64.DEFAULT);
            }

            return base64;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
