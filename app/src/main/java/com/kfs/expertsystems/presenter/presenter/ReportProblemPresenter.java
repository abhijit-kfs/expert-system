package com.kfs.expertsystems.presenter.presenter;

import android.net.Uri;

import com.kfs.expertsystems.models.CropResponse;

import java.util.List;

/**
 * Created by Tomesh on 27-09-2017.
 */

public interface ReportProblemPresenter {

    void validateProblem(List<Uri> imageUriList, String problemDesc , List<Uri> videoUriList,
                         CropResponse.ResponseDataBean.CropsBean selectedCrop);



      void onDestroy();
}