package com.kfs.expertsystems.presenter.presenter;

/**
 * Created by Tomesh on 21-09-2017.
 */

public interface CropPresenter {

    void getCrop();

    void onDestroy();
}
