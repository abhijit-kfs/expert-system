package com.kfs.expertsystems.presenter.interactorImpl;

import android.os.Handler;
import android.util.Log;

import com.kfs.expertsystems.R;
import com.kfs.expertsystems.application.AgriProApplication;
import com.kfs.expertsystems.models.PromotionsResponse;
import com.kfs.expertsystems.presenter.interactor.PromotionInteractor;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * Created by Tomesh on 21-09-2017.
 */

public class PromotionInteractorImpl implements PromotionInteractor {

    @Override
    public void getPromotions(final OnFinishedPromotions listener) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                boolean error = false;

                Call<PromotionsResponse> call = AgriProApplication.getApiClient().getAllPromotion();
                call.enqueue(new Callback<PromotionsResponse>() {
                    @Override
                    public void onFailure(Call<PromotionsResponse> call, Throwable t) {
                        Log.d(TAG, "onFailure: " + t.getMessage());
//                        listener.onError(AgriProApplication.getAppContext().getString(R.string.unable_to_connect_server));
                        listener.onError("");
                    }

                    @Override
                    public void onResponse(Call<PromotionsResponse> call, Response<PromotionsResponse> response) {
                        try {
                            if (response.body() != null) {
                                if (response.body().getCode() == 200) {

                                    if (response.body().getResponseData().getPromos().isEmpty()) {

//                                        listener.onDataNotFound(AgriProApplication.getAppContext().getString(R.string.crops_not_available));
                                        listener.onDataNotFound("");

                                    } else {

                                        listener.onSuccess(response.body());

                                    }

                                } else {
//                                    listener.onError(AgriProApplication.getAppContext().getString(R.string.no_data_available));
                                    listener.onError("");
                                }
                            } else {
                                listener.onError(AgriProApplication.getAppContext().getString(R.string
                                        .unable_to_connect));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onError("Internal Error, Please try again later.");
                        }
                    }
                });
            }
        }, 500);
    }
}
