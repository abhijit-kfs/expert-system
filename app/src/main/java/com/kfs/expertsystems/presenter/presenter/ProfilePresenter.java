package com.kfs.expertsystems.presenter.presenter;

import com.kfs.expertsystems.models.AddPasswordRequest;
import com.kfs.expertsystems.models.ChangePasswordRequest;
import com.kfs.expertsystems.models.ProfileUpdateRequest;

/**
 * Created by Tomesh on 05-10-2017.
 */

public interface ProfilePresenter {

    void getProfile();

    void updateProfile(ProfileUpdateRequest profile);

    void changePassword(ChangePasswordRequest changePasswordRequest);

    void addPassword(AddPasswordRequest addPasswordRequest);

    void onDestroy();


}
