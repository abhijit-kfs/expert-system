package com.kfs.expertsystems.presenter.interactor;

import android.net.Uri;

import com.kfs.expertsystems.models.ResolutionResponse;
import com.kfs.expertsystems.models.ResolvedModel;

import java.util.List;

/**
 * Created by Tomesh on 27-09-2017.
 */

public interface ResolutionInteractor {

    interface OnFinishedEnquiry {

        void onEnquiryFetchSuccess(ResolutionResponse resolutionResponse);
        void onEnquiryFetchError(String msg);
        void onEnquiryDataNotFound(String msg);

    }


    interface OnUpdateResolution{

        void onProblemResolvedSucess();

        void onProblemResolvedError(String msg);
    }
    interface OnAddComment{

        void onAddCommentSuccess(Object data);
        void onAddCommentError(String msg);

    }


    void markAsResolved(ResolvedModel resolvedModel , OnUpdateResolution listener);
    void getEnquiryList(OnFinishedEnquiry listener);
    void addComment(List<Uri> uriListImage, List<Uri> uriListVideo, String desc, int problemId, OnAddComment listener);

}
