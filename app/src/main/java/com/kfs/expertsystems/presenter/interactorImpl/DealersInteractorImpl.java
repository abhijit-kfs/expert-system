package com.kfs.expertsystems.presenter.interactorImpl;

import android.os.Handler;
import android.util.Log;

import com.kfs.expertsystems.R;
import com.kfs.expertsystems.application.AgriProApplication;
import com.kfs.expertsystems.models.DealersResponse;
import com.kfs.expertsystems.models.ResolutionRequest;
import com.kfs.expertsystems.presenter.interactor.DealersInteractor;
import com.kfs.expertsystems.utils.Constants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Tomesh on 09-10-2017.
 */

public class DealersInteractorImpl implements DealersInteractor {

    private final String TAG = DealersInteractorImpl.class.getName();

    @Override
    public void getDealers(final onDealersFinish listener) {


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


                ResolutionRequest request = new ResolutionRequest();
                request.setUid(AgriProApplication.getPref().getString(Constants.USER_ID));
                Call<DealersResponse> call = AgriProApplication.getApiClient().getDealers();

                call.enqueue(new Callback<DealersResponse>() {
                    @Override
                    public void onResponse(Call<DealersResponse> call, Response<DealersResponse> response) {
                        Log.d(TAG, "onResponse: ");

                        try {

                            if (response.body() != null) {
                                if (response.body().getCode() == 200) {
                                    listener.onDataFound(response.body());
                                } else {
                                    listener.onDataNotFound(response.body().getMessage());
                                }
                            } else {
                                listener.onDataNotFound(AgriProApplication.getAppContext()
                                        .getResources().getString(R.string.unable_to_connect_server));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onDataNotFound(AgriProApplication.getAppContext()
                                    .getResources().getString(R.string.internal_error));
                        }
                    }

                    @Override
                    public void onFailure(Call<DealersResponse> call, Throwable t) {
                        Log.d(TAG, "onFailure: " + t.getMessage());
                        t.printStackTrace();
                        listener.onDataNotFound(AgriProApplication.getAppContext()
                                .getResources().getString(R.string.unable_to_connect));
                    }
                });


            }
        }, 500);


    }
}
