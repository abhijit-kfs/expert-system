package com.kfs.expertsystems.presenter.views;

import com.kfs.expertsystems.models.ProfileModel;

/**
 * Created by Tomesh on 05-10-2017.
 */

public interface ProfileView extends BaseView {

    void onProfileDataFound(ProfileModel profile);

    void onDataNotFound(String msg);

    void onProfileUpdate(String msg);

    void onNameError(String msg);

    void onPhoneNumberError(String msg);

    void onAddressError(String msg);

    void onProfileUpdated();

    void onProfileUpdateError(String msg);

    void onPasswordChangeSuccess(String msg);

    void onPasswordChangeError(String msg);

    void onOldPasswordError(String msg);

    void onNewPasswordError(String msg);

    void onConfirmPasswordError(String msg);

    void onAddPasswordSuccess(String msg);

    void onAddPasswordError(String msg);
}
