package com.kfs.expertsystems.presenter.presenter;

/**
 * Created by Shylesh on 23-Feb-18.
 */

public interface PromotionPresenter {

    void getPromotions();

    void onDestroy();

}
