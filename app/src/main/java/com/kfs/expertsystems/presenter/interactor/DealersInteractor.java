package com.kfs.expertsystems.presenter.interactor;

import com.kfs.expertsystems.models.DealersResponse;

/**
 * Created by Tomesh on 09-10-2017.
 */

public interface DealersInteractor {


    interface onDealersFinish {

         void onDataFound(DealersResponse dealers);

        void onError(String msg);

        void onDataNotFound(String msg);
    }

    void getDealers(onDealersFinish listener);



}
