package com.kfs.expertsystems.presenter.presenterImpl;

import android.util.Log;

import com.kfs.expertsystems.models.PromotionsResponse;
import com.kfs.expertsystems.presenter.interactor.PromotionInteractor;
import com.kfs.expertsystems.presenter.interactorImpl.PromotionInteractorImpl;
import com.kfs.expertsystems.presenter.presenter.PromotionPresenter;
import com.kfs.expertsystems.presenter.views.PromotionsView;

/**
 * Created by Shylesh on 23-Feb-18.
 */

public class PromotionPresenterImpl implements PromotionPresenter, PromotionInteractor.OnFinishedPromotions {

    private final String TAG = PromotionPresenterImpl.class.getName();
    private PromotionsView promotionsView;
    private PromotionInteractor promotionInteractor;

    public PromotionPresenterImpl(PromotionsView promotionsView) {

        this.promotionsView = promotionsView;
        promotionInteractor = new PromotionInteractorImpl();
    }

    @Override
    public void getPromotions() {
        Log.d(TAG, "getCrop: ");

        if (promotionsView != null) {
            promotionsView.showProgress();
        }
        promotionInteractor.getPromotions(this);
    }

    @Override
    public void onDestroy() {
        promotionsView = null;
    }

    @Override
    public void onSuccess(PromotionsResponse promotions) {
        if (promotionsView != null) {
            promotionsView.hideProgress();
            promotionsView.onDataFound(promotions);

        }
    }

    @Override
    public void onError(String msg) {
        if (promotionsView != null) {
            promotionsView.hideProgress();
            promotionsView.setError(msg);
        }

    }

    @Override
    public void onDataNotFound(String msg) {
        if (promotionsView != null) {
            promotionsView.hideProgress();
            promotionsView.onDataNotFound(msg);
        }
    }
}
