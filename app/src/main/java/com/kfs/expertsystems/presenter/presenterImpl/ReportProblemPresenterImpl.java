package com.kfs.expertsystems.presenter.presenterImpl;

import android.net.Uri;
import android.util.Log;

import com.kfs.expertsystems.models.CropResponse;
import com.kfs.expertsystems.presenter.interactor.ReportProblemInteractor;
import com.kfs.expertsystems.presenter.interactorImpl.ReportProblemInteractorImpl;
import com.kfs.expertsystems.presenter.presenter.ReportProblemPresenter;
import com.kfs.expertsystems.presenter.views.ReportProblem;

import java.util.List;

/**
 * Created by Tomesh on 27-09-2017.
 */

public class ReportProblemPresenterImpl implements ReportProblemPresenter, ReportProblemInteractor.OnProblemFinishedListener {

    private final String TAG = ReportProblemPresenterImpl.class.getName();
    private ReportProblem reportProblem;
    private ReportProblemInteractor reportProblemInteractor;

    public ReportProblemPresenterImpl(ReportProblem loginView) {
        this.reportProblem = loginView;
        this.reportProblemInteractor = new ReportProblemInteractorImpl();
    }

    @Override
    public void validateProblem(List<Uri> imageUriList, String problemDesc, List<Uri> videoUriList, CropResponse.ResponseDataBean.CropsBean selectedCrop) {
        Log.d(TAG, "validateProblem: ");

        if (reportProblem != null) {
            reportProblem.showProgress();
        }

        reportProblemInteractor.validateProblem(imageUriList, problemDesc, videoUriList,
                selectedCrop, this);
    }



    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy: ");
        reportProblem = null;
    }

    @Override
    public void onDescriptionError() {
        Log.d(TAG, "onDescriptionError: ");
        if (reportProblem != null) {
            reportProblem.hideProgress();
            reportProblem.onDescriptionError();
        }
    }

    @Override
    public void onImageEmpty() {
        Log.d(TAG, "onImageEmpty: ");
        if (reportProblem != null) {
            reportProblem.hideProgress();
            reportProblem.onImageEmpty();
        }
    }

    @Override
    public void onError(String msg) {
        Log.d(TAG, "onError: ");
        if (reportProblem != null) {
            reportProblem.hideProgress();
            reportProblem.setError(msg);
        }
    }

    @Override
    public void onSuccess(Object data) {
        Log.d(TAG, "onSuccess: ");
        if (reportProblem != null) {
            reportProblem.hideProgress();
            reportProblem.onSuccess(data);
        }
    }


}
