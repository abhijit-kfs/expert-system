package com.kfs.expertsystems.presenter.presenterImpl;

import android.util.Log;

import com.kfs.expertsystems.presenter.interactor.LoginInteractor;
import com.kfs.expertsystems.presenter.interactorImpl.LoginInteractorImpl;
import com.kfs.expertsystems.presenter.presenter.RegisterPresenter;
import com.kfs.expertsystems.presenter.views.RegisterView;

/**
 * Created by Tomesh on 20-09-2017.
 */

public class RegisterPresenterImpl implements RegisterPresenter, LoginInteractor.OnRegisterFinishedListener {

    private final String TAG = RegisterPresenterImpl.class.getName();
    private RegisterView registerView;
    private LoginInteractor loginInteractor;


    public RegisterPresenterImpl(RegisterView loginView) {
        this.registerView = loginView;
        this.loginInteractor = new LoginInteractorImpl();
    }

    @Override
    public void onAddressError(String msg) {
        Log.d(TAG, "onAddressError: ");

        if (registerView != null) {
            registerView.onAddressError(msg);
            registerView.hideProgress();
        }
    }

    @Override
    public void onConfirmPasswordError(String msg) {
        Log.d(TAG, "onConfirmPasswordError: ");

        if (registerView != null) {
            registerView.onConfirmPasswordError(msg);
            registerView.hideProgress();
        }
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy: ");
        registerView = null;
    }

    @Override
    public void onEmailError(String msg) {
        Log.d(TAG, "onEmailError: ");
        if (registerView != null) {
            registerView.onEmailError(msg);
            registerView.hideProgress();
        }
    }

    @Override
    public void onError(String msg) {
        Log.d(TAG, "onError: ");
        if (registerView != null) {
            registerView.setError(msg);
            registerView.hideProgress();
        }
    }

    @Override
    public void onNameError(String msg) {
        Log.d(TAG, "onNameError: ");
        if (registerView != null) {
            registerView.onNameError(msg);
            registerView.hideProgress();
        }

    }

    @Override
    public void onPasswordError(String msg) {
        Log.d(TAG, "onPasswordError: ");

        if (registerView != null) {
            registerView.onPasswordError(msg);
            registerView.hideProgress();
        }
    }

    @Override
    public void onPhoneNumberError(String msg) {
        Log.d(TAG, "onPhoneNumberError: ");

        if (registerView != null) {
            registerView.onPhoneNumberError(msg);
            registerView.hideProgress();
        }
    }

    @Override
    public void onRegisterSuccess() {
        Log.d(TAG, "onSuccess: ");
        if (registerView != null) {
            registerView.hideProgress();
            registerView.navigateToHome();
        }
    }

    @Override
    public void registerUser(String name, String email, String phone, String address, String password, String confirmPassword) {
        Log.d(TAG, "registerUser: ");

        if (registerView != null) {
            registerView.showProgress();
        }

        loginInteractor.register(name, email, phone, address, password, confirmPassword, 0, this);

    }
}
