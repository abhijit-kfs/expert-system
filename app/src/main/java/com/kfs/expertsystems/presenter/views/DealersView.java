package com.kfs.expertsystems.presenter.views;

import com.kfs.expertsystems.models.DealersResponse;

/**
 * Created by Tomesh on 09-10-2017.
 */

public interface DealersView extends BaseView{



    void onDataFound(DealersResponse dealers);

    void onError(String msg);

    void onDataNotFound(String msg);
}


