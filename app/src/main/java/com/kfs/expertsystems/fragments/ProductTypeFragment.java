package com.kfs.expertsystems.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kfs.expertsystems.R;
import com.kfs.expertsystems.adapters.ProductsTypeAdapter;
import com.kfs.expertsystems.callbacks.Interactor;
import com.kfs.expertsystems.models.CategoryResponse.ResponseDataBean;


import java.util.ArrayList;
import java.util.List;

public class ProductTypeFragment extends Fragment implements ProductsTypeAdapter.Callback {

    private ProductsTypeAdapter productsTypeAdapter;
    private Interactor.ProductType callback;

    private RecyclerView rvList;


    public static ProductTypeFragment getInstance() {
        Bundle args = new Bundle();
        ProductTypeFragment fragment = new ProductTypeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_type, container, false);

        rvList = view.findViewById(R.id.rv_list);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initAdapter();
        if (callback != null)
            updateAdapter(callback.getProductTypeList());
    }

    private void initAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        productsTypeAdapter = new ProductsTypeAdapter(new ArrayList<ResponseDataBean>(), getContext());
        productsTypeAdapter.setCallback(this);
        rvList.setLayoutManager(layoutManager);
        rvList.setAdapter(productsTypeAdapter);
        rvList.addItemDecoration(new DividerItemDecoration(rvList.getContext(), DividerItemDecoration.VERTICAL));
    }

    public void updateAdapter(List<ResponseDataBean> list) {
        if (productsTypeAdapter != null)
            productsTypeAdapter.updateList(list);
    }

    @Override
    public void onSelectedCategory(int id) {
        if (callback != null)
            callback.onCategorySelected(id);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callback = (Interactor.ProductType) context;
        if (productsTypeAdapter != null) {
            productsTypeAdapter.setCallback(this);
            productsTypeAdapter.setContext(context);
        }
    }

    @Override
    public void onDetach() {
        callback = null;
        if (productsTypeAdapter != null) {
            productsTypeAdapter.removeCallback();
            productsTypeAdapter.removeContext();
        }
        super.onDetach();
    }
}
