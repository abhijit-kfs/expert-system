package com.kfs.expertsystems.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kfs.expertsystems.R;

public class DealerMapFragment extends Fragment implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Double mLat = 0.0;
    private Double mLong = 0.0;
    private String mDealerName = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_maps, container, false);
//        mRootView = view;
//        intUi();
//
//        if (getArguments() != null) {
//            Log.d(TAG, "onCreateView: ");
//            dealersResponse = (DealersResponse) getArguments().getSerializable("DATA");
//            Log.d(TAG, "onCreateView: " + dealersResponse.getResponseData().getDealer().size());
//        }
//
//
//        SupportMapFragment supportMapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));
//        supportMapFragment.getMapAsync(this);
        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(mLat, mLong);
        mMap.addMarker(new MarkerOptions().position(sydney).icon(BitmapDescriptorFactory.fromResource(R
                .drawable.ic_location_yellow)).title(mDealerName));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }
}
