package com.kfs.expertsystems.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kfs.expertsystems.R;
import com.kfs.expertsystems.models.DealersResponse;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class DealersMapFragment extends Fragment implements OnMapReadyCallback, GoogleMap
        .OnMarkerClickListener, View.OnClickListener {

    private GoogleMap mMap;
    RelativeLayout markerItem;
    private View mRootView;
    private DealersResponse dealersResponse;
    List<DealersResponse.ResponseDataBean.DealerBean> insecticides;
    List<DealersResponse.ResponseDataBean.DealerBean> herbicides;
    List<DealersResponse.ResponseDataBean.DealerBean> fungicides;
    LinearLayout dealer_map_filter_insecticide, dealer_map_filter_herbicides, dealer_map_filter_fungicides,
            dealer_map_filter_others;
    String CurrentMarkerList = "";

    TextView dealer_list_name, dealer_list_company_name,
            dealer_list_phone, dealer_list_address, dealer_list_others_text;

    LinearLayout dealer_list_insecticide, dealer_list_herbicides, dealer_list_fungicides,
            dealer_list_others;

    RelativeLayout dealer_call;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dealers_map_fragment, container, false);
        mRootView = view;
        intUi();

        if (getArguments() != null) {
            Log.d(TAG, "onCreateView: ");
            dealersResponse = (DealersResponse) getArguments().getSerializable("DATA");
            Log.d(TAG, "onCreateView: " + dealersResponse.getResponseData().getDealer().size());

            insecticides = new ArrayList<>();
            herbicides = new ArrayList<>();
            fungicides = new ArrayList<>();


            if (dealersResponse != null) {
                int size = dealersResponse.getResponseData().getDealer().size();
                for (int i = 0; i < size; i++) {
                    DealersResponse.ResponseDataBean.DealerBean dealerBean;
                    dealerBean = dealersResponse.getResponseData().getDealer().get(i);

                    if (dealerBean.isPestisides()) {
                        insecticides.add(dealerBean);
                    }


                    if (dealerBean.isFungicides()) {
                        fungicides.add(dealerBean);
                    }


                    if (dealerBean.isHerbicides()) {
                        herbicides.add(dealerBean);
                    }

                }
            }

        }


        SupportMapFragment supportMapFragment = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map_view));
        supportMapFragment.getMapAsync(this);
        return view;
    }

    private void intUi() {
        markerItem = (RelativeLayout) mRootView.findViewById(R.id.marker_item);
        markerItem.setVisibility(View.INVISIBLE);

        dealer_map_filter_insecticide = (LinearLayout) mRootView.findViewById(R.id
                .dealer_map_filter_insecticide);
        dealer_map_filter_insecticide.setOnClickListener(this);

        dealer_map_filter_herbicides = (LinearLayout) mRootView.findViewById(R.id
                .dealer_map_filter_herbicides);
        dealer_map_filter_herbicides.setOnClickListener(this);

        dealer_map_filter_fungicides = (LinearLayout) mRootView.findViewById(R.id
                .dealer_map_filter_fungicides);
        dealer_map_filter_fungicides.setOnClickListener(this);

        dealer_map_filter_others = (LinearLayout) mRootView.findViewById(R.id
                .dealer_map_filter_others);

        dealer_map_filter_others.setOnClickListener(this);


        dealer_list_name = (TextView) mRootView.findViewById(R.id.dealer_list_name);
        dealer_list_company_name = (TextView) mRootView.findViewById(R.id.dealer_list_company_name);
        dealer_list_phone = (TextView) mRootView.findViewById(R.id.dealer_list_phone);
        dealer_list_address = (TextView) mRootView.findViewById(R.id.dealer_list_address);
        dealer_list_others_text = (TextView) mRootView.findViewById(R.id.dealer_list_others_text);

        dealer_list_insecticide = (LinearLayout) mRootView.findViewById(R.id.dealer_list_insecticide);
        dealer_list_herbicides = (LinearLayout) mRootView.findViewById(R.id.dealer_list_herbicides);
        dealer_list_fungicides = (LinearLayout) mRootView.findViewById(R.id.dealer_list_fungicides);
        dealer_list_others = (LinearLayout) mRootView.findViewById(R.id.dealer_list_others);
        dealer_call = (RelativeLayout) mRootView.findViewById(R.id.layout_call_map);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        setMarker(insecticides, "insecticides");
    }


    void setMarker(List<DealersResponse.ResponseDataBean.DealerBean> listMarker, String type) {
        LatLng latLng = null;
        markerItem.setVisibility(View.INVISIBLE);
        CurrentMarkerList = type;

        if (!listMarker.isEmpty()) {
            if (mMap != null) {
                for (int i = 0; i < listMarker.size(); i++) {
                    mMap.addMarker(new MarkerOptions().position(new LatLng(listMarker.get(i)
                            .getDealerLat(),
                            listMarker.get(i).getDealerLong())).icon(BitmapDescriptorFactory.fromResource(R
                            .drawable.ic_location_yellow)).title(listMarker.get(i).getDealerCompanyName()));
                    latLng = new LatLng(listMarker.get(i).getDealerLat(), listMarker.get(i).getDealerLong());
                    mMap.setOnMarkerClickListener(this);
                }
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 1));
            }
        }

    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        Log.d(TAG, "onMarkerClick: " + marker.getTitle());

        if (CurrentMarkerList.equalsIgnoreCase("insecticides")) {

            for (int i = 0; i < insecticides.size(); i++) {
                if (marker.getTitle().equalsIgnoreCase(insecticides.get(i)
                        .getDealerCompanyName())) {
                    setMarkerViewDetail(insecticides.get(i));
                    break;
                }
            }

        } else if (CurrentMarkerList.equalsIgnoreCase("herbicides")) {

            for (int i = 0; i < insecticides.size(); i++) {
                if (marker.getTitle().equalsIgnoreCase(insecticides.get(i)
                        .getDealerCompanyName())) {
                    setMarkerViewDetail(insecticides.get(i));
                    break;
                }
            }


        } else if (CurrentMarkerList.equalsIgnoreCase("fungicides")) {

            for (int i = 0; i < insecticides.size(); i++) {
                if (marker.getTitle().equalsIgnoreCase(insecticides.get(i)
                        .getDealerCompanyName())) {
                    setMarkerViewDetail(insecticides.get(i));
                    break;
                }
            }


        } else {

        }
        return true;
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.dealer_map_filter_insecticide:
                if (mMap != null) {
                    mMap.clear();
                    setMarker(insecticides, "insecticides");
                }
                break;
            case R.id.dealer_map_filter_herbicides:
                if (mMap != null) {
                    mMap.clear();
                    setMarker(herbicides, "herbicides");
                }
                break;
            case R.id.dealer_map_filter_fungicides:
                if (mMap != null) {
                    mMap.clear();
                    setMarker(fungicides, "fungicides");
                }
                break;
            case R.id.dealer_map_filter_others:
               /* if(mMap!= null)
                setMarker(insecticides , "insecticides");*/
                break;

        }

    }


    void setMarkerViewDetail(final DealersResponse.ResponseDataBean.DealerBean dealerBean) {

        markerItem.setVisibility(View.VISIBLE);


        dealer_list_name.setText(dealerBean.getDealerName());
        if (dealerBean.getDealerCompanyName() != null)
            dealer_list_company_name.setText(dealerBean.getDealerCompanyName());
        else
            dealer_list_company_name.setText(dealerBean.getDealerName());


        dealer_list_phone.setText(dealerBean.getDealerNumber());
        dealer_list_address.setText(dealerBean.getDealerAddress());

        Log.d("ced", dealerBean.getDealerNumber());

        dealer_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+ dealerBean.getDealerNumber()));
                getActivity().startActivity(intent);
            }
        });


        if (dealerBean.isPestisides()) {
            dealer_list_insecticide.setVisibility(View.VISIBLE);
        } else {
            dealer_list_insecticide.setVisibility(View.GONE);
        }
        if (dealerBean.isFungicides()) {
            dealer_list_fungicides.setVisibility(View.VISIBLE);
        } else {
            dealer_list_fungicides.setVisibility(View.GONE);
        }

        if (dealerBean.isHerbicides()) {
            dealer_list_herbicides.setVisibility(View.VISIBLE);
        } else {
            dealer_list_herbicides.setVisibility(View.GONE);
        }

        if (dealerBean.getOthers() != null) {
            if (!dealerBean.getOthers().isEmpty()) {
                dealer_list_others.setVisibility(View.VISIBLE);
                dealer_list_others_text.setText(dealerBean.getOthers());
            } else {
                dealer_list_others.setVisibility(View.GONE);
            }
        } else {
            dealer_list_others.setVisibility(View.GONE);
        }

    }
}
