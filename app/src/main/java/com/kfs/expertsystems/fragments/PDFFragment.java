package com.kfs.expertsystems.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.kfs.expertsystems.R;
import com.kfs.expertsystems.adapters.PDFAdapter;
import com.kfs.expertsystems.api.ApiClient;
import com.kfs.expertsystems.callbacks.Interactor;
import com.kfs.expertsystems.models.CategoryResponse.ResponseDataBean.GalleryCategoryBean;

import java.util.ArrayList;
import java.util.List;

public class PDFFragment extends Fragment implements PDFAdapter.Callback {


    private PDFAdapter pdfAdapter;
    private Interactor.Product callback;
    private int categoryId;

    private String baseUrl = "";

    private RecyclerView rvList;

    public static PDFFragment getInstance() {
        Bundle args = new Bundle();
        PDFFragment fragment = new PDFFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pdf, container, false);

        rvList = view.findViewById(R.id.rv_list);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initAdapter();
        if (callback != null) {
            if (categoryId > 0)
                updateAdapter(callback.getGalleryList(categoryId));
            else updateAdapter(new ArrayList<GalleryCategoryBean>());
        }
    }

    private void initAdapter() {
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 2);

        pdfAdapter = new PDFAdapter(getContext(), new ArrayList<GalleryCategoryBean>(), ApiClient.BASE_IMAGE_URL);
        pdfAdapter.setCallback(this);
        rvList.setLayoutManager(layoutManager);
        rvList.setAdapter(pdfAdapter);
        rvList.addItemDecoration(new DividerItemDecoration(rvList.getContext(), DividerItemDecoration.VERTICAL));
        rvList.addItemDecoration(new DividerItemDecoration(rvList.getContext(), DividerItemDecoration.HORIZONTAL));

        //pdfAdapter.updateBaseUrl(ApiClient.BASE_IMAGE_URL);
    }

    public void updateAdapter(List<GalleryCategoryBean> list) {
        if (pdfAdapter != null) {
            pdfAdapter.updateList(list);
        }
    }

    public void setBaseUrl(String imageUrl) {
        if (imageUrl != null && !imageUrl.isEmpty()) {
            baseUrl = imageUrl;
        }
    }

    @Override
    public void onErrorPdfUrl() {
        if (getContext() != null) {
            Toast.makeText(getContext(), "PDF URL is incorrect!",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (pdfAdapter != null) {
            pdfAdapter.setCallback(this);
            pdfAdapter.setContext(context);
        }
        callback = (Interactor.Product) context;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (callback != null) {
            if (categoryId > 0 && pdfAdapter != null) {
                updateAdapter(callback.getGalleryList(categoryId));
            }
        }
    }

    @Override
    public void onDetach() {
        callback = null;
        if (pdfAdapter != null) {
            pdfAdapter.removeCallback();
            pdfAdapter.removeContext();
        }
        super.onDetach();
    }

    public void setCategoryId(int id) {
        this.categoryId = id;
    }
}
