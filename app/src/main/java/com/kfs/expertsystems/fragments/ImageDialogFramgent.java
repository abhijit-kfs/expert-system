package com.kfs.expertsystems.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kfs.expertsystems.R;
import com.kfs.expertsystems.activities.MyResolutionDetailScreen;
import com.kfs.expertsystems.activities.ReportProblemScreen;

/**
 * Created by Tomesh on 26-09-2017.
 */


public class ImageDialogFramgent extends DialogFragment {

    private ImageView enlargeImage, dialog_image_delete, dialog_play, dialog_item_video;
    private String activity_name;
//    private VideoView dialog_item_video;

    public static ImageDialogFramgent newInstance(Uri uri, Boolean isImage) {
        ImageDialogFramgent f = new ImageDialogFramgent();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString("URI", uri.toString());
        args.putBoolean("isImage", isImage);
//        args.putString("Activity_name",activity_name);
        f.setArguments(args);

        return f;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_image, container,
                false);
        getDialog().setTitle("");

        // getDialog().setCanceledOnTouchOutside(true);
        // Do something else
        final String arg = getArguments().getString("URI", "");
        final Boolean isImage = getArguments().getBoolean("isImage");
        activity_name = getArguments().getString("Activity_name", "");
        enlargeImage = (ImageView) rootView.findViewById(R.id.dialog_image);
        dialog_image_delete = (ImageView) rootView.findViewById(R.id.dialog_image_delete);
        dialog_play = (ImageView) rootView.findViewById(R.id.dialog_play);
        dialog_item_video = (ImageView) rootView.findViewById(R.id.dialog_item_video);


        if (!arg.equalsIgnoreCase("")) {
//            enlargeImage.setImageBitmap( Utils.getBitmapFromUri(Uri.parse(arg),getContext()));
            if(isImage) {
                enlargeImage.setVisibility(View.VISIBLE);
                Glide.with(this).load(Uri.parse(arg))
                        .thumbnail(0.5f)
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .error(R.drawable.ic_image_place_holder)
                        .crossFade()
                        .into(enlargeImage);
                dialog_item_video.setVisibility(View.GONE);
                dialog_play.setVisibility(View.GONE);
            } else {
                dialog_item_video.setVisibility(View.VISIBLE);

                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContext().getContentResolver().query(Uri.parse(arg), filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();

                Bitmap bitmap2 = ThumbnailUtils.createVideoThumbnail(picturePath, MediaStore.Video.Thumbnails.MICRO_KIND);

                dialog_item_video.setImageBitmap(bitmap2);
                enlargeImage.setVisibility(View.GONE);
                dialog_play.setVisibility(View.VISIBLE);
            }
        }

        dialog_image_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getActivity() instanceof ReportProblemScreen)
                    ((ReportProblemScreen) getActivity()).onDelete(Uri.parse(arg), isImage);
                else if (getActivity() instanceof MyResolutionDetailScreen)
                    ((MyResolutionDetailScreen) getActivity()).onDelete(Uri.parse(arg), isImage);
            }
        });

        dialog_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String videoPath = arg;
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(videoPath));
                intent.setDataAndType(Uri.parse(videoPath), "video/mp4");
                getActivity().startActivity(intent);
            }
        });


        return rootView;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
//        dialog.getWindow().getAttributes().windowAnimations = R.style.detailDialogAnimation;
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();

        // safety check
        if (getDialog() == null)
            return;

        int dialogWidth = 700; // specify a value here
        int dialogHeight = 600; // specify a value here

        getDialog().getWindow().setLayout(dialogWidth, dialogHeight);

        // ... other stuff you want to do in your onStart() method

        Log.d("Frag", "onStart: ");
    }

}
