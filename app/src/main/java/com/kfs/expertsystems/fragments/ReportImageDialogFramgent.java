package com.kfs.expertsystems.fragments;

import android.app.Dialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kfs.expertsystems.R;

/**
 * Created by Tomesh on 26-09-2017.
 */


public class ReportImageDialogFramgent extends DialogFragment {

   private ImageView enlargeImage , dialog_image_delete, dialog_play;
    private ImageView dialog_item_video;

    public static ReportImageDialogFramgent newInstance(Uri uri, Boolean isImage) {
        ReportImageDialogFramgent f = new ReportImageDialogFramgent();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString("URI", uri.toString());
        f.setArguments(args);

        return f;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_image, container,
                false);
        getDialog().setTitle("");

        // getDialog().setCanceledOnTouchOutside(true);
        // Do something else
       final String arg =  getArguments().getString("URI","");
        enlargeImage = (ImageView) rootView.findViewById(R.id.dialog_image);
        dialog_image_delete = (ImageView) rootView.findViewById(R.id.dialog_image_delete);
        dialog_image_delete.setVisibility(View.GONE);
        dialog_play = (ImageView) rootView.findViewById(R.id.dialog_play);
        dialog_play.setVisibility(View.GONE);
        dialog_item_video = (ImageView) rootView.findViewById(R.id.dialog_item_video);
        dialog_item_video.setVisibility(View.GONE);
        if(!arg.equalsIgnoreCase("")){
//            enlargeImage.setImageBitmap( Utils.getBitmapFromUri(Uri.parse(arg),getContext()));
            Glide.with(this).load(Uri.parse(arg))
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .error(R.drawable.ic_image_place_holder)
                    .crossFade()
                    .into(enlargeImage);

        }



        return rootView;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
//        dialog.getWindow().getAttributes().windowAnimations = R.style.detailDialogAnimation;
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        return dialog;
    }

    @Override
    public void onStart()
    {
        super.onStart();

        // safety check
        if (getDialog() == null)
            return;

        int dialogWidth = 700; // specify a value here
        int dialogHeight = 600 ; // specify a value here

        getDialog().getWindow().setLayout(dialogWidth, dialogHeight);

        // ... other stuff you want to do in your onStart() method

        Log.d("Frag", "onStart: ");
    }

}
