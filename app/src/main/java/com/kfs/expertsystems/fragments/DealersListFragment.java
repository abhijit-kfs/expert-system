package com.kfs.expertsystems.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.kfs.expertsystems.R;
import com.kfs.expertsystems.adapters.MyDealersAdapter;
import com.kfs.expertsystems.models.DealersResponse;

import java.util.ArrayList;
import java.util.List;


public class DealersListFragment extends Fragment implements View.OnClickListener {
    private final String TAG = DealersListFragment.class.getName();
    RecyclerView rvDealers;
    MyDealersAdapter mAdapter;

    View mRooView;
    DealersResponse dealersResponse;
    List<DealersResponse.ResponseDataBean.DealerBean> insecticides;
    List<DealersResponse.ResponseDataBean.DealerBean> herbicides;
    List<DealersResponse.ResponseDataBean.DealerBean> fungicides;

    private LinearLayout dealer_list_filter_insecticide, dealer_list_filter_herbicide,
            dealer_list_filter_fungicides, dealer_list_filter_others;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dealers_list_fragment, container, false);
        mRooView = view;
        if (getArguments() != null) {
            Log.d(TAG, "onCreateView: ");
            dealersResponse = (DealersResponse) getArguments().getSerializable("DATA");
            Log.d(TAG, "onCreateView: " + dealersResponse.getResponseData().getDealer().size());
        }

        initUi();
        return view;
    }

    private void initUi() {

        rvDealers = (RecyclerView) mRooView.findViewById(R.id.rv_dealers);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvDealers.setLayoutManager(layoutManager);
        rvDealers.setItemAnimator(new DefaultItemAnimator());

        dealer_list_filter_insecticide = (LinearLayout) mRooView.findViewById(R.id
                .dealer_list_filter_insecticide);
        dealer_list_filter_insecticide.setOnClickListener(this);


        dealer_list_filter_herbicide = (LinearLayout) mRooView.findViewById(R.id
                .dealer_list_filter_herbicide);
        dealer_list_filter_herbicide.setOnClickListener(this);


        dealer_list_filter_fungicides = (LinearLayout) mRooView.findViewById(R.id
                .dealer_list_filter_fungicides);
        dealer_list_filter_fungicides.setOnClickListener(this);

        dealer_list_filter_others = (LinearLayout) mRooView.findViewById(R.id
                .dealer_list_filter_others);
        dealer_list_filter_others.setOnClickListener(this);

        insecticides = new ArrayList<>();
        herbicides = new ArrayList<>();
        fungicides = new ArrayList<>();


        if (dealersResponse != null) {
            int size = dealersResponse.getResponseData().getDealer().size();
            for (int i = 0; i < size; i++) {
                DealersResponse.ResponseDataBean.DealerBean dealerBean;
                dealerBean = dealersResponse.getResponseData().getDealer().get(i);

                if (dealerBean.isPestisides()) {
                    insecticides.add(dealerBean);
                }


                if (dealerBean.isFungicides()) {
                    fungicides.add(dealerBean);
                }


                if (dealerBean.isHerbicides()) {
                    herbicides.add(dealerBean);
                }

            }
        }


        mAdapter = new MyDealersAdapter(getActivity(), insecticides);

        rvDealers.setAdapter(mAdapter);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.dealer_list_filter_insecticide:
                mAdapter = new MyDealersAdapter(getActivity(), insecticides);
                rvDealers.setAdapter(mAdapter);
                break;
            case R.id.dealer_list_filter_herbicide:
                mAdapter = new MyDealersAdapter(getActivity(), herbicides);
                rvDealers.setAdapter(mAdapter);
                break;
            case R.id.dealer_list_filter_fungicides:
                mAdapter = new MyDealersAdapter(getActivity(), fungicides);
                rvDealers.setAdapter(mAdapter);
                break;
            case R.id.dealer_list_filter_others:

                break;
        }

    }
}
