package com.kfs.expertsystems.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kfs.expertsystems.R;
import com.kfs.expertsystems.callbacks.OnClickItemListener;
import com.kfs.expertsystems.models.ProductsResponse;

import java.util.List;

/**
 * Created by swaroop on 18/04/18.
 */

public class ProductsGridAdapter extends BaseAdapter {

    Context mContext;
    OnClickItemListener listener;
    List<ProductsResponse.ResponseDataBean.ProductsBean> data;
    String baseUrl;
    private static LayoutInflater inflater=null;

    public ProductsGridAdapter(Context context, List<ProductsResponse.ResponseDataBean.ProductsBean>
            adpterValue, String baseUrl) {
        this.mContext = context;
        this.listener = (OnClickItemListener) context;
        this.data = adpterValue;
        this.baseUrl = baseUrl;
    }


    public void filterList(List<ProductsResponse.ResponseDataBean.ProductsBean> adpterValue) {
        this.data = adpterValue;
        notifyDataSetChanged();
    }

    public class Holder
    {
        TextView product_item_title, product_item_desc;
        ImageView product_item_image;
    }

    public View getView(final int position,
                        View convertView, ViewGroup parent) {

        ProductsGridAdapter.Holder holder=new ProductsGridAdapter.Holder();
        View rowView;

        rowView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_products, parent, false);

        holder.product_item_desc = (TextView) rowView.findViewById(R.id.product_item_desc);
        holder.product_item_title = (TextView) rowView.findViewById(R.id.product_item_title);
        holder.product_item_image = (ImageView) rowView.findViewById(R.id.product_item_image);

        holder.product_item_desc.setText(data.get(position).getProductQuantity());
        holder.product_item_title.setText(data.get(position).getProductName());
        String url = data.get(position).getProductImage();
        if (!url.equalsIgnoreCase
                ("")) {

            Glide.with(mContext).load(baseUrl + url)
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.ic_image_place_holder) // can also be a drawable
                    .error(R.drawable.ic_image_place_holder)
                    .crossFade()
                    .into(holder.product_item_image);
        }

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClick(position, data.get(position));
            }
        });


        return rowView;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }
}