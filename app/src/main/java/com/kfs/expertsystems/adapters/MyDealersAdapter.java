package com.kfs.expertsystems.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kfs.expertsystems.R;
import com.kfs.expertsystems.callbacks.OnClickItemListener;
import com.kfs.expertsystems.models.DealersResponse;

import java.util.List;

public class MyDealersAdapter extends RecyclerView.Adapter<MyDealersAdapter.ViewHolder> {

    private Context mContext;
    private OnClickItemListener listener;
    private List<DealersResponse.ResponseDataBean.DealerBean> listData;

    public MyDealersAdapter(Context context, List<DealersResponse.ResponseDataBean.DealerBean>
            data) {
        this.mContext = context;
        this.listener = (OnClickItemListener) context;
        this.listData = data;
    }

    @Override
    public MyDealersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dealers, parent, false);
        return new MyDealersAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyDealersAdapter.ViewHolder holder, final int position) {

        holder.dealer_list_name.setText(listData.get(position).getDealerName());
        if (listData.get(position).getDealerCompanyName() != null)
            holder.dealer_list_company_name.setText(listData.get(position).getDealerCompanyName());
        else
            holder.dealer_list_company_name.setText(listData.get(position).getDealerName());


        holder.dealer_list_phone.setText(listData.get(position).getDealerNumber());
        holder.dealer_list_address.setText(listData.get(position).getDealerAddress());


        if (listData.get(position).isPestisides()) {
            holder.dealer_list_insecticide.setVisibility(View.VISIBLE);
        } else {
            holder.dealer_list_insecticide.setVisibility(View.GONE);
        }
        if (listData.get(position).isFungicides()) {
            holder.dealer_list_fungicides.setVisibility(View.VISIBLE);
        } else {
            holder.dealer_list_fungicides.setVisibility(View.GONE);
        }

        if (listData.get(position).isHerbicides()) {
            holder.dealer_list_herbicides.setVisibility(View.VISIBLE);
        } else {
            holder.dealer_list_herbicides.setVisibility(View.GONE);
        }

        if (listData.get(position).getOthers() != null) {
            if (!listData.get(position).getOthers().isEmpty()) {
                holder.dealer_list_others.setVisibility(View.VISIBLE);
                holder.dealer_list_others_text.setText(listData.get(position).getOthers());
            } else {
                holder.dealer_list_others.setVisibility(View.GONE);
            }
        } else {
            holder.dealer_list_others.setVisibility(View.GONE);
        }

        holder.dealer_layout_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+ listData.get(position).getDealerNumber()));
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView dealer_list_name, dealer_list_company_name,
                dealer_list_phone, dealer_list_address, dealer_list_others_text;

        LinearLayout dealer_list_insecticide, dealer_list_herbicides, dealer_list_fungicides,
                dealer_list_others;

        RelativeLayout dealer_layout_call;


        public ViewHolder(View itemView) {
            super(itemView);


            dealer_list_name = (TextView) itemView.findViewById(R.id.dealer_list_name);
            dealer_list_company_name = (TextView) itemView.findViewById(R.id.dealer_list_company_name);
            dealer_list_phone = (TextView) itemView.findViewById(R.id.dealer_list_phone);
            dealer_list_address = (TextView) itemView.findViewById(R.id.dealer_list_address);
            dealer_list_others_text = (TextView) itemView.findViewById(R.id.dealer_list_others_text);

            dealer_list_insecticide = (LinearLayout) itemView.findViewById(R.id.dealer_list_insecticide);
            dealer_list_herbicides = (LinearLayout) itemView.findViewById(R.id.dealer_list_herbicides);
            dealer_list_fungicides = (LinearLayout) itemView.findViewById(R.id.dealer_list_fungicides);
            dealer_list_others = (LinearLayout) itemView.findViewById(R.id.dealer_list_others);

            dealer_layout_call = (RelativeLayout) itemView.findViewById(R.id.layout_call);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(getAdapterPosition(), listData.get(getAdapterPosition()));
                }
            });
        }
    }
}
