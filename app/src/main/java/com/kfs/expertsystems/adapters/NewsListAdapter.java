package com.kfs.expertsystems.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kfs.expertsystems.R;
import com.kfs.expertsystems.callbacks.OnClickItemListener;
import com.kfs.expertsystems.models.NewsResponse;

public class NewsListAdapter extends RecyclerView.Adapter<NewsListAdapter.ViewHolder> {

    OnClickItemListener listener;
    Context mContext;
    NewsResponse newsList;

    public NewsListAdapter(Context context, NewsResponse news) {
        this.mContext = context;
        this.listener = (OnClickItemListener) context;
        this.newsList = news;
    }

    @Override
    public NewsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_news_articles, parent, false);

        return new NewsListAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        NewsResponse.ResponseDataBean.NewsBean newsBeen = newsList.getResponseData().getNews()
                .get(position);
        holder.news_title.setText(newsBeen.getNewsTitle());
        holder.news_desc.setText(newsBeen.getNews_Discription());
        if (newsBeen.getImage() != null) {


            Glide.with(mContext).load(newsList.getResponseData().getBaseUrl() + newsBeen.getImage())
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.ic_image_place_holder) // can also be a drawable
                    .error(R.drawable.ic_image_place_holder)
                    .crossFade()
                    .into(holder.news_image);

        }
    }

    @Override
    public int getItemCount() {
        return newsList.getResponseData().getNews().size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView news_time, news_desc, news_title;
        ImageView news_image;

        public ViewHolder(View itemView) {
            super(itemView);
            news_time = (TextView) itemView.findViewById(R.id.news_time);
            news_desc = (TextView) itemView.findViewById(R.id.news_desc);
            news_title = (TextView) itemView.findViewById(R.id.news_title);

            news_image = (ImageView) itemView.findViewById(R.id.news_image);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(getAdapterPosition(), newsList.getResponseData().getNews()
                            .get(getAdapterPosition()));
                }
            });
        }
    }
}
