package com.kfs.expertsystems.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kfs.expertsystems.R;
import com.kfs.expertsystems.models.CategoryResponse.ResponseDataBean.GalleryCategoryBean;
import com.kfs.expertsystems.utils.DownloadTask;

import java.util.List;


public class PDFAdapter extends RecyclerView.Adapter<PDFAdapter.ViewHolder> {

    private List<GalleryCategoryBean> list;
    private String baseUrl;
    private Context context;
    private Callback callback;


    public PDFAdapter(Context context, List<GalleryCategoryBean>
            galleryList, String baseUrl) {
        this.context = context;
        this.list = galleryList;
        this.baseUrl = baseUrl;
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public void removeCallback() {
        this.callback = null;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void removeContext() {
        this.context = null;
    }

    @Override
    public PDFAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_pdf, parent, false));
    }

    @Override
    public void onBindViewHolder(PDFAdapter.ViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void updateList(List<GalleryCategoryBean> list) {
        this.list.clear();
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public void updateBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public interface Callback {

        void onErrorPdfUrl();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvPdfName;

        public ViewHolder(View itemView) {
            super(itemView);

            tvPdfName = (TextView) itemView.findViewById(R.id.tv_pdf_name);
        }

        private void onBind(final int position) {
            final GalleryCategoryBean galleryCategoryBean = list.get(position);

            if (galleryCategoryBean != null && galleryCategoryBean.getPdfUrl() != null
                    && !galleryCategoryBean.getPdfUrl().isEmpty()) {

                String pdfUrl = galleryCategoryBean.getPdfUrl();
                String pdfName = galleryCategoryBean.getPdfName();

                if (pdfUrl == null)
                    pdfUrl = "";
                if (pdfName == null)
                    pdfName = "";


                tvPdfName.setText(pdfName);

                final String finalPdfUrl = pdfUrl;
                final String finalPdfName = pdfName;
                tvPdfName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new DownloadTask(context,
                                    baseUrl + "pdfs" + finalPdfUrl,
                                    finalPdfName);
                    }
                });
            } else {
                tvPdfName.setText("DOWNLOAD");
                tvPdfName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (callback != null)
                            callback.onErrorPdfUrl();
                    }
                });
            }
        }
    }
}