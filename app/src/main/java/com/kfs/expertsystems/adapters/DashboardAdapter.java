package com.kfs.expertsystems.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kfs.expertsystems.R;
import com.kfs.expertsystems.models.PromotionsResponse;

import java.util.List;


public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.ViewHolder> {

    private List<PromotionsResponse.ResponseDataBean.PromosBean> promos;
    private Context mContext;
    private String baseUrl;

    public DashboardAdapter(Context context, List<PromotionsResponse.ResponseDataBean.PromosBean> promos, String baseUrl) {
        this.promos = promos;
        this.baseUrl = baseUrl;
        mContext = context;
    }

    @Override
    public int getItemCount() {
        return promos.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PromotionsResponse.ResponseDataBean.PromosBean promosBean = promos.get(position);
        if (!promosBean.getPromoImageURL().equalsIgnoreCase
                ("")) {

            Glide.with(mContext).load(baseUrl + promosBean.getPromoImageURL())
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.ic_image_place_holder) // can also be a drawable
                    .error(R.drawable.ic_image_place_holder)
                    .crossFade()
                    .into(holder.ivPromos);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_dashboard, parent, false);

        return new ViewHolder(itemView);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView ivPromos;

        public ViewHolder(final View itemView) {
            super(itemView);
            ivPromos = itemView.findViewById(R.id.ivPromo);

        }
    }
}
