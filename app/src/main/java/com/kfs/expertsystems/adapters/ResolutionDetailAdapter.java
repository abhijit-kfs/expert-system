package com.kfs.expertsystems.adapters;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kfs.expertsystems.R;
import com.kfs.expertsystems.callbacks.OnClickItemListenerwithType;
import com.kfs.expertsystems.models.CommentsModel;

import java.util.List;

public class ResolutionDetailAdapter extends RecyclerView.Adapter<ResolutionDetailAdapter.ViewHolder> {

    private Context mContext;
    private OnClickItemListenerwithType listener;

    class ViewHolder extends RecyclerView.ViewHolder implements OnClickItemListenerwithType{

        RecyclerView rVResolutionDetails;
        private ResolutionDetailUploadsAdapter rvHorizontalAdapter;
        TextView resolution_sub_item_desc , resolution_sub_item_user_type;

        public ViewHolder(View itemView) {
            super(itemView);
            Context context = itemView.getContext();

            rVResolutionDetails = (RecyclerView) itemView.findViewById(R.id.rv_resolution_horizontal);
            resolution_sub_item_desc = (TextView) itemView.findViewById(R.id
                    .resolution_sub_item_desc);
            resolution_sub_item_user_type = (TextView) itemView.findViewById(R.id
                    .resolution_sub_item_user_type);


            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            rVResolutionDetails.setLayoutManager(layoutManager);
            rVResolutionDetails.setItemAnimator(new DefaultItemAnimator());

          /*  itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(getAdapterPosition(), new Object());
                    Log.d("tag", "onClick: ");
                }
            });*/
        }

        @Override
        public void onClick(int adapterPosition, Boolean image, Object data) {
            Log.d("item", "onClick:itemImage ");
        }
    }

    private List<CommentsModel> commentsModelsList;

    public ResolutionDetailAdapter(List<CommentsModel> commentsModelsList, Context context) {
        this.mContext = context;
        this.listener = (OnClickItemListenerwithType) context;
        this.commentsModelsList = commentsModelsList;
    }

    @Override
    public ResolutionDetailAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_resolution_detail, parent, false);
        return new ResolutionDetailAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ResolutionDetailAdapter.ViewHolder holder, int position) {
        holder.resolution_sub_item_desc.setText(commentsModelsList.get(position).getComDesc());
        if(commentsModelsList.get(position).getUserType() == 0){
            holder.resolution_sub_item_user_type.setText("Me");
        }else if(commentsModelsList.get(position).getUserType() == 1){
            holder.resolution_sub_item_user_type.setText("Expert");
        }else{
            holder.resolution_sub_item_user_type.setText("Unknown");
        }


        holder.rvHorizontalAdapter = new ResolutionDetailUploadsAdapter(commentsModelsList
                .get(position).getGallery(),mContext,commentsModelsList.get(position).getBaseUrl());
        holder.rVResolutionDetails.setAdapter(holder.rvHorizontalAdapter);



    }

    @Override
    public int getItemCount() {
        return commentsModelsList.size();
    }
}
