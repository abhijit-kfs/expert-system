package com.kfs.expertsystems.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kfs.expertsystems.R;
import com.kfs.expertsystems.callbacks.OnClickItemListener;
import com.kfs.expertsystems.models.ResolutionModel;

import java.util.Calendar;
import java.util.List;

public class ResolutionMultiListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    OnClickItemListener listener;
    List<ResolutionModel> resolutionList;

    private static final int VIEW_NORMAL = 1;
    private static final int VIEW_EMPTY = 2;

    public ResolutionMultiListAdapter(Context context, List<ResolutionModel> resolutionList) {
        this.mContext = context;
        this.listener = (OnClickItemListener) context;
        this.resolutionList = resolutionList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_NORMAL:
                return new NormalViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_myresolutions, parent, false));
            case VIEW_EMPTY:
            default:
                return new EmptyViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.empty_screen_layout2, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (resolutionList.size() > 0)
            ((NormalViewHolder) holder).onBind(position);
        else
            ((EmptyViewHolder) holder).onBind(position);
    }

    @Override
    public int getItemCount() {
        if (resolutionList.size() > 0)
            return resolutionList.size();
        else
            return 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (resolutionList.size() > 0)
            return VIEW_NORMAL;
        else return VIEW_EMPTY;
    }

    class NormalViewHolder extends RecyclerView.ViewHolder {

        public TextView textViewStatus, problem_id_value, resolution_item_status,
                resolution_item_date, resolution_item_stage_name, resolution_item_crop_name;
        public RelativeLayout relativeLayoutMain, relativeLayoutIndicator;
        private ImageView resolution_item_image;

        public NormalViewHolder(View itemView) {
            super(itemView);
            textViewStatus = (TextView) itemView.findViewById(R.id.resolution_item_status);
            relativeLayoutMain = (RelativeLayout) itemView.findViewById(R.id.relative_main);
            relativeLayoutIndicator = (RelativeLayout) itemView.findViewById(R.id.relative_indicator);
            resolution_item_image = (ImageView) itemView.findViewById(R.id.resolution_item_image);
            problem_id_value = (TextView) itemView.findViewById(R.id.problem_id_value);

            resolution_item_status = (TextView) itemView.findViewById(R.id.resolution_item_status);
            resolution_item_date = (TextView) itemView.findViewById(R.id.resolution_item_date);
            resolution_item_stage_name = (TextView) itemView.findViewById(R.id.resolution_item_stage_name);
            resolution_item_crop_name = (TextView) itemView.findViewById(R.id.resolution_item_crop_name);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(getAdapterPosition(), resolutionList.get(getAdapterPosition()));
                }
            });
        }

        public void onBind(int position) {
            ResolutionModel resolutionModel = resolutionList.get(position);


            Glide.with(mContext).load(resolutionModel.getBaseURL() + resolutionModel.getCropImage())
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.ic_image_place_holder) // can also be a drawable
                    .error(R.drawable.ic_image_place_holder)
                    .crossFade()
                    .into(resolution_item_image);

            textViewStatus.setText(resolutionModel.getStatus());
            problem_id_value.setText("CRPID" + resolutionModel.getProblemId());
            resolution_item_crop_name.setText(resolutionModel.getCropName() + ">");
//        holder.resolution_item_stage_name.setText(resolutionModel.getStageName());

            resolution_item_date.setText(getDate(resolutionModel.getCreatedDate()));

            if (resolutionModel.getStatus().equalsIgnoreCase("closed")) {
                relativeLayoutMain.setBackgroundColor(ContextCompat.getColor(mContext, R.color.lightGray));
                relativeLayoutIndicator.setBackgroundColor(ContextCompat.getColor(mContext, R.color.buttonDisabledGray));
            } else {
                relativeLayoutMain.setBackgroundColor(ContextCompat.getColor(mContext, R.color
                        .white));
                relativeLayoutIndicator.setBackgroundColor(ContextCompat.getColor(mContext, R.color.textGreen));
            }
        }
    }

    public class EmptyViewHolder extends RecyclerView.ViewHolder {

        public EmptyViewHolder(View itemView) {
            super(itemView);
        }

        public void onBind(int position) {

        }
    }


    private String getDate(long time) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(time);
        String date = DateFormat.format("dd MMM", cal).toString();
        String year = DateFormat.format("yy", cal).toString();
        return date + "'" + year;
    }
}
