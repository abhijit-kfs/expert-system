package com.kfs.expertsystems.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kfs.expertsystems.R;
import com.kfs.expertsystems.callbacks.OnClickItemListener;
import com.kfs.expertsystems.models.CropResponse;

import java.util.List;

public class CropsListAdapter extends BaseAdapter {

    Context mContext;
    OnClickItemListener listener;
    List<CropResponse.ResponseDataBean.CropsBean> data;
    String baseUrl;
    private static LayoutInflater inflater=null;

    public CropsListAdapter(Context context, List<CropResponse.ResponseDataBean.CropsBean>
            adpterValue, String baseUrl) {
        this.mContext = context;
        this.listener = (OnClickItemListener) context;
        this.data = adpterValue;
        this.baseUrl = baseUrl;
    }


    public void filterList(List<CropResponse.ResponseDataBean.CropsBean> adpterValue) {
        this.data = adpterValue;
        notifyDataSetChanged();
    }

//    @Override
//    public CropsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View itemView = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.item_crops, parent, false);
//
//        return new CropsListAdapter.ViewHolder(itemView);
//    }
//
//    @Override
//    public void onBindViewHolder(CropsListAdapter.ViewHolder holder, int position) {
//
//        holder.crop_item_desc.setText(list.get(position).getCropDesc());
//        holder.crop_item_title.setText(list.get(position).getCropName());
//        String url = list.get(position).getCropImageUrl();
//        if (!url.equalsIgnoreCase
//                ("")) {
//
//            Glide.with(mContext).load(baseUrl + url)
//                    .thumbnail(0.5f)
//                    .crossFade()
//                    .diskCacheStrategy(DiskCacheStrategy.ALL)
//                    .placeholder(R.drawable.ic_image_place_holder) // can also be a drawable
//                    .error(R.drawable.ic_image_place_holder)
//                    .crossFade()
//                    .into(holder.crop_item_image);
//        }
//
//    }

//    @Override
//    public int getItemCount() {
//        return list.size();
////    }

    public class Holder
    {
        TextView crop_item_title, crop_item_desc;
        ImageView crop_item_image;
    }

    public View getView(final int position,
                        View convertView, ViewGroup parent) {

        Holder holder=new Holder();
        View rowView;

        rowView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.grid_item_crops, parent, false);

        holder.crop_item_desc = (TextView) rowView.findViewById(R.id.crop_item_desc);
        holder.crop_item_title = (TextView) rowView.findViewById(R.id.crop_item_title);
        holder.crop_item_image = (ImageView) rowView.findViewById(R.id.crop_item_image);

        holder.crop_item_desc.setText(data.get(position).getCropDesc());
        holder.crop_item_title.setText(data.get(position).getCropName());
        String url = data.get(position).getCropImageUrl();
        if (!url.equalsIgnoreCase
                ("")) {

            Glide.with(mContext).load(baseUrl + url)
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.ic_image_place_holder) // can also be a drawable
                    .error(R.drawable.ic_image_place_holder)
                    .crossFade()
                    .into(holder.crop_item_image);
        }

        rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(position, data.get(position));
                }
            });


        return rowView;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }
}

//    @Override
//    public View getView(int i, View view, ViewGroup viewGroup) {
//        return null;
//    }

//    class ViewHolder extends RecyclerView.ViewHolder {
//        TextView crop_item_title, crop_item_desc;
//        ImageView crop_item_image;
//
//
//        public ViewHolder(final View itemView) {
//            super(itemView);
//            crop_item_desc = (TextView) itemView.findViewById(R.id.crop_item_desc);
//            crop_item_title = (TextView) itemView.findViewById(R.id.crop_item_title);
//            crop_item_image = (ImageView) itemView.findViewById(R.id.crop_item_image);
//
//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    listener.onClick(getAdapterPosition(), list.get(getAdapterPosition()));
//                }
//            });
//        }
//    }
