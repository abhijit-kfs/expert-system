package com.kfs.expertsystems.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kfs.expertsystems.R;
import com.kfs.expertsystems.callbacks.OnClickItemListenerwithType;

import java.util.List;

public class ReportProblemImagesAdapter extends RecyclerView.Adapter<ReportProblemImagesAdapter.ViewHolder> {

    List<Uri> uriList;
    Context mContext;
    OnClickItemListenerwithType listener;

    public  ReportProblemImagesAdapter(List<Uri> uris , Context context){
        this.uriList = uris;
        this.mContext = context;
        this.listener = (OnClickItemListenerwithType) context;
    }


    public void addList(Uri uri) {
        this.uriList.add(uri);
        notifyDataSetChanged();
    }

    public void deleteList(Uri uri) {
        this.uriList.remove(uri);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_add_photo, parent, false);

        return new ReportProblemImagesAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

//        holder.problem_item_image.setImageBitmap(getBitmapFromUri(uriList.get(position)));
        Glide.with(mContext).load(uriList.get(position))
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.ic_image_place_holder)
                .crossFade()
                .into( holder.problem_item_image);
    }

    @Override
    public int getItemCount() {
        return uriList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView problem_item_image;

        public ViewHolder(View itemView) {
            super(itemView);

            problem_item_image = (ImageView) itemView.findViewById(R.id.problem_item_image);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(getAdapterPosition(), true, uriList.get(getAdapterPosition()));
                }
            });
        }
    }

}
