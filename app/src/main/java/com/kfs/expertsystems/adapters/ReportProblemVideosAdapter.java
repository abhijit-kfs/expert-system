

package com.kfs.expertsystems.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.kfs.expertsystems.R;
import com.kfs.expertsystems.callbacks.OnClickItemListenerwithType;

import java.util.List;

public class ReportProblemVideosAdapter extends RecyclerView.Adapter<ReportProblemVideosAdapter.ViewHolder> {

    List<Uri> uriList;
    Context mContext;
    OnClickItemListenerwithType listener;

    public ReportProblemVideosAdapter(List<Uri> uris, Context context) {
        this.uriList = uris;
        this.mContext = context;
        this.listener = (OnClickItemListenerwithType) context;
    }


    public void addList(Uri uri) {
        this.uriList.add(uri);
        Log.d("ADD list", "addList: ");
        notifyDataSetChanged();
    }

    public void deleteList(Uri uri) {
        this.uriList.remove(uri);
        notifyDataSetChanged();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_add_video, parent, false);

        return new ReportProblemVideosAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Log.d("add List", "onBindViewHolder: " + uriList.get(position).getPath());

        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = mContext.getContentResolver().query(uriList.get(position), filePathColumn, null, null, null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String picturePath = cursor.getString(columnIndex);
        cursor.close();

        Bitmap bitmap2 = ThumbnailUtils.createVideoThumbnail(picturePath, MediaStore.Video.Thumbnails.MICRO_KIND);

        holder.problem_item_video.setImageBitmap(bitmap2);
    /*    Glide.with(mContext).load(uriList.get(position))
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.ic_image_place_holder)
                .crossFade()
                .into(holder.problem_item_image);*/
    }

    @Override
    public int getItemCount() {
        return uriList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView problem_item_video;
        private ImageView video_play;

        public ViewHolder(View itemView) {
            super(itemView);

            problem_item_video = (ImageView) itemView.findViewById(R.id.problem_item_video);
            video_play = (ImageView) itemView.findViewById(R.id.problem_play);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(getAdapterPosition(), false, uriList.get(getAdapterPosition()));
//                    problem_item_video.setVideoURI(uriList.get(getAdapterPosition()));
//                    problem_item_video.start();
                }
            });
        }
    }

}
