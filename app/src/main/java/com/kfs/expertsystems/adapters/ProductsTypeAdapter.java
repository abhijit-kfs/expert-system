package com.kfs.expertsystems.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kfs.expertsystems.R;
import com.kfs.expertsystems.models.CategoryResponse.ResponseDataBean;


import java.util.List;


public class ProductsTypeAdapter extends RecyclerView.Adapter<ProductsTypeAdapter.ViewHolder> {

    private List<ResponseDataBean> list;
    private Context context;
    private Callback callback;


    public ProductsTypeAdapter(List<ResponseDataBean> list, Context context) {
        this.list = list;
        this.context = context;
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public void removeCallback() {
        this.callback = null;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void removeContext() {
        this.context = null;
    }

    @Override
    public ProductsTypeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_product_type, parent, false));
    }

    @Override
    public void onBindViewHolder(ProductsTypeAdapter.ViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void updateList(List<ResponseDataBean> list) {
        this.list.clear();
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public interface Callback {

        void onSelectedCategory(int id);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvProductCategoryName;

        public ViewHolder(final View itemView) {
            super(itemView);
            tvProductCategoryName = itemView.findViewById(R.id.product_type_name);
        }

        private void onBind(final int position) {
            tvProductCategoryName.setText(list.get(position).getProductCategoryName());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (callback != null)
                        callback.onSelectedCategory(list.get(position).getId());
                }
            });
        }
    }
}