package com.kfs.expertsystems.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kfs.expertsystems.R;
import com.kfs.expertsystems.callbacks.ImageReportCallback;
import com.kfs.expertsystems.models.ResolutionResponse;
import com.kfs.expertsystems.utils.Utils;

import java.util.List;

public class ResolutionDetailUploadsAdapter extends RecyclerView.Adapter<ResolutionDetailUploadsAdapter.ViewHolder> {

    private Context mContext;
    private ImageReportCallback listener;
    List<ResolutionResponse.ResponseDataBean.ResolutionListBean.GalleryBean> gallery;
    String baseUrl;
    Bitmap bitmap;

    public ResolutionDetailUploadsAdapter(List<ResolutionResponse.ResponseDataBean.ResolutionListBean.GalleryBean> gallery, Context context, String baseUrl) {
        this.mContext = context;
        this.listener = (ImageReportCallback) context;
        this.gallery = gallery;
        this.baseUrl = baseUrl;
    }

    @Override
    public ResolutionDetailUploadsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_resolution_detail_uploads, parent, false);
        return new ResolutionDetailUploadsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ResolutionDetailUploadsAdapter.ViewHolder holder, final int position) {

        if(gallery.get(position).getImageUrl() != null) {

            holder.resolution_sub_item_video.setVisibility(View.GONE);
            holder.resolution_sub_item_play.setVisibility(View.GONE);
            holder.resolution_sub_item_image.setVisibility(View.VISIBLE);

            Glide.with(mContext).load(baseUrl + gallery.get(position).getImageUrl())
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .error(R.drawable.ic_image_place_holder)
                    .crossFade()
                    .into(holder.resolution_sub_item_image);

        } else {
            holder.resolution_sub_item_video.setVisibility(View.VISIBLE);
            holder.resolution_sub_item_play.setVisibility(View.VISIBLE);
            holder.resolution_sub_item_image.setVisibility(View.GONE);
            Log.d("v", "onBindViewHolder: "+ baseUrl+"/videos"+gallery.get(position).getVideoUrl()+".mp4");

            Thread t = new Thread(new Runnable() {
                public void run() {
                    try {
                        bitmap = Utils.retriveVideoFrameFromVideo(baseUrl+"/videos"+gallery.get(position).getVideoUrl()+".mp4");
                        if (bitmap != null) {
                            ((Activity) mContext).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    bitmap = Bitmap.createScaledBitmap(bitmap, 240, 240, false);
                                    holder.resolution_sub_item_video.setImageBitmap(bitmap);
                                }
                            });

                        }
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }
                }
            });

            t.start();
        }

    }

    @Override
    public int getItemCount() {
        return gallery.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView resolution_sub_item_image, resolution_sub_item_play, resolution_sub_item_video;
//        VideoView resolution_sub_item_video;

        public ViewHolder(View itemView) {
            super(itemView);
            resolution_sub_item_image = (ImageView) itemView.findViewById(R.id
                    .resolution_sub_item_image);
            resolution_sub_item_play = (ImageView) itemView.findViewById(R.id
                    .resolution_sub_item_play);
            resolution_sub_item_video = (ImageView) itemView.findViewById(R.id
                    .resolution_sub_item_video);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("itemimage", "onClick: ");
                    if(gallery.get(getAdapterPosition()).getImageUrl() != null) {
                        listener.startImageEnlarge(Uri.parse(baseUrl + gallery.get(getAdapterPosition())
                                .getImageUrl()), true);
                    }
                    else {
//                        listener.startImageEnlarge(Uri.parse(baseUrl+"/videos"+gallery.get(getAdapterPosition()).getVideoUrl()+".mp4"), false);
                        String videoPath = baseUrl+"/videos"+gallery.get(getAdapterPosition()).getVideoUrl()+".mp4";
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(videoPath));
                        intent.setDataAndType(Uri.parse(videoPath), "video/mp4");
                        mContext.startActivity(intent);
                    }
                }
            });
        }
    }


}
