package com.kfs.expertsystems.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kfs.expertsystems.R;
import com.kfs.expertsystems.callbacks.OnClickItemListener;
import com.kfs.expertsystems.models.CropResponse;

public class CropStageAdapter extends RecyclerView.Adapter<CropStageAdapter.ViewHolder> {

    private Context mContext;
    private OnClickItemListener listener;
    private CropResponse.ResponseDataBean.CropsBean dataList;
    private String BASE_URL;


    public CropStageAdapter(Context context, CropResponse.ResponseDataBean.CropsBean selectedCrop, String BASE_URL) {
        this.mContext = context;
        this.listener = (OnClickItemListener) context;
        this.dataList = selectedCrop;
        this.BASE_URL = BASE_URL;
    }

    @Override
    public CropStageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_crop_stage, parent, false);
        return new CropStageAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CropStageAdapter.ViewHolder holder, int position) {

        holder.stage_item_title.setText(dataList.getStageModel().get(position).getStageName());
        holder.stage_item_desc.setText(dataList.getStageModel().get(position).getStageDesc());


        String url = dataList.getStageModel().get(position).
                getStageImageUrl();
        if (!url.equalsIgnoreCase
                ("")) {

            Glide.with(mContext).load(BASE_URL + url)
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .error(R.drawable.ic_image_place_holder)
                    .crossFade()
                    .into(holder.stage_item_image);
        }
    }

    @Override
    public int getItemCount() {
        return dataList.getStageModel().size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView stage_item_image;
        TextView stage_item_title, stage_item_desc;

        public ViewHolder(View itemView) {
            super(itemView);
            stage_item_image = (ImageView) itemView.findViewById(R.id.stage_item_image);
            stage_item_title = (TextView) itemView.findViewById(R.id.stage_item_title);
            stage_item_desc = (TextView) itemView.findViewById(R.id.stage_item_desc);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(getAdapterPosition(), dataList);
                }
            });
        }
    }
}
