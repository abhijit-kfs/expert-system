package com.kfs.expertsystems.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kfs.expertsystems.R;
import com.kfs.expertsystems.callbacks.CategoriesOnClickListener;
import com.kfs.expertsystems.models.CategoryResponse;

import java.util.List;

/**
 * Created by swaroop on 20/04/18.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    private Context mContext;
    private CategoriesOnClickListener listener;
    private List<CategoryResponse.ResponseDataBean> dataList;


    public CategoryAdapter(Context context, List<CategoryResponse.ResponseDataBean> categoryList) {
        this.mContext = context;
        this.listener = (CategoriesOnClickListener) context;
        this.dataList = categoryList;
    }

    @Override
    public CategoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent, false);
        return new CategoryAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoryAdapter.ViewHolder holder, int position) {

        holder.textViewCategoryName.setText(dataList.get(position).getProductCategoryName());
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewCategoryName;

        public ViewHolder(View itemView) {
            super(itemView);
            textViewCategoryName = (TextView) itemView.findViewById(R.id.text_category_name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClickCategories(getAdapterPosition(), dataList.get(getAdapterPosition()).getProductCategoryName());
                }
            });
        }
    }
}
