package com.kfs.expertsystems.utils;


import
        android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.kfs.expertsystems.R;
import com.kfs.expertsystems.callbacks.AWSListener;

import java.io.File;


/**
 * Created by Tomesh on 03-11-2017.
 */

public class AWSHelper {

    private static AWSHelper INSTANCE = null;

    private final String BUCKET_NAME = "com.sportz";
    private final String AWS_CLOUD_URL = "https://s3.ap-south-1.amazonaws.com";

    private Context mCtx;

    private AWSListener listener;

    public static void main(String args[]) {
        //AWSHelper.getInstance(null).upload("somekey", new File("D:\\BitBucketRepos\\sportzutility\\app\\src\\main\\res\\drawable\\profile_pic.jpg"));
    }

    AWSCredentialsProvider credentialsProvider = new AWSCredentialsProvider() {
        @Override
        public AWSCredentials getCredentials() {
            AWSCredentials credentials = new AWSCredentials() {
                @Override
                public String getAWSAccessKeyId() {
                    return "AKIAJBXEFOERBSUHRWRQ";
                }

                @Override
                public String getAWSSecretKey() {
                    return "M/oyowDZc5Egu0P+Fx69Ds9MuODDOsAT78v/QnFm";
                }
            };
            return credentials;
        }

        @Override
        public void refresh() {

        }
    };

    AmazonS3 s3 = new AmazonS3Client(credentialsProvider);

    private AWSHelper(Activity activity) {
        mCtx = activity.getApplicationContext();
        s3.setRegion(Region.getRegion(Regions.fromName("us-east-2")));
        s3.setEndpoint(AWS_CLOUD_URL);
        utility = new TransferUtility(s3, mCtx);
        listener = (AWSListener) activity;
    }

    public void setListener(AWSListener listener) {
        this.listener = listener;
    }

    TransferUtility utility = null;

    public static AWSHelper getInstance(Activity activity) {
        if (null == INSTANCE) {
            INSTANCE = new AWSHelper(activity);
        }
        return INSTANCE;
    }

    public void upload(final String key, File file) {
        if (!Utils.isNetworkConnected(mCtx)) {
            Toast.makeText(mCtx, mCtx.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            return;
        }
        utility.upload(BUCKET_NAME, key, file, CannedAccessControlList.PublicRead).setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                switch (state) {
                    case IN_PROGRESS:
                        listener.onStart(key);
                        break;
                    case COMPLETED:
                        listener.onComplete(key, AWS_CLOUD_URL + "/" + BUCKET_NAME + "/" + key);
                        break;
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                listener.publicProgress(key, bytesCurrent, bytesTotal);
            }

            @Override
            public void onError(int id, Exception ex) {
                listener.onError(key, ex);
            }
        });
    }
}