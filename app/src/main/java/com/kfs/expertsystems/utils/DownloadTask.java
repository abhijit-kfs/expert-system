package com.kfs.expertsystems.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadTask {

    private static final String TAG = "Download Task";
    private Context context;
    private String downloadDirectory = "ExpertSystem";

    private String downloadUrl = "", downloadFileName = "";
    private ProgressDialog progressDialog;

    public DownloadTask(Context context, String downloadUrl, String downloadFileName) {
        this.context = context;

        this.downloadUrl = downloadUrl;
//        this.downloadFileName = "/" + downloadFileName;
//        this.downloadFileName = "/" + downloadUrl.substring(downloadUrl.lastIndexOf( '/' ) + 1,downloadUrl.length());
//        this.downloadFileName = this.downloadFileName.concat(".pdf");


        this.downloadFileName = downloadFileName;

        Log.e(TAG, downloadFileName);

        //Start Downloading Task
        new DownloadingTask().execute();
    }

    private class DownloadingTask extends AsyncTask<Void, Void, Void> {

        File apkStorage = null;
        File outputFile = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Downloading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Void result) {
            try {
                if (outputFile != null) {
                    progressDialog.dismiss();
                    Toast.makeText(context, "Downloaded Successfully", Toast.LENGTH_SHORT).show();
//                    openDownloadedFolder(downloadFileName);
//                    showPdf(downloadFileName);
                    doTask();
                    openFile(downloadFileName);
                } else {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                        }
                    }, 3000);

                    Log.e(TAG, "Download Failed");

                }
            } catch (Exception e) {
                e.printStackTrace();

                //Change button text if exception occurs

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                    }
                }, 3000);
                Log.e(TAG, "Download Failed with Exception - " + e.getLocalizedMessage());

            }


            super.onPostExecute(result);
        }

        private void doTask() {
            if(Build.VERSION.SDK_INT>=24){
                try{
                    Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                    m.invoke(null);
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                URL url = new URL(downloadUrl);//Create Download URl
                HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
                c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
                c.connect();//connect the URL Connection

                //If Connection response is not OK then show Logs
                if (c.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    Log.e(TAG, "Server returned HTTP " + c.getResponseCode()
                            + " " + c.getResponseMessage());

                }


                //Get File if SD card is present
                if (isSDCardPresent()) {
//                    apkStorage = getPath(Environment.getExternalStorageDirectory() + "/ExpertSystem");
                    apkStorage = getTempFile(downloadDirectory);
                } else
                    Toast.makeText(context, "Oops!! There is no SD Card.", Toast.LENGTH_SHORT).show();

                //If File is not present create directory
                if (!apkStorage.exists()) {
                    apkStorage.mkdir();
                    Log.e(TAG, "Directory Created.");
                }

                outputFile = new File(apkStorage, downloadFileName);//Create Output file in Main File

                //Create New File if not present
                if (!outputFile.exists()) {
                    outputFile.createNewFile();
                    Log.e(TAG, "File Created");
                }

                FileOutputStream fos = new FileOutputStream(outputFile);//Get OutputStream for NewFile Location

                InputStream is = c.getInputStream();//Get InputStream for connection

                byte[] buffer = new byte[1024];//Set buffer type
                int len1 = 0;//init length
                while ((len1 = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);//Write new file
                }

                //Close all connection after doing task
                fos.close();
                is.close();

            } catch (Exception e) {

                //Read exception if something went wrong
                e.printStackTrace();
                outputFile = null;
                Log.e(TAG, "Download Error Exception " + e.getMessage());
            }

            return null;
        }
    }

    public boolean isSDCardPresent() {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            return true;
        }
        return false;
    }

    public static File getPath(String path) {

        File file = new File(path);

        File directory = file;
        if (directory != null) {
            if (!directory.mkdirs()) {
                if (!directory.exists()) {
                    Log.d(TAG, "failed to create directory");
                    return null;
                }
            }
        }
        return file;
    }

    public static File getTempFile(String path) {
        File f = new File(Environment.getExternalStorageDirectory(), path);
        if (!f.exists()) {
            f.mkdirs();
        }

        return f;
    }

    private void openDownloadedFolder(String downloadFileName) {
        //First check if SD Card is present or not
        if (isSDCardPresent()) {

            //Get Download Directory File
            File apkStorage = new File(
                    Environment.getExternalStorageDirectory() + "/"
                            + downloadDirectory);

            //If file is not present then display Toast
            if (!apkStorage.exists())
                Toast.makeText(context, "Right now there is no directory. Please download some file first.", Toast.LENGTH_SHORT).show();

            else {

                //If directory is present Open Folder

                /** Note: Directory will open only if there is a app to open directory like File Manager, etc.  **/

                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath()
                        + "/" + downloadDirectory + downloadFileName);
                intent.setDataAndType(uri, "file/*");
                context.startActivity(Intent.createChooser(intent, "Open Download Folder"));
            }

        } else
            Toast.makeText(context, "Oops!! There is no SD Card.", Toast.LENGTH_SHORT).show();

    }

    public void showPdf(String downloadFileName) {
        try {
            String p = Environment.getExternalStorageDirectory()
                    + "/" + downloadDirectory + downloadFileName;
            File file = new File(p);//name here is the name of any string you want to pass to the method
            if (!file.isDirectory())
                file.mkdir();
            Intent testIntent = new Intent("com.adobe.reader");
            testIntent.setType("application/pdf");
            testIntent.setAction(Intent.ACTION_VIEW);
            Uri uri = Uri.fromFile(file);
            testIntent.setDataAndType(uri, "application/pdf");
            context.startActivity(testIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openFile(String fileName) {
        try {
            String p = Environment.getExternalStorageDirectory()
                    + "/" + downloadDirectory + "/" + fileName;
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setDataAndType(Uri.fromFile(new File(p)), "application/pdf");//this is for pdf file. Use appropreate mime type
            context.startActivity(i);
        } catch (Exception e) {
            Toast.makeText(context,
                    "No pdf viewing application detected. File saved in download folder",
                    Toast.LENGTH_SHORT).show();
        }
    }
}
