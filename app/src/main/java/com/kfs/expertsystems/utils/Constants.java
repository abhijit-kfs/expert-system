package com.kfs.expertsystems.utils;

/**
 * Created by Tomesh on 21-09-2017.
 */

public class Constants {

    public static final String USER_ID = "user_id";

    public static final String LOGIN = "login";
    public static final String SHOW_ADD_PASSWORD = "show_add_password";

    public static final String LATLNG = "latlng";

    public static  final String WEBSITE_URL_PROMOTIONAL = "http://45.55.216.182/";
}
