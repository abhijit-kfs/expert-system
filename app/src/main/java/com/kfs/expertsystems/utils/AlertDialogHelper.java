package com.kfs.expertsystems.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.kfs.expertsystems.R;
import com.kfs.expertsystems.application.AgriProApplication;


/**
 * Created by Swaroop on 12/27/2017.
 */

public class AlertDialogHelper {

    public static void logoutDialog(final Context context, final Class moveto){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater =  (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View dialogView = inflater.inflate(R.layout.dialog_registeredsucc, null);
        dialogBuilder.setView(dialogView);
        final View hl = (View) dialogView.findViewById(R.id.hl);
        final TextView detail = (TextView) dialogView.findViewById(R.id.detail);
        final TextView titletext = (TextView) dialogView.findViewById(R.id.titletext);
        final Button btn_ok = (Button) dialogView.findViewById(R.id.btn_ok);
        final Button btn_cancel = (Button) dialogView.findViewById(R.id.btn_cancel);
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AgriProApplication.getPref().put(Constants.LOGIN, false);
                AgriProApplication.getPref().put(Constants.USER_ID, "");
                LoginManager.getInstance().logOut();
                context.startActivity(new Intent(context, moveto));
                ((Activity) context).finish();
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
    }

}
