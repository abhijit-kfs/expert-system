package com.kfs.expertsystems.application;

import android.app.Application;
import android.content.Context;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.stetho.Stetho;
import com.kfs.expertsystems.api.ApiClient;
import com.kfs.expertsystems.api.ApiInterface;
import com.kfs.expertsystems.utils.Pref;

/**
 * Created by Tomesh on 19-09-2017.
 */

public class AgriProApplication extends Application {

    static Pref pref;
    private static Context appContext;
    @Override
    public void onCreate() {
        super.onCreate();
        appContext = this;
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        Stetho.initializeWithDefaults(this);
    }

    public static ApiInterface getApiClient(){
        return ApiClient.getClient().create(ApiInterface.class);
    }


    public static Context getAppContext() {
        return appContext;
    }

    public static Pref getPref() {
        if (pref == null) {
            pref = new Pref(getAppContext());
        }
        return pref;
    }
}
